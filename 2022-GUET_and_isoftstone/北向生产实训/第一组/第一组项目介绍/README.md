# 买为目录


- [ 后端介绍](buyItBackend/README.md#1-介绍)  
- [ 后端总体设计](buyItBackend/README.md#2-总体设计)     
  - [ 后端项目架构](buyItBackend/README.md#21-后端项目架构)       
  - [ 数据库设计](buyItBackend/README.md#22-数据库设计)        
  - [ 前后端交互设计](buyItBackend/README.md#23-前后端交互设计)        
  - [ 前后端交互设计](buyItBackend/README.md#前后端交互设计)        
  - [ 后端接口设计](buyItBackend/README.md#24-后端接口设计)
- [ 前端介绍](buy-it/README.md#1-介绍)   
- [ 前端模块设计 / 前端功能实现](buy-it/README.md#2-模块设计--功能实现)      
  - [ 主页](buy-it/README.md#21-主页)
  - [ 发现](buy-it/README.md#22-发现)
  - [ 消息](buy-it/README.md#23-消息)
  - [ 购物车](buy-it/README.md#24-购物车)         
  - [ 我的](buy-it/README.md#25-我的)   
  

