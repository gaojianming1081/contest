package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.VideoMapper;
import com.itrui.buyitbackend.pojo.Video;
import com.itrui.buyitbackend.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VideoMapper videoMapper;
    @Override
    public List<Video> getALlVideos() {
        return videoMapper.getALlVideos();
    }
}
