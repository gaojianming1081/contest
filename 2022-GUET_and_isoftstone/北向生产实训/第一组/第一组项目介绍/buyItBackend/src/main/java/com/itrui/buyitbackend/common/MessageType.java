package com.itrui.buyitbackend.common;

public interface MessageType {


    //用户-用户
    static final int NORMAL = 1;    //字符串消息
    static final int SHARE = 2;     //商品分享

    //后台-用户
    static final int REJECT_ALL = 3;    //消息被对方屏蔽
    static final int REJECT_SHARE = 4; //对方不接收商品分享

    //用户-后台
    static final int BLACK = 5; //屏蔽对方

    static final int BLACK_SHARE = 6; //屏蔽对方分享商品

}
