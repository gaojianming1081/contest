package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.pojo.Product;
import com.itrui.buyitbackend.pojo.ProductPicture;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ProductService {

    /**
     * 查询全部商品
     * @return
     */
    public List<Product> getAllProducts();

    /**
     * 添加商品
     * @param product
     * @return
     */
    public boolean addProduct(Product product);

    /**
     * 添加商品图片
     * @param picture
     * @return
     */
    public boolean addProductPicture(ProductPicture picture);

    /**
     * 条件查询
     * @param product
     * @return
     */
    public Product getProductByNameAndTypeAndStatus(Product product);

    /**
     * 通过商品名查询
     * @param name
     * @return
     */
    public List<Product> getProductByName(String name);

    /**
     * 通过商品类查找商品
     * @param type
     * @return
     */
    public List<Product> getProductByType(String type);

    /**
     * 通过id删除商品
     * @param id
     * @return
     */
    public boolean delProductById(Integer id);

    /**
     * 删除商品对应图片信息
     * @param picId
     * @return
     */
    public boolean delProductPic(Integer picId);

    /**
     * 修改商品信息
     * @param product
     * @return
     */
    public boolean updateProduct(Product product);

    /**
     * 通过商品id查找
     * @param id
     * @return
     */
    public Product getProductById(Integer id);

}
