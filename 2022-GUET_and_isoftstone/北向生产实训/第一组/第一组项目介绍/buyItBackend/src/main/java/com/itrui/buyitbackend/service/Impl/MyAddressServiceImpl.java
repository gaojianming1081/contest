package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.MyAddressMapper;
import com.itrui.buyitbackend.pojo.MyAddress;
import com.itrui.buyitbackend.service.MyAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyAddressServiceImpl implements MyAddressService {

    @Autowired
    private MyAddressMapper myAddressMapper;

    @Override
    public List<MyAddress> getAllAddressByAccount(Integer account) {
        return myAddressMapper.getAllAddressByAccount(account);
    }

    @Override
    public boolean delAddressById(Integer id) {
        return myAddressMapper.delAddressById(id) >0;
    }

    @Override
    public boolean updateAddress(MyAddress myAddress) {
        return myAddressMapper.updateAddress(myAddress) >0;
    }

    @Override
    public boolean addAddress(MyAddress myAddress) {
        return myAddressMapper.addAddress(myAddress) >0;
    }
}
