# 2022年ArkUI入门开发者训练营结营作品提交


## 简介

欢迎你上传你的ArkUI训练营结营作品，记录下你的OpenHarmony学习之路吧！

另外无论是收获还是吐槽或者是建议，我们都期待着你的反馈哦！




## 具体提交步骤

我们需要通过git的方式上传到我们代码仓库下：

**特别提示：如果你从未访问过相关gitee网站/git工具使用经验， 请先学习一下FAQ1 章节中提到的相关文档。**

1、访问[本仓库地址](https://gitee.com/openharmony-sig/contest)，登录你的gitee账号，fork本仓库;

2、在你的本地PC上，使用git windows/linux 工具克隆你个人的contest 仓库； 

3、在contest/2022_ArkUI_Bootcamp 目录下新建一个，以你们团队名称命名的文件夹，例如：知识体系工作组就新建了”knowledge_team"的文件夹

4、将你代码作品 或者心得或问题建议 拷贝到步骤3中的目录下：

**特别注意：上传的代码中不要有压缩包和 二进制文件，否则会造成dco 检测失败**

心得文件命名规则： 你的gitee账号_日期.md，例如“kenio_zhang_7_7.md"

gitee账号可以通过访问你的gitee 个人中心查看，如下图：

&nbsp;<img src="../media/gitee_account.png" style="zoom:67%;" />

相关心得文件格式可以参考：[心得文件参考](./knowledge_team/kenio_zhang_7_7.md)，当然你也可以自由发挥。

特别注意：文件格式是 md 文件， 你可以使用typora 软件来进行相关md文档的编辑。

5、发起你提交PR；



特别注意：在发起PR 提交前请特别注意FAQ1 中"PR提交文档"中提到的 签署 dco 协议和 添加signed-off信息， 如果这两点中有一点没有完成，仓库的CI门禁会提示dco 检查失败，从而拒绝你的提交。



## FAQ

### 1、我是一个小白，如何在本仓库上提交学习心得？

1）如果你之前从来没有访问过gitee，请参考[gitee 账号创建文档](https://gitee.com/openharmony-sig/knowledge/blob/master/docs/openharmony_getstarted/register_account/readme.md)。

2）如果你有了gitee账号，没有提交过PR，请参考[PR提交文档](https://gitee.com/openharmony-sig/knowledge/tree/master/docs/openharmony_getstarted/push_pr)。与文档中不同的地方只在于你fork 和提交的仓库不一样，我们本次提交的仓库在[这里](https://gitee.com/openharmony-sig/online_event)

### 2、我有一个开发样例，怎么贡献给到OpenHarmony知识体系？

如果同学们基于OpenHarmony开发了一些样例，希望更多人看到。 请参考[相关文档](https://gitee.com/openharmony-sig/knowledge/tree/master/docs/openharmony_getstarted/co-helloworld_demos)将样例贡献到OpenHaromny知识体系中来，优秀的样例将获得OpenHarmony官网展示的机会哦！