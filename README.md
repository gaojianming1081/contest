# contest

## 介绍
本仓库用于托管赛事作品及代码

### 进行中的活动
#### [2022OpenHarmony组件大赛](https://gitee.com/openharmony-sig/contest/tree/master/2022-OpenHarmony-component-contest)

![输入图片说明](./media/without_qr_poster.jpg)



#### 2022海思嵌入式大赛

海思嵌入式大赛简介：

嵌入式芯片与系统设计竞赛作为全国普通高校大学生竞赛榜单内的重点竞赛项目，自创立至今持续服务国家嵌入式芯片与相关应用产业的发展大局，已成为国内孵化培育学生创新设计与工程实践能力的关键平台。2021年是上海海思思赛道元年，共吸引到**70+**所高等院校的**900+**名大学生报名参赛，最终诞生了**23**个获奖作品。通过上海海思专家赋能、暑期训练营等一系列活动，参赛学生不断提升专业知识和工程实践能力，为未来的职业生涯打下了扎实基础。

精彩的嵌入式大赛离不开广大开发者的参与，为了丰富OpenHarmony生态建设,特邀请各位参赛者将赛事作品提交到社区，为社区繁荣做出自己的贡献。虽然每一个开发者的力量是微小的，但是集众人之力一定会推动OpenHarmony的车轮滚滚向前！

+ [赛事官网](http://www.socchina.net/details?id=f00d29311eac4ffab83d29fb7b760f54)
+ [赛事作品提交说明](2022-hisilicon-national-embedded-competition/README_zh.md)

#### 2022全国大学生物联网设计竞赛_哈工大（深圳）鸿蒙揭榜赛

+ [赛事说明](./2022-HIT(Shenzhen)_OpenHarmony_unveiling_competition/README_zh.md)
+ [揭榜赛赛题解读](https://developer.huawei.com/consumer/cn/training/course/video/C101656641429201028)



#### 2022年九联科技&惠州学院&OpenHarmony共建样例活动

- [样例活动作品提交说明](./2022_Unionman_HZU_OpenHarmony_Demos/README.md)



#### 2022年软通动力&桂林电子科技大学OpenHarmony训练营

- [样例活动作品提交说明](./2022-GUET_and_isoftstone/README.md)



#### 2022年传智&立创EDA_OpenHarmony机械狗训练营

- [样例活动作品提交说明](./2022_itcast_LCEDA_OpenHarmony_camp/README.md)
