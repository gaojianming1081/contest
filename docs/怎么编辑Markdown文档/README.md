# 怎么编辑Markdown文档

### 简介

Markdown 是一种轻量级标记语言，它允许人们使用易读易写的纯文本格式编写文档，于 2004 由John Gruber创建。

Markdown 语言在 2004 由约翰·格鲁伯（英语：John Gruber）创建。

Markdown 编写的文档可以导出 HTML 、Word、图像、PDF、Epub 等多种格式的文档，常用于编写项目readme、技术文档和电子书等。

### Markdown编辑器

##### Typora

Typora 是一款由 Abner Lee 开发的轻量级 Markdown 编辑器，采用所见即所得的编辑方式，实现了即时预览的功能，但也可切换至源代码编辑模式 。

![typora](./media/typora.png)

下载链接

- Typora官网： https://typora.io/
- 百度网盘：https://pan.baidu.com/s/1a7ZMb1Brs6RCpdB929eyIA?pwd=n3rc 
- 阿里网盘：https://www.aliyundrive.com/s/SKwM3jsvMgN

##### VSCode Markdown插件

VSCode（全称：Visual Studio Code）是一款由微软开发且跨平台的免费源代码编辑器，是一款许多程序员常用的代码编辑器，在扩展插件中搜索Markdown，安装Markdown All in one/Markdown preview enchance/Markdown PDF插件即可让VSCode拥有编辑Markdown能力。

![typora](./media/vscode_markdown.png)



![typora](./media/vscode_edit.png)

### 怎么设置标题格式

- 使用#号跟随标题内容即可设置标题，可使用1-6个#号表示1-6级标题；

```
# 一级标题
## 二级标题
### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题
```

### ![atom](./media/title.gif)

- 或者在编辑器中选中标题内容，点击工具栏段落->一级标题；

（本文编辑器操作以Typora截图作示例图）

![setTitle](./media/setTitle.png)

### 怎么链接网页

- 输入如下链接语法进行链接网页；

```
[链接名称](链接地址)

或者

<链接地址>
```

- 或者在编辑器中右键选择链接，填入链接名称和链接地址；

![link](./media/link.gif)

### 怎么链接图片

- 输入如下链接语法进行图片链接；

  ```
  ![alt 属性文本](图片地址)

  ![alt 属性文本](图片地址 "可选标题")
  ```


- 或者在编辑器中右键插入->图像，填入图片地址和alt提示属性文本；

![image](./media/image.gif)

### 怎么加粗文字

- 使用前后各两个*号将文字包裹起来即可加粗文字；

  ```
  **待加粗文字**
  ```

- 或者在编辑器中右键选择加粗，并输入文本；

![Thickening](./media/Thickening.gif)

### 怎么加入列表

- 在*号、+号、-号后加空格即可使用无序列表，在数字加上.号即可加入有序列表；

  ```
  * 无序列表
  + 无序列表
  - 无序列表
  1. 有序列表
  ```


- 或者在编辑器中右键选择对应的列表类型即可生成对应列表；

![list](./media/list.gif)

### 怎么插入代码

- 前后使用3个~号包裹代码片段，并可在开头3个~后指定一种语言（可选）；

  ```
  ​```javascript
  	// 代码片段
  	console.info("Hello OpenHarmony-SIG Knowledge")
  ​```
  ```

- 或者在编辑器中右键插入->代码块,输入代码片段并选择语言；

![code](./media/code.gif)





