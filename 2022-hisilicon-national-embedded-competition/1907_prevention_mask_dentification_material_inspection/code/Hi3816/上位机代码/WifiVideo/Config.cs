using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace WifiVideo
{
	public class Config : Form
	{
		public string FileName;

		private IContainer components;

		private Button button1;

		private Button button2;

		private GroupBox groupBox1;

		private TextBox txtEngineDown;

		private TextBox txtEngineUp;

		private Label label10;

		private Label label9;

		private TextBox txtBackward;

		private TextBox txtStop;

		private TextBox txtLeft;

		private TextBox txtRight;

		private TextBox txtForward;

		private Label label8;

		private Label label7;

		private Label label6;

		private Label label5;

		private Label label4;

		private GroupBox groupBox2;

		private Label label3;

		private Label label2;

		private Label label1;

		private TextBox textBoxControlPort;

		private TextBox textControlURL;

		private TextBox textBoxVideo;

		private Label label12;

		private Label label11;

		private TextBox txtledoff;

		private TextBox txtledon;

		private TextBox txtEngineright;

		private TextBox txtEngineleft;

		private GroupBox groupBox3;

		private Label label14;

		private Label label13;

		private GroupBox groupBox4;

		private Label label22;

		private Label label21;

		private Label label20;

		private Label label19;

		private Label label18;

		private Label label17;

		private Label label16;

		private Label label15;

		private TextBox txtRight4;

		private TextBox txtRight3;

		private TextBox txtRight2;

		private TextBox txtRight1;

		private TextBox txtLeft4;

		private TextBox txtLeft3;

		private TextBox txtLeft2;

		private TextBox txtLeft1;

		public Config()
		{
			this.InitializeComponent();
		}

		[DllImport("kernel32")]
		private static extern bool WritePrivateProfileString(string section, string key, string val, string filePath);

		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section, string key, string def, byte[] retVal, int size, string filePath);

		private void Config_Load(object sender, EventArgs e)
		{
			this.GetIni();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.WriteIni("VideoUrl", "videourl", this.textBoxVideo.Text);
			this.WriteIni("ControlUrl", "controlUrl", this.textControlURL.Text);
			this.WriteIni("ControlPort", "controlPort", this.textBoxControlPort.Text);
			this.WriteIni("ControlCommand", "CMD_Forward", this.txtForward.Text);
			this.WriteIni("ControlCommand", "CMD_Backward", this.txtBackward.Text);
			this.WriteIni("ControlCommand", "CMD_TurnLeft", this.txtLeft.Text);
			this.WriteIni("ControlCommand", "CMD_TurnRight", this.txtRight.Text);
			this.WriteIni("ControlCommand", "CMD_ClampOpen", this.txtLeft1.Text);
			this.WriteIni("ControlCommand", "CMD_ClampClose", this.txtRight1.Text);
			this.WriteIni("ControlCommand", "CMD_Arm_A_Up", this.txtLeft2.Text);
			this.WriteIni("ControlCommand", "CMD_Arm_A_Down", this.txtRight2.Text);
			this.WriteIni("ControlCommand", "CMD_Arm_B_Up", this.txtLeft3.Text);
			this.WriteIni("ControlCommand", "CMD_Arm_B_Down", this.txtRight3.Text);
			this.WriteIni("ControlCommand", "CMD_BottomLeft", this.txtLeft4.Text);
			this.WriteIni("ControlCommand", "CMD_BottomRight", this.txtRight4.Text);
			this.WriteIni("ControlCommand", "CMD_Stop", this.txtStop.Text);
			this.WriteIni("ControlCommand", "CMD_EngineUp", this.txtEngineUp.Text);
			this.WriteIni("ControlCommand", "CMD_EngineDown", this.txtEngineDown.Text);
			this.WriteIni("ControlCommand", "CMD_Engineleft", this.txtEngineleft.Text);
			this.WriteIni("ControlCommand", "CMD_Engineright", this.txtEngineright.Text);
			this.WriteIni("ControlCommand", "CMD_ledon", this.txtledon.Text);
			this.WriteIni("ControlCommand", "CMD_ledoff", this.txtledoff.Text);
			MessageBox.Show("配置成功！请重启程序以使配置生效。", "配置信息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			base.Close();
		}

		public void WriteIni(string Section, string Ident, string Value)
		{
			if (!Config.WritePrivateProfileString(Section, Ident, Value, this.FileName))
			{
				throw new ApplicationException("写入配置文件出错");
			}
		}

		public string ReadIni(string Section, string Ident, string Default)
		{
			byte[] array = new byte[65535];
			int privateProfileString = Config.GetPrivateProfileString(Section, Ident, Default, array, array.GetUpperBound(0), this.FileName);
			return Encoding.GetEncoding(0).GetString(array).Substring(0, privateProfileString).Trim();
		}

		private void GetIni()
		{
			this.FileName = Application.StartupPath + "\\Config.ini";
			this.textBoxVideo.Text = this.ReadIni("VideoUrl", "videourl", "");
			this.textControlURL.Text = this.ReadIni("ControlUrl", "controlUrl", "");
			this.textBoxControlPort.Text = this.ReadIni("ControlPort", "controlPort", "");
			this.txtForward.Text = this.ReadIni("ControlCommand", "CMD_Forward", "");
			this.txtBackward.Text = this.ReadIni("ControlCommand", "CMD_Backward", "");
			this.txtLeft.Text = this.ReadIni("ControlCommand", "CMD_TurnLeft", "");
			this.txtRight.Text = this.ReadIni("ControlCommand", "CMD_TurnRight", "");
			this.txtLeft1.Text = this.ReadIni("ControlCommand", "CMD_ClampOpen", "");
			this.txtRight1.Text = this.ReadIni("ControlCommand", "CMD_ClampClose", "");
			this.txtLeft2.Text = this.ReadIni("ControlCommand", "CMD_Arm_A_Up", "");
			this.txtRight2.Text = this.ReadIni("ControlCommand", "CMD_Arm_A_Down", "");
			this.txtLeft3.Text = this.ReadIni("ControlCommand", "CMD_Arm_B_Up", "");
			this.txtRight3.Text = this.ReadIni("ControlCommand", "CMD_Arm_B_Down", "");
			this.txtLeft4.Text = this.ReadIni("ControlCommand", "CMD_BottomLeft", "");
			this.txtRight4.Text = this.ReadIni("ControlCommand", "CMD_BottomRight", "");
			this.txtStop.Text = this.ReadIni("ControlCommand", "CMD_Stop", "");
			this.txtEngineUp.Text = this.ReadIni("ControlCommand", "CMD_EngineUp", "");
			this.txtEngineDown.Text = this.ReadIni("ControlCommand", "CMD_EngineDown", "");
			this.txtEngineleft.Text = this.ReadIni("ControlCommand", "CMD_Engineleft", "");
			this.txtEngineright.Text = this.ReadIni("ControlCommand", "CMD_Engineright", "");
			this.txtledon.Text = this.ReadIni("ControlCommand", "CMD_ledon", "");
			this.txtledoff.Text = this.ReadIni("ControlCommand", "CMD_ledoff", "");
		}

		private void button2_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void groupBox1_Enter(object sender, EventArgs e)
		{
		}

		private void txtForward_TextChanged(object sender, EventArgs e)
		{
		}

		private void txtLeft2_TextChanged(object sender, EventArgs e)
		{
		}

		private void label9_Click(object sender, EventArgs e)
		{
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(Config));
			this.button1 = new Button();
			this.button2 = new Button();
			this.groupBox1 = new GroupBox();
			this.label12 = new Label();
			this.label11 = new Label();
			this.txtledoff = new TextBox();
			this.txtledon = new TextBox();
			this.txtEngineright = new TextBox();
			this.txtEngineleft = new TextBox();
			this.txtEngineDown = new TextBox();
			this.txtEngineUp = new TextBox();
			this.label10 = new Label();
			this.label9 = new Label();
			this.txtBackward = new TextBox();
			this.txtStop = new TextBox();
			this.txtLeft = new TextBox();
			this.txtRight = new TextBox();
			this.txtForward = new TextBox();
			this.label8 = new Label();
			this.label7 = new Label();
			this.label6 = new Label();
			this.label5 = new Label();
			this.label4 = new Label();
			this.groupBox2 = new GroupBox();
			this.label3 = new Label();
			this.label2 = new Label();
			this.label1 = new Label();
			this.textBoxControlPort = new TextBox();
			this.textControlURL = new TextBox();
			this.textBoxVideo = new TextBox();
			this.groupBox3 = new GroupBox();
			this.label13 = new Label();
			this.label14 = new Label();
			this.groupBox4 = new GroupBox();
			this.txtLeft1 = new TextBox();
			this.txtLeft2 = new TextBox();
			this.txtLeft3 = new TextBox();
			this.txtLeft4 = new TextBox();
			this.txtRight1 = new TextBox();
			this.txtRight2 = new TextBox();
			this.txtRight3 = new TextBox();
			this.txtRight4 = new TextBox();
			this.label15 = new Label();
			this.label16 = new Label();
			this.label17 = new Label();
			this.label18 = new Label();
			this.label19 = new Label();
			this.label20 = new Label();
			this.label21 = new Label();
			this.label22 = new Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			base.SuspendLayout();
			this.button1.Location = new Point(23, 305);
			this.button1.Name = "button1";
			this.button1.Size = new Size(126, 57);
			this.button1.TabIndex = 6;
			this.button1.Text = "保存";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new EventHandler(this.button1_Click);
			this.button2.Location = new Point(202, 305);
			this.button2.Name = "button2";
			this.button2.Size = new Size(123, 57);
			this.button2.TabIndex = 7;
			this.button2.Text = "取消";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new EventHandler(this.button2_Click);
			this.groupBox1.Controls.Add(this.label12);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.txtledoff);
			this.groupBox1.Controls.Add(this.txtledon);
			this.groupBox1.Controls.Add(this.txtBackward);
			this.groupBox1.Controls.Add(this.txtStop);
			this.groupBox1.Controls.Add(this.txtLeft);
			this.groupBox1.Controls.Add(this.txtRight);
			this.groupBox1.Controls.Add(this.txtForward);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Location = new Point(12, 107);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new Size(395, 157);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "指令设置";
			this.groupBox1.Enter += new EventHandler(this.groupBox1_Enter);
			this.label12.AutoSize = true;
			this.label12.Location = new Point(256, 5);
			this.label12.Name = "label12";
			this.label12.Size = new Size(29, 12);
			this.label12.TabIndex = 45;
			this.label12.Text = "关灯";
			this.label11.AutoSize = true;
			this.label11.Location = new Point(196, 5);
			this.label11.Name = "label11";
			this.label11.Size = new Size(29, 12);
			this.label11.TabIndex = 44;
			this.label11.Text = "开灯";
			this.txtledoff.Location = new Point(258, 20);
			this.txtledoff.Name = "txtledoff";
			this.txtledoff.Size = new Size(31, 21);
			this.txtledoff.TabIndex = 43;
			this.txtledon.Location = new Point(212, 20);
			this.txtledon.Name = "txtledon";
			this.txtledon.Size = new Size(31, 21);
			this.txtledon.TabIndex = 42;
			this.txtEngineright.Location = new Point(111, 76);
			this.txtEngineright.Name = "txtEngineright";
			this.txtEngineright.Size = new Size(50, 21);
			this.txtEngineright.TabIndex = 39;
			this.txtEngineleft.Location = new Point(8, 77);
			this.txtEngineleft.Name = "txtEngineleft";
			this.txtEngineleft.Size = new Size(46, 21);
			this.txtEngineleft.TabIndex = 38;
			this.txtEngineDown.Location = new Point(57, 100);
			this.txtEngineDown.Name = "txtEngineDown";
			this.txtEngineDown.Size = new Size(49, 21);
			this.txtEngineDown.TabIndex = 35;
			this.txtEngineUp.Location = new Point(59, 51);
			this.txtEngineUp.Name = "txtEngineUp";
			this.txtEngineUp.Size = new Size(49, 21);
			this.txtEngineUp.TabIndex = 34;
			this.label10.AutoSize = true;
			this.label10.Location = new Point(63, 128);
			this.label10.Name = "label10";
			this.label10.Size = new Size(41, 12);
			this.label10.TabIndex = 33;
			this.label10.Text = "舵机下";
			this.label9.AutoSize = true;
			this.label9.Location = new Point(64, 34);
			this.label9.Name = "label9";
			this.label9.Size = new Size(41, 12);
			this.label9.TabIndex = 32;
			this.label9.Text = "舵机上";
			this.label9.Click += new EventHandler(this.label9_Click);
			this.txtBackward.Location = new Point(106, 98);
			this.txtBackward.Name = "txtBackward";
			this.txtBackward.Size = new Size(31, 21);
			this.txtBackward.TabIndex = 31;
			this.txtStop.Location = new Point(106, 71);
			this.txtStop.Name = "txtStop";
			this.txtStop.Size = new Size(31, 21);
			this.txtStop.TabIndex = 30;
			this.txtLeft.Location = new Point(69, 72);
			this.txtLeft.Name = "txtLeft";
			this.txtLeft.Size = new Size(31, 21);
			this.txtLeft.TabIndex = 29;
			this.txtRight.Location = new Point(143, 72);
			this.txtRight.Name = "txtRight";
			this.txtRight.Size = new Size(31, 21);
			this.txtRight.TabIndex = 28;
			this.txtForward.Location = new Point(106, 39);
			this.txtForward.Name = "txtForward";
			this.txtForward.Size = new Size(31, 21);
			this.txtForward.TabIndex = 27;
			this.txtForward.TextChanged += new EventHandler(this.txtForward_TextChanged);
			this.label8.AutoSize = true;
			this.label8.Location = new Point(71, 101);
			this.label8.Name = "label8";
			this.label8.Size = new Size(29, 12);
			this.label8.TabIndex = 26;
			this.label8.Text = "停止";
			this.label7.AutoSize = true;
			this.label7.Location = new Point(188, 75);
			this.label7.Name = "label7";
			this.label7.Size = new Size(29, 12);
			this.label7.TabIndex = 25;
			this.label7.Text = "右转";
			this.label6.AutoSize = true;
			this.label6.Location = new Point(29, 75);
			this.label6.Name = "label6";
			this.label6.Size = new Size(29, 12);
			this.label6.TabIndex = 24;
			this.label6.Text = "左转";
			this.label5.AutoSize = true;
			this.label5.Location = new Point(108, 130);
			this.label5.Name = "label5";
			this.label5.Size = new Size(29, 12);
			this.label5.TabIndex = 23;
			this.label5.Text = "后退";
			this.label4.AutoSize = true;
			this.label4.Location = new Point(108, 18);
			this.label4.Name = "label4";
			this.label4.Size = new Size(29, 12);
			this.label4.TabIndex = 22;
			this.label4.Text = "前进";
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.textBoxControlPort);
			this.groupBox2.Controls.Add(this.textControlURL);
			this.groupBox2.Controls.Add(this.textBoxVideo);
			this.groupBox2.Location = new Point(12, 18);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new Size(395, 83);
			this.groupBox2.TabIndex = 9;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "WIFI设置";
			this.label3.AutoSize = true;
			this.label3.Location = new Point(236, 55);
			this.label3.Name = "label3";
			this.label3.Size = new Size(53, 12);
			this.label3.TabIndex = 11;
			this.label3.Text = "控制端口";
			this.label2.AutoSize = true;
			this.label2.Location = new Point(9, 55);
			this.label2.Name = "label2";
			this.label2.Size = new Size(53, 12);
			this.label2.TabIndex = 10;
			this.label2.Text = "控制地址";
			this.label1.AutoSize = true;
			this.label1.Location = new Point(9, 25);
			this.label1.Name = "label1";
			this.label1.Size = new Size(53, 12);
			this.label1.TabIndex = 9;
			this.label1.Text = "视频地址";
			this.textBoxControlPort.Location = new Point(295, 50);
			this.textBoxControlPort.Name = "textBoxControlPort";
			this.textBoxControlPort.Size = new Size(91, 21);
			this.textBoxControlPort.TabIndex = 8;
			this.textControlURL.Location = new Point(68, 50);
			this.textControlURL.Name = "textControlURL";
			this.textControlURL.Size = new Size(131, 21);
			this.textControlURL.TabIndex = 7;
			this.textBoxVideo.Location = new Point(68, 21);
			this.textBoxVideo.Name = "textBoxVideo";
			this.textBoxVideo.Size = new Size(318, 21);
			this.textBoxVideo.TabIndex = 6;
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.label13);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Controls.Add(this.txtEngineUp);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.txtEngineDown);
			this.groupBox3.Controls.Add(this.txtEngineright);
			this.groupBox3.Controls.Add(this.txtEngineleft);
			this.groupBox3.Location = new Point(413, 32);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new Size(168, 154);
			this.groupBox3.TabIndex = 10;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "舵机云台设置";
			this.label13.AutoSize = true;
			this.label13.Location = new Point(8, 106);
			this.label13.Name = "label13";
			this.label13.Size = new Size(41, 12);
			this.label13.TabIndex = 40;
			this.label13.Text = "舵机左";
			this.label14.AutoSize = true;
			this.label14.Location = new Point(113, 100);
			this.label14.Name = "label14";
			this.label14.Size = new Size(41, 12);
			this.label14.TabIndex = 41;
			this.label14.Text = "舵机右";
			this.groupBox4.Controls.Add(this.label22);
			this.groupBox4.Controls.Add(this.label21);
			this.groupBox4.Controls.Add(this.label20);
			this.groupBox4.Controls.Add(this.label19);
			this.groupBox4.Controls.Add(this.label18);
			this.groupBox4.Controls.Add(this.label17);
			this.groupBox4.Controls.Add(this.label16);
			this.groupBox4.Controls.Add(this.label15);
			this.groupBox4.Controls.Add(this.txtRight4);
			this.groupBox4.Controls.Add(this.txtRight3);
			this.groupBox4.Controls.Add(this.txtRight2);
			this.groupBox4.Controls.Add(this.txtRight1);
			this.groupBox4.Controls.Add(this.txtLeft4);
			this.groupBox4.Controls.Add(this.txtLeft3);
			this.groupBox4.Controls.Add(this.txtLeft2);
			this.groupBox4.Controls.Add(this.txtLeft1);
			this.groupBox4.Location = new Point(413, 193);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new Size(168, 185);
			this.groupBox4.TabIndex = 11;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "机械臂控制";
			this.txtLeft1.Location = new Point(5, 35);
			this.txtLeft1.Name = "txtLeft1";
			this.txtLeft1.Size = new Size(61, 21);
			this.txtLeft1.TabIndex = 0;
			this.txtLeft2.Location = new Point(5, 74);
			this.txtLeft2.Name = "txtLeft2";
			this.txtLeft2.Size = new Size(62, 21);
			this.txtLeft2.TabIndex = 1;
			this.txtLeft3.Location = new Point(5, 110);
			this.txtLeft3.Name = "txtLeft3";
			this.txtLeft3.Size = new Size(62, 21);
			this.txtLeft3.TabIndex = 2;
			this.txtLeft4.Location = new Point(6, 148);
			this.txtLeft4.Name = "txtLeft4";
			this.txtLeft4.Size = new Size(61, 21);
			this.txtLeft4.TabIndex = 3;
			this.txtRight1.Location = new Point(93, 35);
			this.txtRight1.Name = "txtRight1";
			this.txtRight1.Size = new Size(61, 21);
			this.txtRight1.TabIndex = 4;
			this.txtRight2.Location = new Point(93, 74);
			this.txtRight2.Name = "txtRight2";
			this.txtRight2.Size = new Size(61, 21);
			this.txtRight2.TabIndex = 5;
			this.txtRight3.Location = new Point(93, 110);
			this.txtRight3.Name = "txtRight3";
			this.txtRight3.Size = new Size(61, 21);
			this.txtRight3.TabIndex = 6;
			this.txtRight4.Location = new Point(93, 148);
			this.txtRight4.Name = "txtRight4";
			this.txtRight4.Size = new Size(61, 21);
			this.txtRight4.TabIndex = 7;
			this.label15.AutoSize = true;
			this.label15.Location = new Point(7, 21);
			this.label15.Name = "label15";
			this.label15.Size = new Size(53, 12);
			this.label15.TabIndex = 8;
			this.label15.Text = "手抓张开";
			this.label16.AutoSize = true;
			this.label16.Location = new Point(97, 20);
			this.label16.Name = "label16";
			this.label16.Size = new Size(53, 12);
			this.label16.TabIndex = 9;
			this.label16.Text = "手抓闭合";
			this.label17.AutoSize = true;
			this.label17.Location = new Point(8, 59);
			this.label17.Name = "label17";
			this.label17.Size = new Size(53, 12);
			this.label17.TabIndex = 10;
			this.label17.Text = "上臂上升";
			this.label18.AutoSize = true;
			this.label18.Location = new Point(93, 59);
			this.label18.Name = "label18";
			this.label18.Size = new Size(53, 12);
			this.label18.TabIndex = 11;
			this.label18.Text = "上臂下降";
			this.label19.AutoSize = true;
			this.label19.Location = new Point(7, 97);
			this.label19.Name = "label19";
			this.label19.Size = new Size(53, 12);
			this.label19.TabIndex = 12;
			this.label19.Text = "下臂上升";
			this.label20.AutoSize = true;
			this.label20.Location = new Point(95, 97);
			this.label20.Name = "label20";
			this.label20.Size = new Size(53, 12);
			this.label20.TabIndex = 13;
			this.label20.Text = "下臂下降";
			this.label21.AutoSize = true;
			this.label21.Location = new Point(8, 133);
			this.label21.Name = "label21";
			this.label21.Size = new Size(53, 12);
			this.label21.TabIndex = 14;
			this.label21.Text = "底盘左转";
			this.label22.AutoSize = true;
			this.label22.Location = new Point(99, 134);
			this.label22.Name = "label22";
			this.label22.Size = new Size(53, 12);
			this.label22.TabIndex = 15;
			this.label22.Text = "底盘右转";
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(634, 409);
			base.Controls.Add(this.groupBox4);
			base.Controls.Add(this.groupBox3);
			base.Controls.Add(this.groupBox2);
			base.Controls.Add(this.groupBox1);
			base.Controls.Add(this.button2);
			base.Controls.Add(this.button1);
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "Config";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "设置";
			base.Load += new EventHandler(this.Config_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			base.ResumeLayout(false);
		}
	}
}
