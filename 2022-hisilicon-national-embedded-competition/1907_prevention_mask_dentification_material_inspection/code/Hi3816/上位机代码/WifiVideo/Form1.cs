using System;
using System.ComponentModel;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace WifiVideo
{
	public class Form1 : Form
	{
		private static string FileName = Application.StartupPath + "\\Config.ini";

		private string CameraIp = "";

		private string ControlIp = "192.168.1.1";

		private string Port = "81";

		private string CMD_Forward = "";

		private string CMD_Backward = "";

		private string CMD_TurnLeft = "";

		private string CMD_TurnRight = "";

		private string CMD_TurnLeft1 = "";

		private string CMD_TurnRight1 = "";

		private string CMD_TurnLeft2 = "";

		private string CMD_TurnRight2 = "";

		private string CMD_Stop = "";

		private string CMD_EngineUp = "";

		private string CMD_EngineDown = "";

		private string CMD_Engineleft = "";

		private string CMD_Engineright = "";

		private string CMD_ledon = "";

		private string CMD_ledoff = "";

		private string CMD_Arm_B_Up = "";

		private string CMD_Arm_B_Down = "";

		private string CMD_BottomLeft = "";

		private string CMD_BottomRight = "";

		private IContainer components;

		private PictureBox pictureBox1;

		private Timer timer1;

		private GroupBox groupBox1;

		private Button btnEngineDown;

		private Button btnEngineUp;

		private Button buttonStop;

		private Button button6;

		private Button buttonBackward;

		private Button buttonRight;

		private Button buttonLeft;

		private Button buttonForward;

		private Button button1;

		private Button buttonRight1;

		private Button buttonLeft1;

		private Button ledoff;

		private Button ledon;

		private Button btnEngineright;

		private Button btnEngineleft;

		private Button buttonRight2;

		private Button buttonLeft2;

		private GroupBox groupBox2;

		private GroupBox groupBox3;

		private GroupBox groupBox4;

		private Button button3;

		private Button button2;

		private PictureBox pictureBox2;

		private Label label1;

		private GroupBox groupBox5;

		public Form1()
		{
			this.InitializeComponent();
		}

		[DllImport("kernel32")]
		private static extern bool WritePrivateProfileString(string section, string key, string val, string filePath);

		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section, string key, string def, byte[] retVal, int size, string filePath);

		public string ReadIni(string Section, string Ident, string Default)
		{
			byte[] array = new byte[65535];
			int privateProfileString = Form1.GetPrivateProfileString(Section, Ident, Default, array, array.GetUpperBound(0), Form1.FileName);
			return Encoding.GetEncoding(0).GetString(array).Substring(0, privateProfileString).Trim();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.timer1.Enabled = true;
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			this.pictureBox1.ImageLocation = this.CameraIp;
		}

		private void SendData(string data)
		{
			try
			{
				IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(this.ControlIp.ToString()), Convert.ToInt32(this.Port.ToString()));
				Socket expr_2E = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				expr_2E.Connect(remoteEP);
				byte[] bytes = Encoding.ASCII.GetBytes(data);
				expr_2E.Send(bytes, bytes.Length, SocketFlags.None);
				expr_2E.Close();
			}
			catch (Exception arg_54_0)
			{
				MessageBox.Show(arg_54_0.Message);
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_Forward);
		}

		private void button5_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_Backward);
		}

		private void button3_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_TurnLeft);
		}

		private void button4_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_TurnRight);
		}

		private void button10_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_TurnLeft1);
		}

		private void button11_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_TurnRight1);
		}

		private void button12_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_TurnLeft2);
		}

		private void button13_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_TurnRight2);
		}

		private void button2_Click_1(object sender, EventArgs e)
		{
			this.SendData(this.CMD_BottomLeft);
		}

		private void groupBox1_Enter_1(object sender, EventArgs e)
		{
		}

		private void button3_Click_2(object sender, EventArgs e)
		{
			this.SendData(this.CMD_BottomRight);
		}

		private void button6_Click(object sender, EventArgs e)
		{
			new Config().ShowDialog();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			this.GetIni();
			this.buttonForward.BackColor = Color.LightBlue;
			this.buttonBackward.BackColor = Color.LightBlue;
			this.buttonLeft.BackColor = Color.LightBlue;
			this.buttonRight.BackColor = Color.LightBlue;
			this.buttonLeft1.BackColor = Color.LightBlue;
			this.buttonRight1.BackColor = Color.LightBlue;
			this.buttonLeft2.BackColor = Color.LightBlue;
			this.buttonRight2.BackColor = Color.LightBlue;
			this.buttonStop.BackColor = Color.LightBlue;
			this.btnEngineUp.BackColor = Color.LightBlue;
			this.btnEngineDown.BackColor = Color.LightBlue;
			this.btnEngineleft.BackColor = Color.LightBlue;
			this.btnEngineright.BackColor = Color.LightBlue;
		}

		private void GetIni()
		{
			this.CameraIp = this.ReadIni("VideoUrl", "videoUrl", "");
			this.ControlIp = this.ReadIni("ControlUrl", "controlUrl", "");
			this.Port = this.ReadIni("ControlPort", "controlPort", "");
			this.CMD_Forward = this.ReadIni("ControlCommand", "CMD_Forward", "");
			this.CMD_Backward = this.ReadIni("ControlCommand", "CMD_Backward", "");
			this.CMD_TurnLeft = this.ReadIni("ControlCommand", "CMD_TurnLeft", "");
			this.CMD_TurnRight = this.ReadIni("ControlCommand", "CMD_TurnRight", "");
			this.CMD_TurnLeft1 = this.ReadIni("ControlCommand", "CMD_ClampOpen", "");
			this.CMD_TurnRight1 = this.ReadIni("ControlCommand", "CMD_ClampClose", "");
			this.CMD_TurnLeft2 = this.ReadIni("ControlCommand", "CMD_Arm_A_Up", "");
			this.CMD_TurnRight2 = this.ReadIni("ControlCommand", "CMD_Arm_A_Down", "");
			this.CMD_Arm_B_Up = this.ReadIni("ControlCommand", "CMD_Arm_B_Up", "");
			this.CMD_Arm_B_Down = this.ReadIni("ControlCommand", "CMD_Arm_B_Down", "");
			this.CMD_BottomLeft = this.ReadIni("ControlCommand", "CMD_BottomLeft", "");
			this.CMD_BottomRight = this.ReadIni("ControlCommand", "CMD_BottomRight", "");
			this.CMD_Stop = this.ReadIni("ControlCommand", "CMD_Stop", "");
			this.CMD_EngineUp = this.ReadIni("ControlCommand", "CMD_EngineUp", "");
			this.CMD_EngineDown = this.ReadIni("ControlCommand", "CMD_EngineDown", "");
			this.CMD_Engineleft = this.ReadIni("ControlCommand", "CMD_Engineleft", "");
			this.CMD_Engineright = this.ReadIni("ControlCommand", "CMD_Engineright", "");
			this.CMD_ledon = this.ReadIni("ControlCommand", "CMD_ledon", "");
			this.CMD_ledoff = this.ReadIni("ControlCommand", "CMD_ledoff", "");
		}

		private void Form1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.W)
			{
				this.buttonForward.BackColor = Color.DarkGray;
				this.buttonForward.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.S)
			{
				this.buttonBackward.BackColor = Color.DarkGray;
				this.buttonBackward.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.A)
			{
				this.buttonLeft.BackColor = Color.DarkGray;
				this.buttonLeft.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.D)
			{
				this.buttonRight.BackColor = Color.DarkGray;
				this.buttonRight.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.Q)
			{
				this.buttonLeft1.BackColor = Color.DarkGray;
				this.buttonLeft1.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.E)
			{
				this.buttonRight1.BackColor = Color.DarkGray;
				this.buttonRight1.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.Z)
			{
				this.buttonLeft2.BackColor = Color.DarkGray;
				this.buttonLeft2.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.C)
			{
				this.buttonRight2.BackColor = Color.DarkGray;
				this.buttonRight2.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.X)
			{
				this.buttonStop.BackColor = Color.DarkGray;
				this.buttonStop.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.I)
			{
				this.btnEngineUp.BackColor = Color.DarkGray;
				this.btnEngineUp.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.K)
			{
				this.btnEngineDown.BackColor = Color.DarkGray;
				this.btnEngineDown.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.J)
			{
				this.btnEngineleft.BackColor = Color.DarkGray;
				this.btnEngineleft.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.L)
			{
				this.btnEngineright.BackColor = Color.DarkGray;
				this.btnEngineright.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.N)
			{
				this.ledon.BackColor = Color.DarkGray;
				this.ledon.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.M)
			{
				this.ledoff.BackColor = Color.DarkGray;
				this.ledoff.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.R)
			{
				this.ledon.BackColor = Color.DarkGray;
				this.ledon.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.T)
			{
				this.ledoff.BackColor = Color.DarkGray;
				this.ledoff.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.F)
			{
				this.button2.BackColor = Color.DarkGray;
				this.button2.PerformClick();
				return;
			}
			if (e.KeyCode == Keys.G)
			{
				this.button3.BackColor = Color.DarkGray;
				this.button3.PerformClick();
			}
		}

		private void Form1_KeyUp(object sender, KeyEventArgs e)
		{
			this.buttonStop.PerformClick();
			this.buttonForward.BackColor = Color.LightBlue;
			this.buttonBackward.BackColor = Color.LightBlue;
			this.buttonLeft.BackColor = Color.LightBlue;
			this.buttonRight.BackColor = Color.LightBlue;
			this.buttonLeft1.BackColor = Color.LightBlue;
			this.buttonRight1.BackColor = Color.LightBlue;
			this.buttonLeft2.BackColor = Color.LightBlue;
			this.buttonRight2.BackColor = Color.LightBlue;
			this.button2.BackColor = Color.LightBlue;
			this.button3.BackColor = Color.LightBlue;
			this.btnEngineUp.BackColor = Color.LightBlue;
			this.btnEngineDown.BackColor = Color.LightBlue;
			this.btnEngineleft.BackColor = Color.LightBlue;
			this.btnEngineright.BackColor = Color.LightBlue;
			this.ledon.BackColor = Color.LightBlue;
			this.ledoff.BackColor = Color.LightBlue;
		}

		private void btnEngineUp_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_EngineUp);
		}

		private void btnEngineDown_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_EngineDown);
		}

		private void btnEngineleft_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_Engineleft);
		}

		private void btnEngineright_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_Engineright);
		}

		private void buttonStop_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_Stop);
		}

		private void ledon_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_Arm_B_Up);
		}

		private void ledoff_Click(object sender, EventArgs e)
		{
			this.SendData(this.CMD_Arm_B_Down);
		}

		private void button3_Click_1(object sender, EventArgs e)
		{
		}

		private void groupBox1_Enter(object sender, EventArgs e)
		{
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(Form1));
			this.pictureBox1 = new PictureBox();
			this.timer1 = new Timer(this.components);
			this.groupBox1 = new GroupBox();
			this.buttonStop = new Button();
			this.buttonBackward = new Button();
			this.buttonRight = new Button();
			this.buttonLeft = new Button();
			this.buttonForward = new Button();
			this.ledoff = new Button();
			this.ledon = new Button();
			this.btnEngineright = new Button();
			this.btnEngineleft = new Button();
			this.buttonRight2 = new Button();
			this.buttonLeft2 = new Button();
			this.buttonRight1 = new Button();
			this.buttonLeft1 = new Button();
			this.btnEngineDown = new Button();
			this.btnEngineUp = new Button();
			this.button6 = new Button();
			this.button1 = new Button();
			this.groupBox2 = new GroupBox();
			this.groupBox3 = new GroupBox();
			this.groupBox4 = new GroupBox();
			this.button3 = new Button();
			this.button2 = new Button();
			this.pictureBox2 = new PictureBox();
			this.label1 = new Label();
			this.groupBox5 = new GroupBox();
			((ISupportInitialize)this.pictureBox1).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			((ISupportInitialize)this.pictureBox2).BeginInit();
			this.groupBox5.SuspendLayout();
			base.SuspendLayout();
			this.pictureBox1.BorderStyle = BorderStyle.Fixed3D;
			this.pictureBox1.Image = (Image)componentResourceManager.GetObject("pictureBox1.Image");
			this.pictureBox1.Location = new Point(12, 8);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new Size(708, 472);
			this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.timer1.Interval = 50;
			this.timer1.Tick += new EventHandler(this.timer1_Tick);
			this.groupBox1.Controls.Add(this.buttonStop);
			this.groupBox1.Controls.Add(this.buttonBackward);
			this.groupBox1.Controls.Add(this.buttonRight);
			this.groupBox1.Controls.Add(this.buttonLeft);
			this.groupBox1.Controls.Add(this.buttonForward);
			this.groupBox1.Location = new Point(12, 486);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new Size(258, 186);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "操作指令";
			this.groupBox1.Enter += new EventHandler(this.groupBox1_Enter_1);
			this.buttonStop.Location = new Point(103, 79);
			this.buttonStop.Name = "buttonStop";
			this.buttonStop.Size = new Size(63, 42);
			this.buttonStop.TabIndex = 16;
			this.buttonStop.Text = "停";
			this.buttonStop.UseVisualStyleBackColor = true;
			this.buttonStop.Click += new EventHandler(this.buttonStop_Click);
			this.buttonBackward.Location = new Point(103, 124);
			this.buttonBackward.Name = "buttonBackward";
			this.buttonBackward.Size = new Size(63, 56);
			this.buttonBackward.TabIndex = 14;
			this.buttonBackward.Text = "后\r\n(S)";
			this.buttonBackward.UseVisualStyleBackColor = true;
			this.buttonBackward.Click += new EventHandler(this.button5_Click);
			this.buttonRight.ImageAlign = ContentAlignment.MiddleLeft;
			this.buttonRight.Location = new Point(172, 75);
			this.buttonRight.Name = "buttonRight";
			this.buttonRight.Size = new Size(80, 42);
			this.buttonRight.TabIndex = 13;
			this.buttonRight.Text = "右\r\n(D)";
			this.buttonRight.UseVisualStyleBackColor = true;
			this.buttonRight.Click += new EventHandler(this.button4_Click);
			this.buttonLeft.Location = new Point(18, 76);
			this.buttonLeft.Name = "buttonLeft";
			this.buttonLeft.Size = new Size(79, 42);
			this.buttonLeft.TabIndex = 12;
			this.buttonLeft.Text = "左\r\n(A)";
			this.buttonLeft.UseVisualStyleBackColor = true;
			this.buttonLeft.Click += new EventHandler(this.button3_Click);
			this.buttonForward.Location = new Point(103, 16);
			this.buttonForward.Name = "buttonForward";
			this.buttonForward.Size = new Size(63, 59);
			this.buttonForward.TabIndex = 11;
			this.buttonForward.Text = "前\r\n（W）";
			this.buttonForward.UseVisualStyleBackColor = true;
			this.buttonForward.Click += new EventHandler(this.button2_Click);
			this.ledoff.Location = new Point(126, 191);
			this.ledoff.Name = "ledoff";
			this.ledoff.Size = new Size(86, 46);
			this.ledoff.TabIndex = 26;
			this.ledoff.Text = "下臂下降\r\n(T)";
			this.ledoff.UseVisualStyleBackColor = true;
			this.ledoff.Click += new EventHandler(this.ledoff_Click);
			this.ledon.Location = new Point(11, 191);
			this.ledon.Name = "ledon";
			this.ledon.Size = new Size(96, 46);
			this.ledon.TabIndex = 25;
			this.ledon.Text = "下臂上升\r\n (R)";
			this.ledon.UseVisualStyleBackColor = true;
			this.ledon.Click += new EventHandler(this.ledon_Click);
			this.btnEngineright.Location = new Point(147, 65);
			this.btnEngineright.Name = "btnEngineright";
			this.btnEngineright.Size = new Size(63, 42);
			this.btnEngineright.TabIndex = 24;
			this.btnEngineright.Text = "舵机右(L)";
			this.btnEngineright.UseVisualStyleBackColor = true;
			this.btnEngineright.Click += new EventHandler(this.btnEngineright_Click);
			this.btnEngineleft.Location = new Point(9, 63);
			this.btnEngineleft.Name = "btnEngineleft";
			this.btnEngineleft.Size = new Size(63, 42);
			this.btnEngineleft.TabIndex = 23;
			this.btnEngineleft.Text = "舵机左(J)";
			this.btnEngineleft.UseVisualStyleBackColor = true;
			this.btnEngineleft.Click += new EventHandler(this.btnEngineleft_Click);
			this.buttonRight2.Location = new Point(126, 118);
			this.buttonRight2.Name = "buttonRight2";
			this.buttonRight2.Size = new Size(86, 47);
			this.buttonRight2.TabIndex = 22;
			this.buttonRight2.Text = "上臂下降\r\n(C)";
			this.buttonRight2.UseVisualStyleBackColor = true;
			this.buttonRight2.Click += new EventHandler(this.button13_Click);
			this.buttonLeft2.Location = new Point(11, 119);
			this.buttonLeft2.Name = "buttonLeft2";
			this.buttonLeft2.Size = new Size(96, 47);
			this.buttonLeft2.TabIndex = 21;
			this.buttonLeft2.Text = "上臂上升\r\n(Z)";
			this.buttonLeft2.UseVisualStyleBackColor = true;
			this.buttonLeft2.Click += new EventHandler(this.button12_Click);
			this.buttonRight1.Location = new Point(126, 44);
			this.buttonRight1.Name = "buttonRight1";
			this.buttonRight1.Size = new Size(86, 48);
			this.buttonRight1.TabIndex = 20;
			this.buttonRight1.Text = "手抓闭合\r\n(E)";
			this.buttonRight1.UseVisualStyleBackColor = true;
			this.buttonRight1.Click += new EventHandler(this.button11_Click);
			this.buttonLeft1.Location = new Point(16, 44);
			this.buttonLeft1.Name = "buttonLeft1";
			this.buttonLeft1.Size = new Size(91, 48);
			this.buttonLeft1.TabIndex = 19;
			this.buttonLeft1.Text = "手抓张开\r\n(Q)";
			this.buttonLeft1.UseVisualStyleBackColor = true;
			this.buttonLeft1.Click += new EventHandler(this.button10_Click);
			this.btnEngineDown.Location = new Point(80, 104);
			this.btnEngineDown.Name = "btnEngineDown";
			this.btnEngineDown.Size = new Size(63, 42);
			this.btnEngineDown.TabIndex = 18;
			this.btnEngineDown.Text = "舵机下(K)";
			this.btnEngineDown.UseVisualStyleBackColor = true;
			this.btnEngineDown.Click += new EventHandler(this.btnEngineDown_Click);
			this.btnEngineUp.Location = new Point(79, 23);
			this.btnEngineUp.Name = "btnEngineUp";
			this.btnEngineUp.Size = new Size(63, 42);
			this.btnEngineUp.TabIndex = 17;
			this.btnEngineUp.Text = "舵机上(I)";
			this.btnEngineUp.UseVisualStyleBackColor = true;
			this.btnEngineUp.Click += new EventHandler(this.btnEngineUp_Click);
			this.button6.Location = new Point(21, 22);
			this.button6.Name = "button6";
			this.button6.Size = new Size(75, 42);
			this.button6.TabIndex = 15;
			this.button6.Text = "设置";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new EventHandler(this.button6_Click);
			this.button1.Location = new Point(127, 25);
			this.button1.Name = "button1";
			this.button1.Size = new Size(75, 38);
			this.button1.TabIndex = 10;
			this.button1.Text = "视频";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new EventHandler(this.button1_Click);
			this.groupBox2.Controls.Add(this.button1);
			this.groupBox2.Controls.Add(this.button6);
			this.groupBox2.Location = new Point(740, 26);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new Size(224, 80);
			this.groupBox2.TabIndex = 27;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "系统设置";
			this.groupBox3.Controls.Add(this.btnEngineright);
			this.groupBox3.Controls.Add(this.btnEngineleft);
			this.groupBox3.Controls.Add(this.btnEngineDown);
			this.groupBox3.Controls.Add(this.btnEngineUp);
			this.groupBox3.Location = new Point(740, 147);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new Size(224, 166);
			this.groupBox3.TabIndex = 28;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "视觉云台控制";
			this.groupBox4.Controls.Add(this.button3);
			this.groupBox4.Controls.Add(this.button2);
			this.groupBox4.Controls.Add(this.buttonLeft1);
			this.groupBox4.Controls.Add(this.buttonLeft2);
			this.groupBox4.Controls.Add(this.ledon);
			this.groupBox4.Controls.Add(this.ledoff);
			this.groupBox4.Controls.Add(this.buttonRight2);
			this.groupBox4.Controls.Add(this.buttonRight1);
			this.groupBox4.Location = new Point(740, 348);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new Size(224, 332);
			this.groupBox4.TabIndex = 29;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "机械臂控制";
			this.button3.Location = new Point(126, 262);
			this.button3.Name = "button3";
			this.button3.Size = new Size(86, 43);
			this.button3.TabIndex = 28;
			this.button3.Text = "底盘右转\r\n(G)";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new EventHandler(this.button3_Click_2);
			this.button2.Location = new Point(11, 262);
			this.button2.Name = "button2";
			this.button2.Size = new Size(96, 43);
			this.button2.TabIndex = 27;
			this.button2.Text = "底盘左转\r\n(F)";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new EventHandler(this.button2_Click_1);
			this.pictureBox2.Anchor = AnchorStyles.Bottom;
			this.pictureBox2.Image = (Image)componentResourceManager.GetObject("LOGO.png");
			this.pictureBox2.Location = new Point(15, 15);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new Size(399, 153);
			this.pictureBox2.TabIndex = 30;
			this.pictureBox2.TabStop = false;
			this.label1.AutoSize = true;
			this.label1.Location = new Point(6, 171);
			this.label1.Name = "label1";
			this.label1.Size = new Size(101, 12);
			this.label1.TabIndex = 31;
			this.label1.Text = "开发团队：大黑山的神秘力量";
			this.groupBox5.Controls.Add(this.label1);
			this.groupBox5.Controls.Add(this.pictureBox2);
			this.groupBox5.Location = new Point(277, 484);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new Size(443, 186);
			this.groupBox5.TabIndex = 32;
			this.groupBox5.TabStop = false;
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			this.BackColor = Color.FromArgb(0, 192, 192);
			base.ClientSize = new Size(999, 698);
			base.Controls.Add(this.groupBox5);
			base.Controls.Add(this.groupBox4);
			base.Controls.Add(this.groupBox3);
			base.Controls.Add(this.groupBox2);
			base.Controls.Add(this.groupBox1);
			base.Controls.Add(this.pictureBox1);
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.KeyPreview = true;
			base.Name = "Form1";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "WIFI智能小车操作平台 V2.0";
			base.Load += new EventHandler(this.Form1_Load);
			base.KeyDown += new KeyEventHandler(this.Form1_KeyDown);
			base.KeyUp += new KeyEventHandler(this.Form1_KeyUp);
			((ISupportInitialize)this.pictureBox1).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			((ISupportInitialize)this.pictureBox2).EndInit();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			base.ResumeLayout(false);
		}
	}
}
