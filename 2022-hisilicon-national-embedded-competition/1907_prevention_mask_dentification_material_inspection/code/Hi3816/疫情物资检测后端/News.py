import numpy
import requests
import xmnlp

xmnlp.set_model('./xmnlp-onnx-models-v4/xmnlp-onnx-models')
path = "http://api.tianapi.com/topnews/index?key=b0084f09b897ef0ebcda87859a7c6887&num=50&word=%E7%96%AB%E6%83%85"

def search_news():
    answer = []
    r = requests.get(path)
    for j in range(0, len(r.json()["newslist"])):
        info = []
        info.append(r.json()["newslist"][j]["ctime"])
        info.append(r.json()["newslist"][j]["title"])
        info.append(r.json()["newslist"][j]["description"])
        info.append(r.json()["newslist"][j]["source"])
        info.append(r.json()["newslist"][j]["picUrl"])
        info.append(r.json()["newslist"][j]["url"])
        word = info[1] + "。" + info[2]
        key = xmnlp.keyword(word, k=5, stopword=True)
        ans = ""
        for words in key:
            ans = ans + words[0] + "；"
        info.append(ans)
        sen = xmnlp.keyphrase(word, k=1)
        info.append(sen[0])
        tag = xmnlp.sentiment(word)
        if (tag[0] >= tag[1]):
            info.append("消极情绪")
        else:
            info.append("积极情绪")
        info.append(str(tag[0]))
        info.append(str(tag[1]))
        answer.append(info)
    return answer