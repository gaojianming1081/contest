import random
import requests
import os
import numpy as np
import health_code

os.environ['NO_PROXY'] = 'apis.map.qq.com'

def make_health_data():
    answer = []
    answer.append(round(random.uniform(36, 37),2))
    answer.append(random.randint(60,100))
    return answer

def get_loaction(wd,jd):
    answer = []
    url = "http://apis.map.qq.com/ws/geocoder/v1/?key=NWXBZ-HU6LD-DGD45-P44HO-CSX55-LDFZD&location="+wd+","+jd
    data = requests.get(url).json()
    answer.append(data["result"]["address"])
    url = "http://apis.map.qq.com/ws/geocoder/v1/?address=辽宁省大连市大连理工大学开发区校区憩苑&key=NWXBZ-HU6LD-DGD45-P44HO-CSX55-LDFZD"
    location = requests.get(url).json()
    new_wd = location["result"]["location"]["lat"]
    new_jd = location["result"]["location"]["lng"]
    if(abs(float(new_wd)-float(wd))<0.1 and abs(float(new_jd)-float(jd))<0.1):
        answer.append("是")
    else:
        answer.append("否")
    return answer

def detect_health(flag1,flag2,flag3,flag4,flag5,flag6,flag7,ctime,jd,wd,t,h,l,j,content):
    time_data = []
    if (os.path.exists("./Time_data/health_data.npy")):
        time_data = np.load("./Time_data/health_data.npy", allow_pickle=True).tolist()
    insert = []
    index_len = len(time_data)
    insert.append(index_len)
    insert.append(int(flag1))
    insert.append(int(flag2))
    insert.append(int(flag3))
    insert.append(int(flag4))
    insert.append(int(flag5))
    insert.append(int(flag6))
    insert.append(int(flag7))
    insert.append(ctime)
    insert.append(jd)
    insert.append(wd)
    insert.append(t)
    insert.append(h)
    insert.append(l)
    insert.append(j)
    insert.append(content)
    insert.append(health_code.generateQRCode(l,ctime,t,h,j))
    time_data.append(insert)
    np.save("./Time_data/health_data.npy", np.array(np.array(time_data)))

def health_data_function():
    if (os.path.exists("./Time_data/health_data.npy")):
        data = np.load("./Time_data/health_data.npy",allow_pickle=True)
        return data.tolist()
    else:
        data = []
        return data

def get_health_data(flag):
    data = health_data_function()
    return data[int(flag)]

if __name__ == '__main__':
    print(get_loaction("38.9137","121.6148"))