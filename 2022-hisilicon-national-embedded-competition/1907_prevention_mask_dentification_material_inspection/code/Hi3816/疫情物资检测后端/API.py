# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
import Yolov5
import Car
import Health
import News

app = Flask("my-app")

@app.route('/')
def start():
    answer = {}
    answer['flag']='success'
    return jsonify(answer)

@app.route('/detect_thing')
def thing():
    answer = {}
    answer['flag'] = 'success'
    ans_text = Yolov5.detect_thing()
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/thing_data')
def thing_data():
    answer = {}
    answer['flag'] = 'success'
    ans_text = Yolov5.time_data_function()
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/get_time_data')
def get_time_data():
    answer = {}
    answer['flag'] = 'success'
    flag = request.args['flag']
    ans_text = Yolov5.get_time_data(flag)
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/detect_car')
def car():
    answer = {}
    answer['flag'] = 'success'
    title = request.args['title']
    content = request.args['content']
    ctime = request.args['ctime']
    jd = request.args['jd']
    wd = request.args['wd']
    flag = request.args['flag']
    Car.detect_human(flag,title,content,ctime,jd,wd)
    return jsonify(answer)

@app.route('/car_data')
def car_data():
    answer = {}
    answer['flag'] = 'success'
    ans_text = Car.car_data_function()
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/get_car_data')
def get_car_data():
    answer = {}
    answer['flag'] = 'success'
    flag = request.args['flag']
    ans_text = Car.get_car_data(flag)
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/make_health_data')
def make_health_data():
    answer = {}
    answer['flag'] = 'success'
    ans_text = Health.make_health_data()
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/get_loaction')
def get_loaction():
    answer = {}
    answer['flag'] = 'success'
    wd = request.args['wd']
    jd = request.args['jd']
    ans_text = Health.get_loaction(wd,jd)
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/detect_health')
def health():
    answer = {}
    answer['flag'] = 'success'
    flag1 = request.args['flag1']
    flag2 = request.args['flag2']
    flag3 = request.args['flag3']
    flag4 = request.args['flag4']
    flag5 = request.args['flag5']
    flag6 = request.args['flag6']
    flag7 = request.args['flag7']
    ctime = request.args['ctime']
    jd = request.args['jd']
    wd = request.args['wd']
    t = request.args['t']
    h = request.args['h']
    l = request.args['l']
    j = request.args['j']
    content = request.args['content']
    Health.detect_health(flag1,flag2,flag3,flag4,flag5,flag6,flag7,ctime,jd,wd,t,h,l,j,content)
    return jsonify(answer)

@app.route('/health_data')
def health_data():
    answer = {}
    answer['flag'] = 'success'
    ans_text = Health.health_data_function()
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/get_health_data')
def get_health_data():
    answer = {}
    answer['flag'] = 'success'
    flag = request.args['flag']
    ans_text = Health.get_health_data(flag)
    answer['ans'] = ans_text
    return jsonify(answer)

@app.route('/news')
def news():
    answer = {}
    answer['flag'] = 'success'
    ans_text = News.search_news()
    answer['ans'] = ans_text
    return jsonify(answer)

if __name__ == '__main__':
    app.config['JSON_AS_ASCII'] = False
    app.run(host='0.0.0.0', port=7031, debug=True)