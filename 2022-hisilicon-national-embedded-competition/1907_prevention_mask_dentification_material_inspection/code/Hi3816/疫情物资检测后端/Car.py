from YOLO import human
import cv2
import uuid
import datetime
import numpy as np
import os
import random

def get_name():
    uuid_str = uuid.uuid4().hex
    file_name = uuid_str + ".png"
    return file_name

def slam():
    img_path = str(random.randint(1,6))+".png"
    answer = "https://image.cpolar.cn/data/" + img_path
    return answer

def photo():
    cap = cv2.VideoCapture(0)
    # 从摄像头获取图像，第一个为布尔变量表示成功与否，第二个变量是图像
    ret, filename = cap.read()
    # 保存图像至Haar相同路径
    img_name = get_name()
    cv2.imwrite('./YOLO/data/'+img_name, filename)
    # 释放摄像头资源
    cap.release()
    return img_name

def detect_human(flag,title,content,time_str,jd,wd):
    answer = []
    if(flag=="0"):
        img_path = photo()
        human.function(img_path)
        answer.append("https://image.cpolar.cn/data/" + img_path)
        answer.append("https://image.cpolar.cn/output/human/" + img_path)
        answer.append(slam())
    time_data = []
    if (os.path.exists("./Time_data/car_data.npy")):
        time_data = np.load("./Time_data/car_data.npy", allow_pickle=True).tolist()
    insert = []
    index_len = len(time_data)
    insert.append(index_len)
    insert.append(time_str)
    insert.append(title)
    insert.append(content)
    insert.append(jd)
    insert.append(wd)
    insert.append(answer)
    time_data.append(insert)
    np.save("./Time_data/car_data.npy", np.array(np.array(time_data)))

def car_data_function():
    if (os.path.exists("./Time_data/car_data.npy")):
        data = np.load("./Time_data/car_data.npy",allow_pickle=True)
        return data.tolist()
    else:
        data = []
        return data

def get_car_data(flag):
    data = car_data_function()
    return data[int(flag)]

if __name__ == '__main__':
    print(car_data_function())