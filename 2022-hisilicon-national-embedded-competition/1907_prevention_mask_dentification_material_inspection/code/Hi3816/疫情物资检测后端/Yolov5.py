from YOLO import human
from YOLO import thing
import cv2
import uuid
import datetime
import numpy as np
import os

def get_name():
    uuid_str = uuid.uuid4().hex
    file_name = uuid_str + ".png"
    return file_name

def photo():
    cap = cv2.VideoCapture(0)
    # 从摄像头获取图像，第一个为布尔变量表示成功与否，第二个变量是图像
    ret, filename = cap.read()
    # 保存图像至Haar相同路径
    img_name = get_name()
    cv2.imwrite('./YOLO/data/'+img_name, filename)
    # 释放摄像头资源
    cap.release()
    return img_name

def detect_human():
    img_path = photo()
    answer = []
    human.function(img_path)
    answer.append("https://image.cpolar.cn/data/" + img_path)
    answer.append("https://image.cpolar.cn/output/human/" + img_path)
    return answer

def detect_thing():
    img_path = photo()
    answer = []
    data = thing.function(img_path)
    answer.append("https://image.cpolar.cn/data/" + img_path)
    answer.append("https://image.cpolar.cn/output/thing/" + img_path)
    answer.append(data)

    now_time = datetime.datetime.now()
    time_str = now_time.strftime("%Y-%m-%d %H:%M:%S")
    time_data = []
    if(os.path.exists("./Time_data/time_data.npy")):
        time_data = np.load("./Time_data/time_data.npy",allow_pickle=True).tolist()
    index_len = len(time_data)
    insert = []
    insert.append(index_len)
    insert.append(time_str)
    insert.append(answer)
    time_data.append(insert)
    np.save("./Time_data/time_data.npy",np.array(np.array(time_data)))
    return answer

def time_data_function():
    if (os.path.exists("./Time_data/time_data.npy")):
        data = np.load("./Time_data/time_data.npy",allow_pickle=True)
        return data.tolist()
    else:
        data = []
        return data

def get_time_data(flag):
    data = time_data_function()
    return data[int(flag)]

if __name__ == '__main__':
    print(time_data_function()[2][0])