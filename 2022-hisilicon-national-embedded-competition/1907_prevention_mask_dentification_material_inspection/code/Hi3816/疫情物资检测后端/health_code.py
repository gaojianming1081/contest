import qrcode
from qrcode.constants import ERROR_CORRECT_H
import uuid

def get_name():
    uuid_str = uuid.uuid4().hex
    file_name = uuid_str + ".png"
    return file_name

def generateQRCode(l,ctime,t,h,j):
    qr = qrcode.QRCode(version=20,
                       error_correction=ERROR_CORRECT_H,
                       box_size=3, border=2)
    # 添加自定义文本信息，
    data = "位置："+l+"；时间："+ctime+"；"
    flag=""
    path=get_name()
    if(j=="是"):
        if(float(t)<=37.0 and int(h)>=60 and int(h)<=100):
            flag="green"
            data = data+"未离开隔离区，健康数据正常；"
        else:
            flag="yellow"
            data = data + "未离开隔离区，健康数据异常；"
    else:
        flag="red"
        if (float(t) <= 37.0 and int(h) >= 60 and int(h) <= 100):
            data = data + "离开过隔离区，健康数据正常；"
        else:
            data = data + "离开过隔离区，健康数据异常；"
    qr.add_data(data)
    qr.make()
    # 创建二维码图片
    img = qr.make_image(fill_color=flag)
    # 保存二维码图片
    img.save('./YOLO/data/'+path)  # 自己的存储路径
    return "https://image.cpolar.cn/data/" + path