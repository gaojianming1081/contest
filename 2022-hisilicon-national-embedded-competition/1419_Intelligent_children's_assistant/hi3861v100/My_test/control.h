#ifndef __CONTROL_H__
#define __CONTROL_H__

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include "hand.h"
#include "motor.h"

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;

#define PROGRAM_MODE 0
#define MONITOR_MODE 1

bool Conditional_judgment(unsigned char *str, unsigned short *shunxu, unsigned short *flag);
bool strnum_anlyse(unsigned char *str, unsigned short *shunxu, unsigned short *tar);
bool str_analyse(unsigned char *str);

#endif