#ifndef __ESP_H__
#define __ESP_H__

#include <stdio.h>
#include <unistd.h>
#include <hi_stdlib.h>
#include <hi_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"
#include "control.h"

#define U1_RX 1
#define U1_TX 0

#define ESP_TASK_STACK_SIZE 1024 * 10

#define AT1 "AT\r\n"
#define AT2 "AT+RST\r\n"
#define AT3 "AT+CWJAP="     \
            "\"nova 7 5G\"" \
            ","             \
            "\"123456789\"" \
            "\r\n"
#define AT4 "AT+CIPMUX=0\r\n"
#define AT5 "AT+CIPSTART="      \
            "\"TCP\""           \
            ","                 \
            "\"47.100.93.186\"" \
            ",8061\r\n"
#define AT6 "AT+CIPMODE=1\r\n"
#define AT7 "AT+CIPSEND\r\n"
#define AT8 "+++\r\n"

void Esp8266_GpioInit();

#endif