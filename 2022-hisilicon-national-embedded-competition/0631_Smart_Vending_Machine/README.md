# 基于手势识别的非接触式智能商品售卖机
### 0631 梁思地；刘奕；刘亦骢

        _____ __  __          _____ _______  __      ________ _   _ _____   ____  _____  
       / ____|  \/  |   /\   |  __ \__   __| \ \    / /  ____| \ | |  __ \ / __ \|  __ \  
      | (___ | \  / |  /  \  | |__) | | |     \ \  / /| |__  |  \| | |  | | |  | | |__) |  
       \___ \| |\/| | / /\ \ |  _  /  | |      \ \/ / |  __| | . ` | |  | | |  | |  _  /  
       ____) | |  | |/ ____ \| | \ \  | |       \  /  | |____| |\  | |__| | |__| | | \ \  
      |_____/|_|  |_/_/    \_\_|  \_\ |_|        \/   |______|_| \_|_____/ \____/|_|  \_\  

  Copyright (c) 2022 Sidi Liang, Yi Liu, Yicong Liu.  
  Licensed under the Apache License, Version 2.0 (the "License");  
  you may not use this file except in compliance with the License.  
  You may obtain a copy of the License at  

      http://www.apache.org/licenses/LICENSE-2.0  

  Unless required by applicable law or agreed to in writing, software  
  distributed under the License is distributed on an "AS IS" BASIS,  
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
  See the License for the specific language governing permissions and  
  limitations under the License.  
