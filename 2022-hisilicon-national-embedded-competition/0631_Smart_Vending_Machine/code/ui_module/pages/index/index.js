import mqtt from "../../utils/mqtt.min.js";

Page({
  data: {
    client: null,
    conenctBtnText: "连接",
    host: "YT32IOSCAL.ap-guangzhou.iothub.tencentdevices.com:443",
    subTopic: "YT32IOSCAL/UI/data",
    shadowTopic:"$shadow/operation/result/YT32IOSCAL/UI",
    pubTopic: "YT32IOSCAL/UI/event",
    receivedMsg: "",
    received:"",
    shadowMsg:"",
    shadow:"",
    name1:"口罩",
    name2:"酒精棉片",
    name3:"碘酒",
    name4:"创可贴",
    price1:1,
    price2:9.9,
    price3:5.9,
    price4:9.9,
    mqttOptions: {
      username: "YT32IOSCALUI;12010126;88a3d;1664208000",
      password: "2788c933085788d342950273e749b9c5f328221ef752a3392c0f590ec660e7c0;hmacsha256",
      reconnectPeriod: 1000,
      connectTimeout: 30 * 1000
    }
  },

  setValue(key, value) {
    this.setData({
      [key]: value,
    });
  },

  setHost(e) {
    this.setValue("host", e.detail.value);
  },
  setSubTopic(e) {
    this.setValue("subTopic", e.detail.value);
  },
  setPubTopic(e) {
    this.setValue("pubTopic", e.detail.value);
  },
  setPubMsg(e) {
    this.setValue("pubMsg", e.detail.value);
  },
  setRecMsg(msg) {
    this.setValue("receivedMsg", msg);
  },

  connect() {
    try {
      this.setValue("conenctBtnText", "连接中...");
      const clientId = new Date().getTime();
      this.data.client = mqtt.connect(`wxs://${this.data.host}/mqtt`, {
        ...this.data.mqttOptions,
        clientId,
      });

      this.data.client.on("connect", () => {
        /*wx.showToast({
          title: "连接成功",
        });*/
        this.setValue("conenctBtnText", "连接成功");

        this.data.client.on("message", (topic, payload) => {
          /*wx.showModal({
            content: `收到消息 - Topic: ${topic}，Payload: ${payload}`,
            showCancel: false,
          });*/
          if(topic=="YT32IOSCAL/UI/data"){
          const currMsg = this.data.receivedMsg ? `<br/>${payload}` : payload;
          this.setValue("receivedMsg", this.data.receivedMsg.concat(currMsg));
          this.publish ();
          this.data.receivedMsg = "";
          }
          if(topic=="$shadow/operation/result/YT32IOSCAL/UI"){
          const currMsg = this.data.shadowMsg ? `<br/>${payload}` : payload;
          this.setValue("shadowMsg", this.data.shadowMsg.concat(currMsg));
          this.shadowDeal();
          this.data.shadowMsg = "";
          }
          
        });

        this.data.client.on("error", (error) => {
          this.setValue("conenctBtnText", "连接");
          console.log("onError", error);
        });

        this.data.client.on("reconnect", () => {
          this.setValue("conenctBtnText", "连接");
          console.log("reconnecting...");
        });

        this.data.client.on("offline", () => {
          this.setValue("conenctBtnText", "连接");
          console.log("onOffline");
        });
      });
    } catch (error) {
      this.setValue("conenctBtnText", "连接");
      console.log("mqtt.connect error", error);
    }
  },

  subscribe() {
    if (this.data.client) {
      this.data.client.subscribe(this.data.subTopic);
      this.data.client.subscribe(this.data.shadowTopic);
      /*wx.showModal({
        content: `成功订阅主题：${this.data.subTopic}`,
        showCancel: false,
      });*/
      return;
    }
    /*wx.showToast({
      title: "请先点击连接",
      icon: "error",
    });*/
  },

  unsubscribe() {
    if (this.data.client) {
      this.data.client.unsubscribe(this.data.subTopic);
      this.data.client.subscribe(this.data.shadowTopic);
      /*wx.showModal({
        content: `成功取消订阅主题：${this.data.subTopic}`,
        showCancel: false,
      });*/
      return;
    }
    /*wx.showToast({
      title: "请先点击连接",
      icon: "error",
    });*/
  },

shadowDeal(){
  const received = JSON.parse(this.data.shadowMsg);
  this.data.name1 = received.state.reported.product_info[0].product_name;
  this.data.name2 = received.state.reported.product_info[1].product_name;
  this.data.name3 = received.state.reported.product_info[2].product_name;
  this.data.name4 = received.state.reported.product_info[3].product_name;
  this.data.price1 = received.state.reported.product_info[0].product_price;
  this.data.price2 = received.state.reported.product_info[1].product_price;
  this.data.price3 = received.state.reported.product_info[2].product_price;
  this.data.price4 = received.state.reported.product_info[3].product_price;
},

  deal()
  {
    const received = JSON.parse(this.data.receivedMsg);
    if(received.slot_num == 1){
      return {chosen_slot:"slot_1"};
    }
    if(received.slot_num == 2){
      return {chosen_slot:"slot_2"};
    }
    if(received.slot_num == 3){
      return {chosen_slot:"slot_3"};
    }
    if(received.slot_num == 4){
      return {chosen_slot:"slot_4"};
    }
  },
  
  publish() {
    if (this.data.client) {
      this.data.client.publish(this.data.pubTopic, JSON.stringify(this.deal()));
      console.log(JSON.stringify(this.deal()));
      return;
    }
    /*wx.showToast({
      title: "请先点击连接",
      icon: "error",
    });*/
  },

  disconnect() {
    this.data.client.end();
    this.data.client = null;
    this.setValue("conenctBtnText", "连接");
    /*wx.showToast({
      title: "成功断开连接",
    });*/
  },

  onLoad:function(){
      this.connect();
      this.subscribe();
    },
})
