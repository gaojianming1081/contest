import mqtt from "../../utils/mqtt.min.js";

Page({
  data: {
    client: null,
    conenctBtnText: "连接",
    host: "YT32IOSCAL.ap-guangzhou.iothub.tencentdevices.com:443",
    subTopic: "$shadow/operation/result/YT32IOSCAL/UI",
    pubTopic: "$shadow/operation/YT32IOSCAL/UI",
    receivedMsg: "",
    received:"",
    version:1,
    name1:"创可贴",
    price1:9.9,
    remain_num1:3,
    name2:"口罩",
    price2:1,
    remain_num2:3,
    name3:"消毒喷雾",
    price3:15,
    remain_num3:3,
    name4:"口罩",
    price4:1,
    remain_num4:3,
    mqttOptions: {
      username: "YT32IOSCALUI;12010126;88a3d;1664208000",
      password: "2788c933085788d342950273e749b9c5f328221ef752a3392c0f590ec660e7c0;hmacsha256",
      reconnectPeriod: 1000,
      connectTimeout: 30 * 1000
    }
  },

_nameEdit1: function(event){
  this.setData({name1:event.detail.value});
  this.publish();
},
_priceEdit1: function(event){
  this.setData({price1:event.detail.value});
  this.publish();
},
_remainingNumEdit1: function(event){
  this.setData({remain_num1:event.detail.value});
  this.publish();
},

_nameEdit2: function(event){
  this.setData({name2:event.detail.value});
  this.publish();
},
_priceEdit2: function(event){
  this.setData({price2:event.detail.value});
  this.publish();
},
_remainingNumEdit2: function(event){
  this.setData({remain_num2:event.detail.value});
  this.publish();
},

_nameEdit3: function(event){
  this.setData({name3:event.detail.value});
  this.publish();
},
_priceEdit3: function(event){
  this.setData({price3:event.detail.value});
  this.publish();
},
_remainingNumEdit3: function(event){
  this.setData({remain_num3:event.detail.value});
  this.publish();
},

_nameEdit4: function(event){
  this.setData({name4:event.detail.value});
  this.publish();
},
_priceEdit4: function(event){
  this.setData({price4:event.detail.value});
  this.publish();
},
_remainingNumEdit4: function(event){
  this.setData({remain_num4:event.detail.value});
  this.publish();
},

  setValue(key, value) {
    this.setData({
      [key]: value,
    });
  },

  setHost(e) {
    this.setValue("host", e.detail.value);
  },
  setSubTopic(e) {
    this.setValue("subTopic", e.detail.value);
  },
  setPubTopic(e) {
    this.setValue("pubTopic", e.detail.value);
  },
  setPubMsg(e) {
    this.setValue("pubMsg", e.detail.value);
  },
  setRecMsg(msg) {
    this.setValue("receivedMsg", msg);
  },

  connect() {
    try {
      this.setValue("conenctBtnText", "连接中...");
      const clientId = new Date().getTime();
      this.data.client = mqtt.connect(`wxs://${this.data.host}/mqtt`, {
        ...this.data.mqttOptions,
        clientId,
      });

      this.data.client.on("connect", () => {
        /*wx.showToast({
          title: "连接成功",
        });*/
        this.setValue("conenctBtnText", "连接成功");

        this.data.client.on("message", (topic, payload) => {
          /*wx.showModal({
            content: `收到消息 - Topic: ${topic}，Payload: ${payload}`,
            showCancel: false,
          });*/
          const currMsg = this.data.receivedMsg ? `<br/>${payload}` : payload;
          
          this.setValue("receivedMsg", this.data.receivedMsg.concat(currMsg));
        });

        this.data.client.on("error", (error) => {
          this.setValue("conenctBtnText", "连接");
          console.log("onError", error);
        });

        this.data.client.on("reconnect", () => {
          this.setValue("conenctBtnText", "连接");
          console.log("reconnecting...");
        });

        this.data.client.on("offline", () => {
          this.setValue("conenctBtnText", "连接");
          console.log("onOffline");
        });
      });
    } catch (error) {
      this.setValue("conenctBtnText", "连接");
      console.log("mqtt.connect error", error);
    }
  },

  subscribe() {
    if (this.data.client) {
      this.data.client.subscribe(this.data.subTopic);
      /*wx.showModal({
        content: `成功订阅主题：${this.data.subTopic}`,
        showCancel: false,
      });*/
      return;
    }
    /*wx.showToast({
      title: "请先点击连接",
      icon: "error",
    });*/
  },

  unsubscribe() {
    if (this.data.client) {
      this.data.client.unsubscribe(this.data.subTopic);
      /*wx.showModal({
        content: `成功取消订阅主题：${this.data.subTopic}`,
        showCancel: false,
      });*/
      return;
    }
    /*wx.showToast({
      title: "请先点击连接",
      icon: "error",
    });*/
  },

  deal()//需要根据通信协议更改的地方
  {
    return {
      type: "update",
      state:        {
              reported:     {
                      product_info: [{
                                      slot_num:"1",
                                      product_price: this.data.price1,
                                      remaining_num: this.data.remain_num1,
                                      product_name: this.data.name1,
                              }, {
                                      slot_num:"2",
                                      product_price: this.data.price2,
                                      remaining_num: this.data.remain_num2,
                                      product_name: this.data.name2,
                              }, {
                                      slot_num: 3,
                                      product_price: this.data.price3,
                                      remaining_num: this.data.remain_num3,
                                      product_name: this.data.name3,
                              }, {
                                      slot_num: 4,
                                      product_price: this.data.price4,
                                      remaining_num: this.data.remain_num4,
                                      product_name: this.data.name4,
                              }]
              }
      },
      version: this.data.version,
      clientToken:  ""
};
  },
  
  publish() {
    if (this.data.client) {
      this.data.client.publish(this.data.pubTopic, JSON.stringify(this.deal()));
      this,data.version++;
      console.log(JSON.stringify(this.deal()));
      return;
    }
    wx.showToast({
      title: "请先点击连接",
      icon: "error",
    });
  },

  /*disconnect() {
    this.data.client.end();
    this.data.client = null;
    this.setValue("conenctBtnText", "连接");
    wx.showToast({
      title: "成功断开连接",
    });
  },*/

  onLoad:function(){
      this.connect();
      this.subscribe();
    },
})
