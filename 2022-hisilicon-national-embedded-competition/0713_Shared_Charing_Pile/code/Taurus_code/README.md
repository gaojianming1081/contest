# 共享智能充——基于视觉感知与网络通信的新能源电动汽车智能共享充电桩 Taurus code
***
作者： 黄璟烨 杨博帆 张志灿    
时间： 2022/6
***
## 1.libopencv
交叉编译时所依赖的opencv库，已经打包好了；
亦可根据自己项目需要选择不同版本/不同阉割程度的opencv依赖库，需自行编译。

## 2.libhisi
交叉编译时所依赖的nnie库，海思官方提供，未作处理全部堆上，lib静态库，亦可换为动态库。


## 3.src
### sample_nnie_api.h
yolov3 wk模型接口函数：
1. int yolo_init(const char *yolo_model_path);
2. int yolo_run(unsigned char *input_yuv420_data, int input_w, int input_h);
3. int yolo_unit();
可封装为标准的输入输出接口。

### sample_nnie_api.cpp
yolov3 wk模型后处理过程详细的代码实现。


## 4.main.cpp
主函数代码实现

## 5.交叉编译环境
交叉编译工具见海思Hisilicon官方提供的SDK，即**arm-himix200-linux.tgz**，3.6个GB。


## 6.编译文件
**Makefile**较为简洁直观的编译文件

## 7.可执行文件
1. 文件test为可执行文件
2. 使用时 需要将test文件放入到Taurus开发板上
3. 生成一个空文件 `touch 1.jpg`
4. 键入代码`./test 1.jpg yourWkModel DestIPaddress DestPort` \
*注意yourWkModel指的是您的wk model的位置* \
*DestIPaddress DestPort指的是目标Pegasus板的ip地址与可用端口号*



如有技术问题欢迎交流！
如有版权问题请及时告知！
