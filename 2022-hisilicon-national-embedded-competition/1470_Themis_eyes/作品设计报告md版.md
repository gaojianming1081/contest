# 基于AI视觉的口罩检测系统---Themis eyes

**刘嘉兴；邓鑫伟；阮跃**

------



## 第一部分 设计概述

### 1.1  设计目的

2020年初,新冠病毒席卷全球,新冠状病毒具有很强的传染性,规范佩戴口罩可以阻隔病毒通过空气中的飞沫、气溶胶等载体传播,在很多公共场合要求佩戴口罩。利用计算机视觉检测人脸是否佩戴口罩以及识别是否佩戴规范,可以避免行人之间、检测人员与被检人员接触感染的风险且更加高效。

### 1.2  应用领域

主要应用在人群密集的场所入口，如医院、机场、火车站、商场等场所入口，检测行人的口罩佩戴情况并给出提示，防止因未佩戴口罩或口罩佩戴不规范而造成的感染风险。

### 1.3  主要技术特点

智能化：终端智能化后，T板直接对拍摄的视频内容进行帧处理后检测目标，同时将检测信息发送给P板处理。

远程化：通过mqtt协议使得T板与云端通信，相比于传统拉取方式,基于MQTT的物联网消息推送系统更加及时、高效、省流量、省资源。

人机交互：通过微信小程序，可以控制T板的语音播报功能开关。

可更改性：能够根据用户的需求，可以更改外设，以满足用户再不同情景下的需要。

模块互联：Taurus使用的uart协议和Pegasus连接，Pegasus用mqtt协议与云端互联，形成整条通路。

### 1.4  关键性能指标

本作品有两个性能指标，一是识别率，二是语音播报的即时性。

（1）  识别率：

训练之后的权重文件召回率基本为100%，IOU 93%以上，效果较好。

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141432425.jpg)

图1.1 训练结果

板端测试置信门限设置为0.8，识别100张图片，准确率为92%，效果较好。

（2）即时性：语音即时播报效果良好。

### 1.5  主要创新点

（1）T板能够直接检测出口罩佩戴不规范的情况，不用将数据上传至云端处理，就近提供边缘智能服务。

（2）能通过微信小程序远程查看检测结果并控制语音提示功能的开关，适用于多种场景。

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141429551.jpg)

图1.2 小程序界面

------



## 第二部分 系统组成及功能说明

### 2.1 整体介绍

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141432305.png)

图2.1 系统整体结构

Themis eyes 系统由四个模块组成，如图2.1所示，分别是AI摄像头模块、WIFI物联网模块、语音模块和微信小程序用户界面。

（1）AI摄像头模块使用了HiSpark Taurus套件作为硬件。功能是定位人脸，用绿色框选正确佩戴口罩目标，用红色框选未佩戴口罩目标和佩戴口罩不规范的目标。将判断结果打包成数据帧通过uart1口传到物联网模块。摄像头模块结合NNIE神经网络引擎和支持的深度学习框架对人脸进行识别，并做出判决。

（2）WIFI物联网模块使用了HiSpark Pegasus套件作为硬件。功能是接受摄像头的判决结果，把判决结果发送到腾讯云上。同时，在识别到违规目标后，控制LED闪烁，并且触发语音模块播放特定语音进行提醒。

（3）语音模块使用了JQ8900-16P板作为硬件。功能是从P板的uart2口接收物联网模块的判决结果信号，根据不同信号触发不同的语音提醒。

（4）用户界面使用了腾讯云做后端云平台，微信小程序作为前端。物联网模块连接4G网络后，通过MQTT协议与云端进行通信。微信小程序获得云端的权限后，把云端的有效的信息，在小程序界面显示出来。

### 2.2 各模块介绍

#### 2.2.1 AI摄像头模块

AI摄像头模块在Themis eyes系统中负责目标检测与判决的功能：

（1）在系统中的功能：

a. 将视频流中的人脸进行检测分类，共有三种类别：佩戴口罩、未佩戴口罩以及口罩佩戴不规范；

b. 当画面中有以上三种目标物时，该模块会将目标区域框选出，第一种类别用绿色框线，另外两种类别用红色框线；

c. 将当前视频帧中目标物的检测结果通过串口uart1发送到P板，进行后续操作。

（2）程序实现：工作流程如图2.2所示：

![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141429641.png)

图2.2 T板工作流程图

#### 2.2.2 WIFI物联网模块

物联网模块在Themis eyes系统中主要负责通信的功能：

Hi3861L芯片共有3个UART接口，都和GPIO复用。其中UART0 可通过USB转UART 的转换芯片U4进行USB Debug。本系统使用了UART1和UART2。UART1用于摄像头和物联网模块的串口互联，UART2用于语音模块和物联网模块的串口互联。

 

| Digital | UART0 | UART1/2   |
| ------- | ----- | --------- |
| GPIO_00 |       | UART1_TXD |
| GPIO_01 |       | UART1_RXD |
| GPIO_11 |       | UART2_TXD |
| GPIO_12 |       | UART2_RXD |

表2.1 串口使用情况

 

\1.   与摄像头的串口互联

（1）在系统中的功能：

a. 在物联网板子的串口上显示摄像头的检测信息；

b. 获得检测目标的判决结果；

c. 控制语音根据不同判决结果播报相应语音提示；

d. 获得是否向云端发布的标志位。

（2）所用协议：两模块之间通过Hisignalling协议（利用crc校验）通信。

（3）程序实现：程序流程如图2.3所示：

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141429373.png)

图2.3 P板、T板通信流程图

 

\2.   与语音模块的串口互联：

（1）在系统中的功能：在小程序的控制下，播报特定的提示语音。

（2）程序实现：程序流程如图2.4所示：

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141429152.png)

图2.4 P板语音模块流程图

 

\3. 与腾讯云（小程序）互联：

 

（1）在系统中的功能：

a. 接收云端的语音功能开关信号；

b. 把检测目标的判决结果发布到云端；

c. 发现异常目标的同时点亮LED提示灯。

（2）所用协议：MQTT协议。

（3）程序实现：程序流程如图2.5、2.6所示：

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141432087.png)

图2.5 状态信息获取流程图

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141429102.png)

图2.6 信息打包发布流程图

#### 2.2.3 语音模块

语音模块在Themis eyes系统中负责语音提醒的功能：

语音模块通过JQ8900-16P模块实现，提前生成特定语句的音频文件，并将音频文件拷入模块自带的Flash中，通过设定好的通信协议发送特定的数组调用即可播放对应的音频。

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141432880.png)

图2.7 语音模块实物图

 

#### 2.2.4 用户界面

微信小程序在Themis eyes系统中负责信息显示和系统控制的功能：

（1）在系统中的功能：

a. 通过小程序直观的了解到当前系统的检测状态；

b. 通过按键来控制是否打开语音播报，选择合适于当前环境的模式；

c. 小程序部分预留了许多资源空间，以便后续扩展功能时添加。

（2）程序实现：程序流程如图2.8所示：

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141429277.png)

图2.8 小程序模块流程图

------



## 第三部分 完成情况及性能参数

目前yolov2模型在PC端可识别不带口罩的人脸、戴口罩的人脸和戴口罩不规范的人脸，训练之后的权重文件召回率基本为100%，IOU为93%以上，效果较好。

首先，在模型部署到板端时，能准确识别视频和图片中不带口罩的人脸和戴口罩的人脸的情况。在识别戴口罩不规范的人目标时，只能识别在镜头近处的视频和图片中目标。识别100张图片的准确率为92%。

此外，本系统规定，戴口罩的人脸和戴口罩不规范的人脸是违规个体。当检测到违规个体，蓝色LED灯点亮，并且分别发出提示语音，红色LED灯点亮，如图3.1。

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141430363.png)

 图3.1 检测到未佩戴口罩

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141430791.png)

图3.2 检测到佩戴口罩

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141430391.jpg)

图3.3 未检测到目标

 ![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141432281.jpg)

图3.4 检测口罩佩戴不规范的目标

最终，小程序上对摄像头下检测出的未检测目标、已佩戴口罩、未佩戴口罩和口罩佩戴异常四种情况进行相应文字显示提示。并且，有控制语音播报的按钮。      

![](https://ced-md-picture.oss-cn-beijing.aliyuncs.com/img/202207141430249.png)

图3.5 口罩识别界面

------



## 第四部分 总结

### 4.1 可扩展之处

本作品具有较强的可更改性，适用场所多样，可以根据实际需求加上电机、舵机等外设，控制闸门开关，用于各种大型场所入口的自动口罩检测，也可以加上滑轮，用于公交车内、地铁车厢内，移动识别人们的口罩佩戴情况，将情况反馈给安保人员，减轻巡视的工作量。

### 4.2 心得体会

经历这次比赛，我们对Pegasus和Taurus这两个套件有了一定的认识，也对于终端智能化充满了信心。从对于AI的一窍不通到运用AI完成口罩检测，这个过程磕磕绊绊也充满惊喜，当在Taurus上面看到目标物上面的方框时，内心的喜悦是难以言喻的。

通过对成熟的源码的不断理解，第一次感受到了海思公司在所管业务范围内，是在何处做到的严谨。以后我也会不管学习借鉴OpenHarmony_master源码的严谨的思想，指导我以后的变成环节。

在此期间，我们和其他参赛队伍的同学，海思的老师交流学习，解决了很多问题，学到了很多在学校学习过程学不到的东西，同时提高了我们学习效率，团队合作的能力。非常感谢老师和同学们提供的帮助，我们不仅是比赛的竞争对手，更是攻坚克难、携手并进的“战友”！

------



## 第五部分 参考文献

[1]肖俊杰.基于YOLOV3和YCrCb的人脸口罩检测与规范佩戴识别[J].软件,2020,41(07):164-169.

[2]曹城硕,袁杰.基于YOLO-Mask算法的口罩佩戴检测方法[J].激光与光电子学进展,2021,58(08):211-218.

[3]姜妮,张宇,赵志军.基于MQTT物联网消息推送系统[J].网络新媒体技术,2014,3(06):62-64.

[4]CSDN: UART协议，2022年4月14日，https://blog.csdn.net/zxc126534/article/details/124173207。

 

 

------



## 第六部分 附录

摄像头模块重要代码：

\1. /* function : Yolov2 software para init */ 

\2. **static** HI_S32 SampleSvpNnieYolov2SoftwareInit(SAMPLE_SVP_NNIE_CFG_S* pstCfg,   SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam, SAMPLE_SVP_NNIE_YOLOV2_SOFTWARE_PARAM_S* pstSoftWareParam) 

\3. { 

\4.   HI_S32 s32Ret; 

\5.   HI_U32 u32ClassNum = 0; 

\6.   HI_U32 u32BboxNum; 

\7.   HI_U32 u32TotalSize = 0; 

\8.   HI_U32 u32DstRoiSize; 

\9.   HI_U32 u32DstScoreSize; 

\10.   HI_U32 u32ClassRoiNumSize; 

\11.   HI_U32 u32TmpBufTotalSize; 

\12.   HI_U64 u64PhyAddr = 0; 

\13.   HI_U8* pu8VirAddr = NULL; 

\14.  

\15.   pstSoftWareParam->u32OriImHeight = pstNnieParam->astSegData[0].astSrc[0].unShape.stWhc.u32Height; 

\16.   pstSoftWareParam->u32OriImWidth = pstNnieParam->astSegData[0].astSrc[0].unShape.stWhc.u32Width; 

\17.   pstSoftWareParam->u32BboxNumEachGrid = 5; // 5: 2BboxNumEachGrid 

\18.   pstSoftWareParam->u32ClassNum = 3; // 5: class number 

\19.   pstSoftWareParam->u32GridNumHeight = 12; // 12: GridNumHeight 

\20.   pstSoftWareParam->u32GridNumWidth = 20; // 20: GridNumWidth 

\21.   pstSoftWareParam->u32NmsThresh = (HI_U32)(0.3f*SAMPLE_SVP_NNIE_QUANT_BASE); 

\22.   pstSoftWareParam->u32ConfThresh = (HI_U32)(0.25f*SAMPLE_SVP_NNIE_QUANT_BASE); 

\23.   pstSoftWareParam->u32MaxRoiNum = 10; // 10: MaxRoiNum 

\24.   pstSoftWareParam->af32Bias[0] = 0.52; // 0.52: af32Bias[0] value 

\25.   pstSoftWareParam->af32Bias[1] = 0.61; // 0.61: af32Bias[1] value 

\26.   pstSoftWareParam->af32Bias[ARRAY_SUBSCRIPT_2] = 1.05; // 1.05: af32Bias[ARRAY_SUBSCRIPT_2] value 

\27.   pstSoftWareParam->af32Bias[ARRAY_SUBSCRIPT_3] = 1.12; // 1.12: af32Bias[ARRAY_SUBSCRIPT_3] value 

\28.   pstSoftWareParam->af32Bias[ARRAY_SUBSCRIPT_4] = 1.85; // 1.85: af32Bias[ARRAY_SUBSCRIPT_4] value 

\29.   pstSoftWareParam->af32Bias[ARRAY_SUBSCRIPT_5] = 2.05; // 2.05: af32Bias[ARRAY_SUBSCRIPT_5] value 

\30.   pstSoftWareParam->af32Bias[ARRAY_SUBSCRIPT_6] = 4.63; // 4.63: af32Bias[ARRAY_SUBSCRIPT_6] value 

\31.   pstSoftWareParam->af32Bias[ARRAY_SUBSCRIPT_7] = 4.49; // 4.49: af32Bias[ARRAY_SUBSCRIPT_7] value 

\32.   pstSoftWareParam->af32Bias[ARRAY_SUBSCRIPT_8] = 7.15; // 7.15: af32Bias[ARRAY_SUBSCRIPT_8] value 

\33.   pstSoftWareParam->af32Bias[ARRAY_SUBSCRIPT_9] = 7.56; // 7.56: af32Bias[ARRAY_SUBSCRIPT_9] value 

\34.  

\35.   /* Malloc assist buffer memory */ 

\36.   u32ClassNum = pstSoftWareParam->u32ClassNum + 1; 

\37.   u32BboxNum = pstSoftWareParam->u32BboxNumEachGrid*pstSoftWareParam->u32GridNumHeight* 

\38.     pstSoftWareParam->u32GridNumWidth; 

\39.   u32TmpBufTotalSize = SAMPLE_SVP_NNIE_Yolov2_GetResultTmpBuf(pstSoftWareParam); 

\40.   u32DstRoiSize = SAMPLE_SVP_NNIE_ALIGN16(u32ClassNum * u32BboxNum * **sizeof**(HI_U32) * SAMPLE_SVP_NNIE_COORDI_NUM); 

\41.   u32DstScoreSize = SAMPLE_SVP_NNIE_ALIGN16(u32ClassNum * u32BboxNum * **sizeof**(HI_U32)); 

\42.   u32ClassRoiNumSize = SAMPLE_SVP_NNIE_ALIGN16(u32ClassNum * **sizeof**(HI_U32)); 

\43.   u32TotalSize = u32TotalSize + u32DstRoiSize + u32DstScoreSize + u32ClassRoiNumSize + u32TmpBufTotalSize; 

\44.   s32Ret = SAMPLE_COMM_SVP_MallocCached("SAMPLE_YOLOV2_INIT", NULL, (HI_U64*)&u64PhyAddr, 

\45.     (**void****)&pu8VirAddr, u32TotalSize); 

\46.   SAMPLE_SVP_CHECK_EXPR_RET(HI_SUCCESS != s32Ret, s32Ret, SAMPLE_SVP_ERR_LEVEL_ERROR, 

\47.     "Error,Malloc memory failed!\n"); 

\48.   memset_s(pu8VirAddr, u32TotalSize, 0, u32TotalSize); 

\49.   SAMPLE_COMM_SVP_FlushCache(u64PhyAddr, (**void***)pu8VirAddr, u32TotalSize); 

\50.  

\51.   /* set each tmp buffer addr */ 

\52.   pstSoftWareParam->stGetResultTmpBuf.u64PhyAddr = u64PhyAddr; 

\53.   pstSoftWareParam->stGetResultTmpBuf.u64VirAddr = (HI_U64)((HI_UL)pu8VirAddr); 

\54.  

\55.   /* set result blob */ 

\56.   pstSoftWareParam->stDstRoi.enType = SVP_BLOB_TYPE_S32; 

\57.   pstSoftWareParam->stDstRoi.u64PhyAddr = u64PhyAddr + u32TmpBufTotalSize; 

\58.   pstSoftWareParam->stDstRoi.u64VirAddr = (HI_U64)((HI_UL)pu8VirAddr + u32TmpBufTotalSize); 

\59.   pstSoftWareParam->stDstRoi.u32Stride = SAMPLE_SVP_NNIE_ALIGN16(u32ClassNum * 

\60.     u32BboxNum * **sizeof**(HI_U32) * SAMPLE_SVP_NNIE_COORDI_NUM); 

\61.   pstSoftWareParam->stDstRoi.u32Num = 1; 

\62.   pstSoftWareParam->stDstRoi.unShape.stWhc.u32Chn = 1; 

\63.   pstSoftWareParam->stDstRoi.unShape.stWhc.u32Height = 1; 

\64.   pstSoftWareParam->stDstRoi.unShape.stWhc.u32Width = u32ClassNum * 

\65.     u32BboxNum*SAMPLE_SVP_NNIE_COORDI_NUM; 

\66.  

\67.   pstSoftWareParam->stDstScore.enType = SVP_BLOB_TYPE_S32; 

\68.   pstSoftWareParam->stDstScore.u64PhyAddr = u64PhyAddr + u32TmpBufTotalSize + u32DstRoiSize; 

\69.   pstSoftWareParam->stDstScore.u64VirAddr = (HI_U64)((HI_UL)pu8VirAddr + u32TmpBufTotalSize + u32DstRoiSize); 

\70.   pstSoftWareParam->stDstScore.u32Stride = SAMPLE_SVP_NNIE_ALIGN16(u32ClassNum * u32BboxNum * **sizeof**(HI_U32)); 

\71.   pstSoftWareParam->stDstScore.u32Num = 1; 

\72.   pstSoftWareParam->stDstScore.unShape.stWhc.u32Chn = 1; 

\73.   pstSoftWareParam->stDstScore.unShape.stWhc.u32Height = 1; 

\74.   pstSoftWareParam->stDstScore.unShape.stWhc.u32Width = u32ClassNum*u32BboxNum; 

\75.  

\76.   pstSoftWareParam->stClassRoiNum.enType = SVP_BLOB_TYPE_S32; 

\77.   pstSoftWareParam->stClassRoiNum.u64PhyAddr = u64PhyAddr + u32TmpBufTotalSize + 

\78.     u32DstRoiSize + u32DstScoreSize; 

\79.   pstSoftWareParam->stClassRoiNum.u64VirAddr = (HI_U64)((HI_UL)pu8VirAddr + u32TmpBufTotalSize + 

\80.     u32DstRoiSize + u32DstScoreSize); 

\81.   pstSoftWareParam->stClassRoiNum.u32Stride = SAMPLE_SVP_NNIE_ALIGN16(u32ClassNum***sizeof**(HI_U32)); 

\82.   pstSoftWareParam->stClassRoiNum.u32Num = 1; 

\83.   pstSoftWareParam->stClassRoiNum.unShape.stWhc.u32Chn = 1; 

\84.   pstSoftWareParam->stClassRoiNum.unShape.stWhc.u32Height = 1; 

\85.   pstSoftWareParam->stClassRoiNum.unShape.stWhc.u32Width = u32ClassNum; 

\86.  

\87.   **return** s32Ret; 

\88. } 

\89.  

\90. /* function : object detect */ 

\91. HI_S32 Yolo2HandDetectResnetClassifyCal(**uintptr_t** model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm) 

\92. { 

\93.   SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model; 

\94.   HI_S32 resLen = 0; 

\95.   **int** objNum; 

\96.   **int** ret; 

\97.   **int** num = 0; 

\98.  

\99.   ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img); 

\100.   SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "hand detect for YUV Frm to Img FAIL, ret=%#x\n", ret); 

\101.  

\102.   objNum = HandDetectCal(&img, objs); // Send IMG to the detection net for reasoning 

\103.   **for** (**int** i = 0; i < objNum; i++) { 

\104.     cnnBoxs[i] = objs[i].box; 

\105.     RectBox *box = &objs[i].box; 

\106.     RectBoxTran(box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT, 

\107.       dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height); 

\108.     SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin, box->xmax, box->ymax); 

\109.     boxs[i] = *box; 

\110.   } 

\111.   biggestBoxIndex = GetBiggestHandIndex(boxs, objNum); 

\112.   SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum); 

\113.  

\114.   // When an object is detected, a rectangle is drawn in the DSTFRM 标记 

\115.   **if** (biggestBoxIndex >= 0) { 

\116.     objBoxs[0] = boxs[biggestBoxIndex]; 

\117.     SAMPLE_PRT("class:%d\n", objs[biggestBoxIndex].cls); 

\118.     **if**(objs[biggestBoxIndex].cls == 1) 

\119.       MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN, DRAW_RETC_THICK); // 框出最大目标 

\120.     **else** 

\121.       MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_RED, DRAW_RETC_THICK); 

\122.     MaskDetectFlag(objs[biggestBoxIndex]); 

\123.   } 

\124.  

\125.   **return** ret; 

\126. } 

物联网模块重要代码：

\1. hi_void *HisignallingMsgHandle(**char** *param) 

\2. { 

\3.   unsigned **char** *recBuff = NULL;//开辟接收缓存信息的指针 

\4.   **extern** **int** g_voiceStatus; 

\5.  

\6.   **while** (1) { 

\7.     (**void**)memset_s(g_sendUartBuff, **sizeof**(g_sendUartBuff) / **sizeof**(g_sendUartBuff[0]), 

\8.       0x0, **sizeof**(g_sendUartBuff)/**sizeof**(g_sendUartBuff[0]));//设置目的缓冲区为特定值，为新申请的内存做初始化工作 

\9.     **if** (GetUartConfig(UART_RECEIVE_FLAG) == HI_TRUE) {//函数的返回值是uartDefConfig.g_uartReceiveFlag（或者uartDefConfig.g_uartLen的值） 

\10.       /* 接收数据 */ 

\11.       HisignallingMsgReceive(GetUartReceiveMsg(), GetUartConfig(UART_RECVIVE_LEN));//参数分别是uartDefConfig.g_receiveUartBuff和uartDefConfig.g_uartLen 

\12.       /* 回显数据组包 */ 

\13.       **if** (GetUartConfig(UART_RECVIVE_LEN) > (HISGNALLING_MSG_CRC32_LEN + HISIGNALLING_MSG_HEADER_TAIL_LEN)) { 

\14.         recBuff = GetUartReceiveMsg(); 

\15.         (**void**)memcpy_s(g_sendUartBuff, 

\16.           (GetUartConfig(UART_RECVIVE_LEN) - HISGNALLING_MSG_CRC32_LEN - HISIGNALLING_MSG_HEADER_TAIL_LEN), 

\17.           &recBuff[HISGNALLING_MSG_FRAME_HEADER_LEN], 

\18.           (GetUartConfig(UART_RECVIVE_LEN) - HISGNALLING_MSG_CRC32_LEN - HISIGNALLING_MSG_HEADER_TAIL_LEN));//复制 src 所指的内 

\19. 存内容的前 num 个字节到 dest 所指的内存地址上 

\20.         /* 接收到Hi3516DV300数据后，从uart1发送回显 */ 

\21.         HisignallingMsgSend(g_sendUartBuff, 

\22.           (GetUartConfig(UART_RECVIVE_LEN) - HISGNALLING_MSG_CRC32_LEN - HISIGNALLING_MSG_HEADER_TAIL_LEN)); 

\23.  

\24.  

\25.         HISIGNALLING_LOG_INFO("g_voiceStatus:%x", g_voiceStatus); 

\26.         /* 接收到Hi3516DV300数据后，从uart2口触发语音 */ 

\27.         **if** (g_voiceStatus == 1) { 

\28.           HisignallingMsgSendVoi(); 

\29.         } 

\30.          

\31.       } 

\32.       (**void**)SetUartRecvFlag(UART_RECV_FALSE); 

\33.       ResetUartReceiveMsg(); 

\34.     } **else** { 

\35.       g_uartbuffer = 0x0; 

\36.     } 

\37.     TaskMsleep(HISGNALLING_FREE_TASK_TIME); 

\38.   } 

\39. } 

 

\1. **static** hi_void *DemoEntry(**const** **char** *arg) 

\2. { 

\3.   WifiStaReadyWait(); //等待连接外网 

\4.   cJsonInit(); //处理cjson的内容 

\5.   IoTMain();//MQTT相关流程的函数 

\6.   /* 云端下发回调 */ 

\7.   IoTSetMsgCallback(DemoMsgRcvCallBack); 

\8.   /* 主动上报 */ 

\9. #ifdef TAKE_THE_INITIATIVE_TO_REPORT 

\10.   **while** (1) { 

\11.     /* 用户可以在这调用发布函数进行发布，需要用户自己写调用函数 */ 

\12.     obj_inf = transfer(); 

\13.     IotPublishSample(obj_inf); // 发布例程 

\14. #endif 

\15.     TaskMsleep(ONE_SECOND);//ONE_SECOND = 1000 (ms) 

\16.   } 

\17.   **return** NULL; 

\18. } 

 

 