# 作品简介
相对于工人，机器人有着更加廉价的成本和更加少的休息时间。不论是商场，店铺，医院，还是家里，教室，图书馆，要么是白天人声鼎沸，夜晚无人问津，要么是晚上充满人烟，白天无人看管。对于小家庭而言，雇保姆或者保安看房子显然是小题大做，对医院而言，要护士在半夜轮岗照看每一个病人也是很难实现。
基于以上场景，本作品设计提供了一个相对廉价的密闭空间的机器人自动跟随巡逻方案。
# 主要技术特点
可以实时检测识别人体，对人/处于倒地的人提供预警。
可以跟踪人体并在跟踪过程中实现避障。
