### **Face Moore**

**指导老师：张洪杰、钱彭飞              学生：薛子萱 姜怡楠 梁婷婷**

**第一部分**  **设计概述**

1.1 设计目的

  人脸检测可以实现人脸识别操作的很大一部分。根据其强度将计算资源集中在持有人脸的图像部分。图片中的人脸检测方法很复杂，因为人脸存在可变性，例如姿势、表情、位置和方向、肤色、眼镜或面部毛发的存在、相机增益的差异、照明条件和图像分辨率。人脸检测是人脸识别的第一步，也是必不可少的一步，用于检测图像中的人脸。它是物体检测的一部分，可用于许多领域，如安防、生物识别、执法、娱乐、人身安全等。

Face Moore是一个嵌入式AI视觉的脸部检测系统，该系统应用海思NNIE硬件加速检测脸部关键点检测。

本系统可以实现对人脸照片信息进行有效分析，可以分析图片中有多少张的人脸，同时得出五个关键点，即左眼、右眼、鼻尖、左嘴角、右嘴角五个点在图片中的位置，后续继续研究开发即可方便部署于智能设备，也可应用于智能检测、家庭娱乐等人机交互。

1.2 应用领域

人脸检测是机器视觉最成熟、最热门的领域，甚至于近几年，人脸识别已经逐步超过指纹识别成为生物识别的主导技术。其中最主要的技术是对人脸的关键点的检测系统。本次我们由于技术和时间的原因，我们只能选择性研究有关于人脸关键点检测的基础内容，在本次研究中，我们拟定寻找图片中相应的五个人脸关键点的坐标位置，分别为：左眼、右眼、鼻子、左嘴角、右嘴角。其拓展后主要的应用领域如下：

\1. 智能楼宇

可以根据人脸识别刷脸进入漏洞，方便安全，实时验证进出人员信息；防盗防窃，事后追溯；可对相应的重点人员布控警告等等。

2 .智慧校园

可以应用于宿管人脸验证、食堂人脸支付、关键场所人脸验证出入、上课考勤签到、考试身份验证。

3 .智慧车站

实名购票，人证合一；刷脸进站快速通过；对接公安系统，重点人员监控；节省人力成本，减少错误审核。

4 .智慧美颜

可以根据人脸关键点对人脸区域进行划分，实现美颜美妆的效果，贴图以及插画各种使用者所需。

 

1.3 主要技术特点

嵌入式AI硬件中神经网络硬件加速引擎适合边端部署，也适合处理敏感图片数据。可以有效的识别出图片中有几张人脸，并且打印出人脸的五个关键点信息，分别为：左眼、右眼、鼻尖、左嘴角、右嘴角。

1.4 关键性能指标

可以就单张照片检测其有多少张有效人脸，并且就其相应位置打印出人脸的五个关键点的坐标位置。

1、 图片大小为640*640，格式为bgr

2、 可以检测图片中有效人脸有多少张，编码并打印人脸框，左上点和右下点

3、 可以检测每张人脸对应的五个关键点的位置，分别为：左眼、右眼、鼻尖、左嘴角、右嘴角

1.5 主要创新点

（1） 借助NNIE硬件加速检测人脸关键点检测，能有效识别有效人脸个数，进一步打印关键点：左眼、右眼、鼻尖、左嘴角、右嘴角在图片中的位置打印出来，更直观的看到人脸的检测信息。

（2） 移植了一个AI基础单应用platform\svp\nnie，边端易于部署

（3） 安全启动硬件隔离系统，避免敏感图片数据侵权。

 

 

**第二部分**  **系统组成及功能说明**

2 

2.1 整体介绍

其主要流程图如下：

 

 

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps1.png) 

各个模块之间的关系如下所示：

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps2.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps3.jpg) 

首先我们以单张人脸为例，当我们以该图片中的左上角为坐标原点时，可以观察出其打印出的其相应的信息如下，数据分别代表图片中人脸的左眼、右眼、鼻尖、左嘴角、右嘴角的位置。

当图片中有多张人脸信息时，我们会发现打印出来的结果：将不同的人脸进行了编号，分别是rect 0,rect 1,rect 2•••后面紧接着对应人脸的信息。

***\*详细步骤如下：\****

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps4.png)![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps5.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps6.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps7.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps8.png)![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps9.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps10.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps11.jpg) 

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps12.png)![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps13.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps14.jpg) 

 

2.2 各模块介绍

| 序号 | 配件名称                      | 配件图片                                                     | 功能介绍                                                     |
| ---- | ----------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1    | HiSpark_WiFi_IoT-主板         | ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps15.png) | 基于Hi3861开发的WiFi主板Type-C型USB接口板载2.4G WiFi天线丰富的管脚功能 |
| 2    | HiSpark_WiFi_IoT-底板         | ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps16.png) | 专用主板的插槽记备多种传感器板接口具有丰富的丝印接口说明，降低开发难度倾留丰富的插槽、卡位、排针，提升开发自主性板载插电池接口智能小车基础底板方案(需要另外搭配智能小车的其他组件) |
| 3    | HiSpark_WiFi_IoT-显示模块     | ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps17.png) | 配备0.96英寸OLED显示屏I2C通信方式搭载两个功能用户自定义按键  |
| 4    | HiSpark_WiFi_IoT-机器人模块   | ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps18.png) | 具有丰富的机器人驱动以及接口，包括寻迹传感器接品/超声波传感器接口/电机接口/舵机接口/串口通信接口/12C接口等 |
| 5    | HiSpark_WiFi_IoT-外设扩展模块 | ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps19.png) | I2C通信方式板子NFC大线圈,灵敏度高，相应速度快,具有良好的用户体验 |
| 6    | HiSpark_WiFi_IoT-NFC板        | ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps20.png) | 具有丰富的接口，包括舵机/LED灯/红外距离传感器/光敏传感器/人体红外传感器/GPS模块/三轴陀螺仪/12C 接口等 |
| 7    | T&P四合一板                   | ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps21.png) |                                                              |
| 8    | 屏幕外壳包                    | ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps22.png) |                                                              |

 

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps23.png)![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps24.png) 

第三部分 **完成情况及性能参数**

操作步骤如下：

修改sample/nnie_face_api.c 文件的 SAMPLE_SVP_NNIE_Forward()，用了 sample/sample_nnie.c 中同名函数替换并保存。

修改程序入口，打开 sample_nnie_main.c，添加#include "nnie_face_api.h"同时，为了执行 main()，需要#define __OHOS__

执行到main()后，按参数分支，这里替换参数 4 的响应函数的内容为自己定义 的。

打开 sample/sample_nnie.c 中 SAMPLE_SVP_NNIE_Cnn()，使用 mobilefacenet 推理图片，可以在 nniefacelib-master/nnie_face_test.c 找到并替换。

指定编译工程规则 sample 开始的 path : lite_component 名，参考 ai_sample 如下:单独编译，第 19 行添加

"sample/platform/svp/nnie:hi3516dv300_nnie_sample", 然后就去 VSCODE build

先将 data/下的 wk 和图片，拷贝到本工程目录，编译过后，再按执行 app 的 c 的载入 wk 和 img 路径，拷贝开发板 ohos 下的 usrdata

拷贝可执行程序和依赖文件至开发板的mnt，挂载 SD 卡，拷贝文件，并执行

 

输入单张640×640含有人脸图片，实现单张图片的人脸检测，输出人的面部中五个部位的相关参数，五个部位依次为左眼中心、右眼中心、鼻尖、左嘴角、右嘴角，能够自动识别其位置信息，并且在VScode中输出推理结果，如下图所示：

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps25.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps26.jpg) 

此图为含一张人脸，对于含多张人脸的图片，输出结果如下:

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps27.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps28.jpg) 

性能参数：

推理速度为40ms左右，且由于RetinaFace模型本身优势位置信息精确度较高。

 

 

**第四部分**  **总结**

3 

4 

4.1 可扩展之处

目前只能够进行图片静态检测，且未能够通过输出位置信息实现复原。可以 进一步能够处理采集信息，实现在显示屏中复原并能够在屏中画出检测框和标记 五个部位的位置的功能。并且希望能够实现视频实时图片检测，且视频帧率为 20 帧。

\1. 可以进一步实现对视频流的分析，即利用摄像头采取外部人脸信息并进 行分析

\2. 后期可以将结果显示至显示屏上，使用者可以更加直观地观测到图片分析的结果。

\3. 可以进一步增强人脸关键点的识别，例如由 5 个关键点进一步演化成 10 个关键点，从而使人脸的识别更加灵敏有效。

 

4.2 心得体会

 

***\*准备阶段：\****在大二的课程阶段得知该项赛事，激发了我们对于嵌入式芯片学 习的兴趣。第一次接触到嵌入式芯片，通过相关宣讲和指导老师的介绍，逐步有 所了解 。

根据产品调研，确定芯片整体规格和模块规格，这里可以简单说下，对于整体规格，主要是运行频率，容量(RAM/FLASH)，外部接口 (UART, SPI, I2C)，模块支持(定时器), 中断支持，特殊模块(算法加速器,图形加速器)等,在结合应用 场景和成本控制需求，定义到合理的范围，芯片的工艺，功率和成本考量，决定了一定要取舍。

主要资料来自于嵌入式大赛海思赛道开发指导，利用 dokcer 镜像将ubuntu 和 windows 连接起来，方便后续工程的实现。

***\*研发阶段\**** ***\*：\****将准备阶段的海思所发放的 2022 年嵌入式大赛海思赛道开发指 导作为实验指南，转换成设计需求文档, 根据设计需求文档实现模块, 然后在芯片上进行验证。

5月YoloV3-tiny无法转成caffe模型，后兜兜转转找RetinaFace5:

我们所借用的nniefacelib是一个在海思35xx系列芯片上运行的人脸算法库，目前集成了mobilefacenet和retinaface。 

用 source insight 看C代码之间的关联，进一步将库嵌入工程中

目录hi3516dv300\sdk.linux\sample\platform\svp\nnie 像是一个 A1基础的单个应用

目录 hi3516dv300\sdk.linux\sample\taurus\ai.sample 是一个 A1 更综合的多应用,垃圾分类都是摄像头喇叭。整个工程来看:程序入口还是xxxmain.c

main 对应的.h,也可能会声明 app 应用函数sAMpLE.xxxx()(定义处会调用 真正的执行函数)

Bu1LD.gn是工程编译设定文件，source 和 include 要设定路径 ,其它不变。

wK真正的执行函数 process 过程:一般有初始化,建立缓存,推理thread, 完成释放缓存,打印结果等。

目前,基于深度学习算法的一系列目标检测算法大致分为两大流派:

两步走(two-st_ge)算法:先产生候选区域然后再进行 cNN 分类 (RCNN 系列 )

一步走(one-st_ge)算法:直接对输入图像应用算法并输出类别和相应的 定位(YOLO系列)

2019年 RetinaFace,最强开源人脸检测算法,提到了砍掉多尺寸输入来处理  NN1E不支持层的做法, Ruyisudio 里使用 prototxt 来m_ker,量化时比对 l_yer 结果。   

mobileFaceNets 是识别,RetinaFace 才是检测。

人脸检测 ,修改 svp/nnie 工程来支持人脸检测,几点不同于之前人脸识别处 的研究如下:

文件sample_nnie_main.c的修改 

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps29.jpg) ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps30.jpg)

 

防止Link时出错，原sample目录下的文件，保留同时config.c，config.h，而list.c，list.h，list_iterator.c，list_node.c放在根目录

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps31.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps32.jpg) 

修改工程目录的BUILD.gn

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps33.jpg) 

 

修改sample/sample_nnie.c文件，isLog改全局变量之后会赋值给isDebugLog来置0以禁止monitor输出Debug信息。

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps34.jpg) 

 

只推理单张图片

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps35.jpg) 

 

修改sample/nnie_face_api.c文件，再次置0以禁止monitor输出Debug信息

在svp/common/sample_comm_svp.h下，有载入模型函数SAMPLE_COMM_SVP_NNIE_LoadModel( )

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps36.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps37.jpg) 

 

结果打印显示、保存也在sample/nnie_face_api.c文件中

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps38.jpg) 

 

 

 

 

结果:sVp.NN1E.MNET( &s. stDetNnieparam ) 函数里面存为了结构体res,又 另存了as32 ResultDet 数组(200*15),不包含百分比

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps39.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps40.jpg)![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps41.png) 

 

要查缺补漏，把芯片功能相关的尽可能覆盖全，当然，此阶段随着对模块的 功能深入了解，对于准备阶段的需求也要有同步的更新纠错，有可能需求和设计 不合理，虽然满足要求，但后期应用过于复杂，也有需求余量过于狭窄，后期对 于可靠性带来风险，查缺补漏是整个设计阶段都要时刻注意的问题。

测试出问题了，就需要配合设计分析原因和解决，解决完成后至少还要完整进行一轮测试，对于 A 的修改有可能影响 B，解决 A 问题会修改A 和 B，可能会导致 B 本来正常的也出错，很难保证没有错误。但尽可能的覆盖十分重要，不然后期会带来很多麻烦。

***\*完成\*******\*阶段\**** ***\*：\****

具体实验步骤如下：

修改sample/nnie_face_api.c 文件的 SAMPLE_SVP_NNIE_Forward()，用了 sample/sample_nnie.c 中同名函数替换并保存。

修改程序入口，打开 sample_nnie_main.c，添加#include "nnie_face_api.h"同时，为了执行 main()，需要#define __OHOS__

执行到main()后，按参数分支，这里替换参数 4 的响应函数的内容为自己定义 的。

打开 sample/sample_nnie.c 中 SAMPLE_SVP_NNIE_Cnn()，使用 mobilefacenet 推理图片，可以在 nniefacelib-master/nnie_face_test.c 找到并替换。

指定编译工程规则 sample 开始的 path : lite_component 名，参考 ai_sample 如下:单独编译，第 19 行添加

"sample/platform/svp/nnie:hi3516dv300_nnie_sample", 然后就去 VSCODE build

先将 data/下的 wk 和图片，拷贝到本工程目录，编译过后，再按执行 app 的 c 的载入 wk 和 img 路径，拷贝开发板 ohos 下的 usrdata

拷贝可执行程序和依赖文件至开发板的mnt，挂载 SD 卡，拷贝文件，并执行

在完成相应的嵌入式芯片如何擦除、编译、烧录后，我们以海思官网所发放 的 AiSample 例子为雏形来实现我们拟定实现的人脸关键点检测系统，由于时间 以及知识的局限性问题，我们有很多功能区域尚未开发成功，只取得了一部分的 结果。毕竟团队成员都只是大二本专业基础课程学习，很多跨学科的功能区域尚未开发，但相信我们的坚持和自信必然会支持我们取得更多更好的结果。

 

**第五部分**  **参考文献**

 

[1] JiankangDeng,JiaGuo,YuxiangZhou,JinkeYu,IreneKotsia,andStefanosZaf eiriou :RetinaFace: Single-stage Dense Face Localisation in the Wild.I n ArXiv.cs.CV, 2019.5

[2] R. Alp G  uler, N. Neverova, and I. Kokkinos. Densepose: Dense human pose estimation in the wild. In CVPR, 2018.2, 3  [3] R. Alp Guler, G. Trigeorgis, E. Antonakos, P. Snape,

[3] S. Zafeiriou, and I. Kokkinos. Densereg: Fully convolutional dense shape regression in-the-wild. In CVPR, 2017. 1

[4] A. Bulat and G. Tzimiropoulos. How far are we from solv- ing the 2d & 3d face alignment problem?(and a dataset of   230,000 3d facial landmarks). In ICCV, 2017. 6

[5] Z. Cai, Q. Fan, R. S. Feris, and N. Vasconcelos. A unified multi-scale deep convolutional neural network for fast object detection. In ECCV, 2016. 5

[6] D. Chen, G. Hua, F. Wen, and J. Sun. Supervised transformer network for efficient face detection. In ECCV, 2016. 2

**第六部分**  **附录**

\1. 思路：

选择合适模型，部署，推理图片，检查结果

 

2、  重点代码

(1) SAMPLE_SVP_NNIE_Forward() SAMPLE_SVP_NNIE_Forward 中

\1. SAMPLE_COMM_SVP_FlushCache 函数主要实现将内存数据刷新到内存中；

\2. HI_MPI_SVP_NNIE_Forward 函数同时对输入样本(s)进行 CNN 预测，对对应样 本(s)进行输出响应；

\3. HI_MPI_SVP_NNIE_Query 函数用于查询 nnie 上运行函数的状态，在阻塞模式 下，系统等待，直到被查询的函数被调用；在非阻塞模式下，查询当前状态，不做任何操作。

 

(2) 修改svp/nnie工程

 

1.文件sample_nnie_main.c的修改 

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps42.jpg) ![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps43.jpg)

 

\2. 防止Link时出错，原sample目录下的文件，保留同时config.c，config.h，而list.c，list.h，list_iterator.c，list_node.c放在根目录

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps44.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps45.jpg) 

\3. 修改工程目录的BUILD.gn

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps46.jpg) 

 

\4. 修改sample/sample_nnie.c文件，isLog改全局变量之后会赋值给isDebugLog来置0以禁止monitor输出Debug信息。

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps47.jpg) 

\5. 只推理单张图片

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps48.jpg) 

\6. 修改sample/nnie_face_api.c文件，再次置0以禁止monitor输出Debug信息

 

\7. 在svp/common/sample_comm_svp.h下，有载入模型函数SAMPLE_COMM_SVP_NNIE_LoadModel( )

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps49.jpg) 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps50.jpg) 

 

结果打印显示、保存也在sample/nnie_face_api.c文件中

 

![img](file:///C:\Users\19826\AppData\Local\Temp\ksohtml21460\wps51.jpg) 

 

 

3、检测结果

(1) 单张人脸：

load sys for Hi3516CV500...OK!

load chnl.ko for Hi3516CV500...OK!

loEnter hiirq_open

ad region for Hi3516CV500...OK!

load gdc for Hi3516CV500...OK!

load vgs for Hi3516CV500...OK!

load dis for Hi3516CV500...OK!

load vi for Hi3516CV500...OK !

load isp for Hi3516CV500...OK !

load vpss for Hi3516CV500...OK!

load vo for Hi3516CV500...OK!

load vedu for Hi3516CV500...OK!

load rc for Hi3516CV500...OK!

load venc for Hi3516CV500...OK!

load h264e for Hi3516CV500...OK!

load h265e for Hi3516CV500...OK!

load jpege for Hi3516CV500...OK!

load jpegd for Hi3516CV500...OK!

load vdec for Hi3516CV500...OK!

load ive for Hi3516CV500...OK!

load nnie for Hi3516CV500...OK!

SDK init ok...

Face Detector Start!!!

NNIE_FACE_DETECTOR_INIT got args : 0.000000, 1610612736

[Level]:Debug,[Func]:SAMPLE_COMM_SVP_CheckSysInit [Line]:90 [Info]:Svp mpi init ok!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:921 [Info]:Cnn Load model!





3、检测结果

（1）单张人脸：

load sys for Hi3516CV500...OK!

load chnl.ko for Hi3516CV500...OK!

loEnter hiirq_open

ad region for Hi3516CV500...OK!

load gdc for Hi3516CV500...OK!

load vgs for Hi3516CV500...OK!

load dis for Hi3516CV500...OK!

load vi for Hi3516CV500...OK !

load isp for Hi3516CV500...OK !

load vpss for Hi3516CV500...OK!

load vo for Hi3516CV500...OK!

load vedu for Hi3516CV500...OK!

load rc for Hi3516CV500...OK!

load venc for Hi3516CV500...OK!

load h264e for Hi3516CV500...OK!

load h265e for Hi3516CV500...OK!

load jpege for Hi3516CV500...OK!

load jpegd for Hi3516CV500...OK!

load vdec for Hi3516CV500...OK!

load ive for Hi3516CV500...OK!

load nnie for Hi3516CV500...OK!

SDK init ok...

Face Detector Start!!!

 NNIE_FACE_DETECTOR_INIT got args : 0.000000, 1610612736

[Level]:Debug,[Func]:SAMPLE_COMM_SVP_CheckSysInit [Line]:90 [Info]:Svp mpi init ok!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:921 [Info]:Cnn Load model!

 

 NNIE_FACE_DETECTOR_INIT changes args : 0.700000, 0

 

 FACE_DETECTOR_PARAM_INIT got args : 0.700000, 0

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:937 [Info]:Cnn parameter initialization!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:942 [Info]:NNIE AddTskBuf!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:947 [Info]:NNIE AddTskBuf end!

 

 

 ========== =========== ========== ==========

 

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_GET [Line]:971 [Info]:Cnn start!

Info, open file!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_GET [Line]:977 [Info]:Load Img!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_GET [Line]:986 [Info]:Forward!

 

[inference]===== TIME SPEND: 34 ms =====

result rect: 0, 173.836807, 23.768631, 516.913208, 470.981384

result lds: 1, 267.750000, 195.125000

result lds: 2, 419.375000, 210.625000

result lds: 3, 333.000000, 281.750000

result lds: 4, 266.125000, 347.500000

result lds: 5, 391.625000, 361.500000

 

[post process]===== TIME SPEND: 6 ms =====

hiirq:disable irq_num:65

hiirq:disable irq_num:72

hiirq:disable irq_num:67

[hiirq: ,line:560]irq_num:67 not enable

hiirq:disable irq_num:68

[hiirq: ,line:560]irq_num:68 not enable

hiirq:disable irq_num:69

[hiirq: ,line:560]irq_num:69 not enable

hiirq:disable irq_num:60

[hiirq: ,line:560]irq_num:60 not enable

hiirq:disable irq_num:58

hiirq:disable irq_num:57

hiirq:disable irq_num:56

hiirq:disable irq_num:59

hiirq:disable irq_num:64

[Level]:Debug,[Func]:SAMPLE_COMM_SVP_CheckSysExit [Line]:101 [Info]:Svp mpi exit ok!

hiirq:disable irq_num:71

unload nnie for Hi3516CV500...OK!

unload ive for Hi3516CV500...OK!

unload vdec for Hi3516CV500...OK!

unload jpegd for Hi3516CV500...OK!

unload jpege for Hi3516CV500...OK!

unload h26Enter hiirq_release.

5e for Hi3516CV500...OK!

unload h264e for Hi3516CV500...OK!

unload venc for Hi3516CV500...OK!

unload rc for Hi3516CV500...OK!

unload vedu for Hi3516CV500...OK!

unload chnl.ko for Hi3516CV500...OK!

unload vo ....OK!

unload vpss ....OK!

unload isp for Hi3516CV500...OK!

unload vi for Hi3516CV500...OK!

unload dis for Hi3516CV500...OK!

unload vgs for Hi3516CV500...OK!

unload gdc for Hi3516CV500...OK!

unload region for Hi3516CV500...OK!

SDK exit ok...

\# 01-01 00:07:31.399 211 211 I 0A001/ueventd: [ueventd_main.c:35]poll ueventd socket timeout, ueventd exit

01-01 00:07:31.400 1 1 I 0A001/Init: [init_common_service.c:421]Reap service ueventd, pid 211.

01-01 00:07:31.400 1 1 I 0A001/Init: [init_service_socket.c:192]Start to monitor socket, fd:4 service name:ueventd

（2）三张人脸：

load sys for Hi3516CV500...OK!

load chnl.ko for Hi3516CV500...OK!

loEnter hiirq_open

ad region for Hi3516CV500...OK!

load gdc for Hi3516CV500...OK!

load vgs for Hi3516CV500...OK!

load dis for Hi3516CV500...OK!

load vi for Hi3516CV500...OK !

load isp for Hi3516CV500...OK !

load vpss for Hi3516CV500...OK!

load vo for Hi3516CV500...OK!

load vedu for Hi3516CV500...OK!

load rc for Hi3516CV500...OK!

load venc for Hi3516CV500...OK!

load h264e for Hi3516CV500...OK!

load h265e for Hi3516CV500...OK!

load jpege for Hi3516CV500...OK!

load jpegd for Hi3516CV500...OK!

load vdec for Hi3516CV500...OK!

load ive for Hi3516CV500...OK!

load nnie for Hi3516CV500...OK!

SDK init ok...

Face Detector Start!!!

 NNIE_FACE_DETECTOR_INIT got args : 0.000000, 1610612736

[Level]:Debug,[Func]:SAMPLE_COMM_SVP_CheckSysInit [Line]:90 [Info]:Svp mpi init ok!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:921 [Info]:Cnn Load model!

 

 NNIE_FACE_DETECTOR_INIT changes args : 0.700000, 0

 

 FACE_DETECTOR_PARAM_INIT got args : 0.700000, 0

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:937 [Info]:Cnn parameter initialization!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:942 [Info]:NNIE AddTskBuf!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_INIT [Line]:947 [Info]:NNIE AddTskBuf end!

 

 

 ========== =========== ========== ==========

 

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_GET [Line]:971 [Info]:Cnn start!

Info, open file!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_GET [Line]:977 [Info]:Load Img!

[Level]:Info,[Func]:NNIE_FACE_DETECTOR_GET [Line]:986 [Info]:Forward!

 

[inference]===== TIME SPEND: 34 ms =====

result rect: 0, 134.341858, 245.862183, 194.158142, 339.887817

result lds: 1, 170.906250, 282.640625

result lds: 2, 178.718750, 279.656250

result lds: 3, 193.109375, 290.421875

result lds: 4, 182.171875, 314.796875

result lds: 5, 187.000000, 313.093750

result rect: 1, 457.641815, 85.661736, 521.951904, 171.713257

result lds: 1, 467.140625, 122.359375

result lds: 2, 481.703125, 119.218750

result lds: 3, 468.093750, 139.265625

result lds: 4, 481.406250, 153.015625

result lds: 5, 492.093750, 149.828125

result rect: 2, 226.379150, 321.239288, 270.058350, 385.104462

result lds: 1, 255.765625, hiirq:disable irq_num:65

346.015625

result lds: 2, 261.015625, 3[hiirq: ,line:560]irq_num:65 not enable

42.937500

result lds: 3, 270.343750, 35hiirq:disable irq_num:72

4.640625

result lds: 4, 260.562500, 369[hiirq: ,line:560]irq_num:72 not enable

.187500

result lds: 5, 264.343750, 367.125000

 

[post process]===== TIME SPEND: 5 ms =====

hiirq:disable irq_num:67

hiirq:disable irq_num:68

[hiirq: ,line:560]irq_num:68 not enable

hiirq:disable irq_num:69

hiirq:disable irq_num:60

[hiirq: ,line:560]irq_num:60 not enable

hiirq:disable irq_num:58

hiirq:disable irq_num:57

hiirq:disable irq_num:56

hiirq:disable irq_num:59

hiirq:disable irq_num:64

[Level]:Debug,[Func]:SAMPLE_COMM_SVP_CheckSysExit [Line]:101 [Info]:Svp mpi exit ok!

hiirq:disable irq_num:71

unload nnie for Hi3516CV500...OK!

unload ive for Hi3516CV500...OK!

unload vdec for Hi3516CV500...OK!

unload jpegd for Hi3516CV500...OK!

unload jpege for Hi3516CV500...OK!

unloEnter hiirq_release.

ad h265e for Hi3516CV500...OK!

unload h264e for Hi3516CV500...OK!

unload venc for Hi3516CV500...OK!

unload rc for Hi3516CV500...OK!

unload vedu for Hi3516CV500...OK!

unload chnl.ko for Hi3516CV500...OK!

unload vo ....OK!

unload vpss ....OK!

unload isp for Hi3516CV500...OK!

unload vi for Hi3516CV500...OK!

unload dis for Hi3516CV500...OK!

unload vgs for Hi3516CV500...OK!

unload gdc for Hi3516CV500...OK!

unload region for Hi3516CV500...OK!

SDK exit ok...

\# 

 