/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <hi_stdlib.h>
#include <hi_uart.h>
#include "uart.h"
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex.h"
#include "iot_gpio.h"
#include "ohos_init.h"
#include "cmsis_os2.h"

#define LED_TEST_GPIO 9
#define LED_INTERVAL_TIME_US 300000

UartDefConfig uartDefConfig = {0};

void UartInit(void)
{
    hi_u32 ret = 0;
    IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);

    IoSetFunc(HI_IO_NAME_GPIO_11, IOT_IO_FUNC_GPIO_11_UART2_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_12, IOT_IO_FUNC_GPIO_12_UART2_RXD);
    IotUartAttribute uartAttr = {
        .baudRate = 115200, /* baudRate: 115200 */
        .dataBits = 8, /* dataBits: 8bits */
        .stopBits = 1, /* stop bit */
        .parity = 0,
    };
    /* Initialize uart driver */
    ret = IoTUartInit(HI_UART_IDX_1, &uartAttr);
    if (ret != HI_ERR_SUCCESS) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
    uartAttr.baudRate = 9600;
    ret = IoTUartInit(HI_UART_IDX_2, &uartAttr);
    if (ret != HI_ERR_SUCCESS) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
    
}
// static hi_void *UartDemoTask(char *param)
// {
//     hi_u8 uartBuff[UART_BUFF_SIZE] = {0};
//     hi_unref_param(param);
//     printf("Initialize uart demo successfully, please enter some datas via DEMO_UART_NUM port...\n");
//     Uart1GpioCOnfig();
//     for (;;) {
//     //     uartDefConfig.g_uartLen = IoTUartRead(DEMO_UART_NUM, uartBuff, UART_BUFF_SIZE);
//     //     if ((uartDefConfig.g_uartLen > 0) && (uartBuff[0] == 0xaa) && (uartBuff[1] == 0xcc)) {
//     //         for (int i = 0; i < UART_BUFF_SIZE; i++) {
//     //             printf("0x%x", uartBuff[i]);
//     //         }
//     //         printf("\r\n");
//     //     }
//     // IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
//     // usleep(LED_INTERVAL_TIME_US);
//     // IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
//     // C
//     //     TaskMsleep(20); /* 20:sleep 20ms */
//         printf("11111111111111111111111111\n");
//         uartBuff[0] = 0xAA;
//         uartBuff[1] = 0x55;
//         uartBuff[2] = 0x01;
//         uartBuff[3] = 0x00;
//         uartBuff[4] = 0x55;
//         uartBuff[5] = 0xAA;
//         IoTUartWrite(DEMO_UART_NUM, uartBuff, 6);
//         usleep(1000000);
//     }
//     return HI_NULL;
// }
