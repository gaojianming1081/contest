
class Paraser():
    def __init__(self):
        self.lightState = {"STRAIGHT_NORTH_SOUTH_GREEN": "南北直行绿灯",
                           "STRAIGHT_NORTH_SOUTH_YELLOW": "南北直行黄灯",
                           "TURNLEFT_NORTH_SOUTH_GREEN": "南北左转绿灯",
                           "TURNLEFT_NORTH_SOUTH_YELLOW": "南北左转黄灯",
                           "STRAIGHT_EAST_WEST_GREEN": "东西直行绿灯",
                           "STRAIGHT_EAST_WEST_YELLOW": "东西直行黄灯",
                           "TURNLEFT_EAST_WEST_GREEN": "东西左转绿灯",
                           "TURNLEFT_EAST_WEST_YELLOW": "东西左转黄灯"}
        self.regions = ["北左转道", "北直行道", "南左转道", "南直行道", "东左转道", "东直行道", "西左转道", "西直行道"]
        self.congression = ["流畅", "拥堵", "重度拥堵"] # 严重拥堵

    def info2str(self,info):
        currentTime = info["current_time"]
        try:
            currentLight = self.lightState[info["current_light"]]  ###################################################################
        except:
            currentLight = ""
        currentTraffic = ""
        for i in range(8):
            currentTraffic = currentTraffic + "  " + self.regions[i] + "：" + self.congression[(info["current_congestion"])[i]] + "\n"

        infoStr = "当前交通灯状态：{}\n\n当前车流拥堵状态：\n{}\n".format(currentLight, currentTraffic)
        return infoStr

