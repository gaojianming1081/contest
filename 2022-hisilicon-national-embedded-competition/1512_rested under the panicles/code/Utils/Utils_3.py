def dhash(image): # 差异值哈希算法
    image = cv2.resize(image, (9, 8), interpolation=cv2.INTER_CUBIC)  # 将图片转化为8*8
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)   # 将图片转化为灰度图
    dhash_str = ''
    for i in range(8):
        for j in range(8):
            if gray[i, j] > gray[i, j + 1]:
                dhash_str = dhash_str + '1'
            else:
                dhash_str = dhash_str + '0'
                result = ''
        for i in range(0, 64, 4):
            result += ''.join('%x' % int(dhash_str[i: i + 4], 2))
        print("dhash值",result)
    return result