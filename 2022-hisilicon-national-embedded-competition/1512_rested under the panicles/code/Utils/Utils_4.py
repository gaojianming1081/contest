url_base=''  # 目的网站
response=requests.get(url,headers=headers)#访问网页
html=response.text
urls=re.findall('<p><a href="(.*?)" title=".*?" target="_blank">.*?</a></p>',html)#获取链接
for url in urls:#遍历链接
    url=""+url#规范格式
    response=requests.get(url,headers=headers)
    html1=response.text
    dir_name=re.findall('<h1>(.*?)</h1>',html1) # 自动创建 —— 标题名为文件夹名
    if not os.path.exists(str(dir_name))  :#  判断文件夹有无，没有则生成
        os.mkdir(str(dir_name))
    pics=re.findall('<div class="il_img"><a href=".*?" title=".*?" target="_blank"><img src="(.*?)" alt=".*?"></a>',html1)#获取图片
    for pic in pics:#遍历图片
        file_name=pic.split('/')[-1]#以/为分隔符，取最后一段作为文件名
        pic="http:"+pic
        response=requests.get(pic,headers=headers)
        with open(str(dir_name) + '/' + file_name,'wb') as f:#/为分级 wb代表二进制模式文件，允许写入文件，
        f.write(response.content)
