def img_predict(img_path):
    data_transform = transforms.Compose(
        [transforms.Resize(256),
         transforms.ToTensor(),
         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    model = resnet34(num_classes=6).to(device)  # 创建模型

    weights_path = ori_path + "/Yang.pth"  # 加载模型权重
    assert os.path.exists(weights_path), "file: '{}' dose not exist.".format(weights_path)
    model.load_state_dict(torch.load(weights_path, map_location=device))

    try:
        json_file = open(ori_path + '/class.json', 'r')  # 加载类型属性
        class_indict = json.load(json_file)
    except Exception as e:
        print(e)
        exit(-1)

    # load image
    img_path = str(img_path)
    assert os.path.exists(img_path), "file: '{}' dose not exist.".format(img_path)
    img = Image.open(img_path).convert('RGB')
    plt.imshow(img)
    img = data_transform(img)
    img = torch.unsqueeze(img, dim=0)

    model.eval()
    with torch.no_grad():
        output = torch.squeeze(model(img.to(device))).cpu()
        predict = torch.softmax(output, dim=0)
        predict_cla = torch.argmax(predict).cpu().numpy()
    ImgIdentResult = class_indict[str(predict_cla)][1:]
    PredictAccuracy = []
    PredictAccuracy.append(round(predict[predict_cla].cpu().tolist() * 100, 2))
    PredictAccuracy = array(PredictAccuracy).tolist()

    return ImgIdentResult, PredictAccuracy