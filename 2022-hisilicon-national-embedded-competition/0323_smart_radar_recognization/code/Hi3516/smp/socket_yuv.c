#include "socket_yuv.h"

#define MAXLINE 4096

int listenfd;

int bind_socket()
{
    struct sockaddr_in servaddr;

    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        printf("create socket error: %s(errno: %d)\n", strerror(errno), errno);
        return 0;
    }
    printf("----init socket----\n");

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(6666);
    //设置端口可重用
    int contain;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &contain, sizeof(int));

    if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
    {
        printf("bind socket error: %s(errno: %d)\n", strerror(errno), errno);
        // return 0;
    }
    printf("----bind sucess----\n");

    return 0;
}

int socket_test()
{
    int connfd;

    char buff[4096];
    FILE *fp;
    int n;

    if (listen(listenfd, 10) == -1)
    {
        printf("listen socket error: %s(errno: %d)\n", strerror(errno), errno);
        return 0;
    }

    printf("======waiting for client's request======\n");

    struct sockaddr_in client_addr;
    socklen_t size = sizeof(client_addr);
    if ((connfd = accept(listenfd, (struct sockaddr *)&client_addr, &size)) == -1)
    {
        printf("accept socket error: %s(errno: %d)", strerror(errno), errno);
    }
    else
    {
        printf("Get connection!\n");
    }

    if ((fp = fopen("receive.yuv", "wb")) == NULL)
    {
        printf("File exists\n");
        close(listenfd);
        exit(1);
    }

    while (1)
    {
        n = read(connfd, buff, MAXLINE);
        if (n == 0)
            break;
        fwrite(buff, 1, n, fp);
    }
    buff[n] = '\0';
    printf("recv msg from client: %s\n", buff);
    close(connfd);
    fclose(fp);

    // close(listenfd);
    return 0;
}