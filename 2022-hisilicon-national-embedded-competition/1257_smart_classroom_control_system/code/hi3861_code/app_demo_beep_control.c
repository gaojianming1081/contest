 /* Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "iot_watchdog.h"
#include "hi_io.h"

#define LED_INTERVAL_TIME_US 500000
#define LED_TASK_STACK_SIZE 512
#define LED_TASK_PRIO 25

static int g_kongState = 1;
static int g_menState = 1;
static int g_dengState = 1;
static int g_touState = 1;

static int g_iState = 0;

#define IOT_GPIO_IDX_kong 12 // for kong
#define IOT_GPIO_KONGKEY   2 //D2口按钮控制空调

#define IOT_GPIO_IDX_men 10 // for men
#define IOT_GPIO_MENKEY   0 //D0口按钮控制门锁

#define IOT_GPIO_IDX_deng 11 // for deng
#define IOT_GPIO_DENGKEY   1 //D1口按钮控制灯


static void *kongCntrolDemo(const char *arg)
{
    while (1) {
        if (g_kongState) {
            IoTGpioSetOutputVal(IOT_GPIO_IDX_kong, IOT_GPIO_VALUE0);
        } else {
            IoTGpioSetOutputVal(IOT_GPIO_IDX_kong, IOT_GPIO_VALUE1);
        }

        if (g_menState) {
            IoTGpioSetOutputVal(IOT_GPIO_IDX_men, IOT_GPIO_VALUE0);
        } else {
            IoTGpioSetOutputVal(IOT_GPIO_IDX_men, IOT_GPIO_VALUE1);
        }

        if (g_dengState) {
            IoTGpioSetOutputVal(IOT_GPIO_IDX_deng, IOT_GPIO_VALUE0);
        } else {
            IoTGpioSetOutputVal(IOT_GPIO_IDX_deng, IOT_GPIO_VALUE1);
        }

        if (g_iState == 0xffff) {
            g_iState = 0;
            break;
        }
    }
}

static void OnkongButtonPressed(const char *arg)
{
    (void) arg;
    g_kongState = !g_kongState;
    usleep(LED_INTERVAL_TIME_US);
    printf("PRESS_KEY!\n");
    g_iState++;
}

static void OnmenButtonPressed(const char *arg)
{
    (void) arg;
    g_menState = !g_menState;
    usleep(LED_INTERVAL_TIME_US);
    printf("PRESS_KEY!\n");
    g_iState++;
}

static void OndengButtonPressed(const char *arg)
{
    (void) arg;
    g_dengState = !g_dengState;
    usleep(LED_INTERVAL_TIME_US);
    printf("PRESS_KEY!\n");
    g_iState++;
}


static void StartkongCntrolDemo(void)
{
    osThreadAttr_t attr;
    IoTGpioInit(IOT_GPIO_KONGKEY);
    IoSetFunc(IOT_GPIO_KONGKEY, 0);
    IoTGpioSetDir(IOT_GPIO_KONGKEY, IOT_GPIO_DIR_IN);
    IoSetPull(IOT_GPIO_KONGKEY, IOT_IO_PULL_UP);
    IoTGpioRegisterIsrFunc(IOT_GPIO_KONGKEY, IOT_INT_TYPE_EDGE, IOT_GPIO_EDGE_FALL_LEVEL_LOW, OnkongButtonPressed, NULL);
    IoTGpioInit(IOT_GPIO_IDX_kong);
    IoTGpioSetDir(IOT_GPIO_IDX_kong, IOT_GPIO_DIR_OUT);


    IoTGpioInit(IOT_GPIO_MENKEY);
    IoSetFunc(IOT_GPIO_MENKEY, 0);
    IoTGpioSetDir(IOT_GPIO_MENKEY, IOT_GPIO_DIR_IN);
    IoSetPull(IOT_GPIO_MENKEY, IOT_IO_PULL_UP);
    IoTGpioRegisterIsrFunc(IOT_GPIO_MENKEY, IOT_INT_TYPE_EDGE, IOT_GPIO_EDGE_FALL_LEVEL_LOW, OnmenButtonPressed, NULL);
    IoTGpioInit(IOT_GPIO_IDX_men);
    IoTGpioSetDir(IOT_GPIO_IDX_men, IOT_GPIO_DIR_OUT);
    IoTWatchDogDisable();

    IoTGpioInit(IOT_GPIO_DENGKEY);
    IoSetFunc(IOT_GPIO_DENGKEY, 0);
    IoTGpioSetDir(IOT_GPIO_DENGKEY, IOT_GPIO_DIR_IN);
    IoSetPull(IOT_GPIO_DENGKEY, IOT_IO_PULL_UP);
    IoTGpioRegisterIsrFunc(IOT_GPIO_DENGKEY, IOT_INT_TYPE_EDGE, IOT_GPIO_EDGE_FALL_LEVEL_LOW, OndengButtonPressed, NULL);
    IoTGpioInit(IOT_GPIO_IDX_deng);
    IoTGpioSetDir(IOT_GPIO_IDX_deng, IOT_GPIO_DIR_OUT);


    attr.name = "kongCntrolDemo";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024; /* 堆栈大小为1024 */
    attr.priority = osPriorityNormal;

    if (osThreadNew((osThreadFunc_t)kongCntrolDemo, NULL, &attr) == NULL) {
        printf("[LedExample] Falied to create LedTask!\n");


    }
}

APP_FEATURE_INIT(StartkongCntrolDemo);