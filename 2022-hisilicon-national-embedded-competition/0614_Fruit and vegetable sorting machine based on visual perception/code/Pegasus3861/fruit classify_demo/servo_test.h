#ifndef __SERVO_TEST_H__
#define __SERVO_TEST_H__

#define LED_TEST_GPIO 9 // for hispark_pegasus
#define IOT_GPIO_SERVO_1 2
#define IOT_GPIO_SERVO_2 4


void servo_1_0(void);
void servo_1_inv90(void);
void servo_1_90(void);
void servo_1_inv45(void);
void servo_1_45(void);
void servo_2_0(void);
void servo_2_inv90(void);
void servo_2_90(void);
void servo_2_inv45(void);
void servo_2_45(void);

#endif