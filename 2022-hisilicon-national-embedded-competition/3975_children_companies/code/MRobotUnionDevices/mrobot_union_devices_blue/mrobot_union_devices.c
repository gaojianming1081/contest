#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include <string.h>
#include "hi_io.h" //==hi_io_set_func()、hi_io_set_pull()
#include "iot_gpio.h"
#include "iot_pwm.h"

#include "bluetooth.h"

char get_data[10] = {'0'};
int data_len = 2;
int flag_water = 0;
/**********************Fan**************************/
static void FanInit()
{
    IoTGpioInit(8);
    //设置GPIO_8方向为PWM输出
    hi_io_set_func(8, HI_IO_FUNC_GPIO_8_PWM1_OUT);
    IoTPwmInit(1); //==初始化pwm1
    // IoTPwmStart(1,50,80000); //==配置pwm1输出参数：占空比50%、频率160M/80000=2KHz
}
static void FanCtr()
{
    //通过串口1接收MRobot2Fan data
    // Bluetooth_read(get_data, data_len);
    // printf("Uart1 read data:%s \n", get_data);
    if (get_data[0] == '1' && get_data[1] == '0') // 10
    {
        printf("turn off fan\n");
        IoTPwmStop(1); // turn off fan
    }
    if (get_data[0] == '1' && get_data[1] == '1') // 11
    {
        printf("turn on fan\n");
        IoTPwmStart(1, 90, 80000); // turn on fan
    }
}

/*****************Door*************************/
static void DoorInit()
{
    //初始化GPIO_10
    IoTGpioInit(10);
    hi_io_set_func(10, HI_IO_FUNC_GPIO_10_GPIO);
    IoTGpioSetDir(10, IOT_GPIO_DIR_OUT);
}
/* @brief  Servo  control *
 @param angle  input value: 0-200 *
*/
void My_servo(int angle)
{
    int j = 0;
    int k = 20000 / 200; //实际应该是20000/180
    angle = k * angle;
    for (j = 0; j < 5; j++)
    {
        IoTGpioSetOutputVal(10, 1);
        hi_udelay(angle); // angle ms
        IoTGpioSetOutputVal(10, 0);
        hi_udelay(20000 - angle); //
    }                             // 20ms 控制舵机
}
static void DoorCtr()
{
    // Bluetooth_read(get_data, data_len);
    // printf("Uart1 read data:%s", get_data);
    if (get_data[0] == '2' && get_data[1] == '1') // 21
    {
        //模拟20ms周期 PWM  控制舵机开门
        printf("open door\n");
        My_servo(1000); //开门
        printf("close door\n");
        My_servo(10000); //机械复位
    }
}

/**********************Lamb**********************/
static void LambInit()
{
    IoTGpioInit(9);
    //设置GPIO_8方向为PWM输出
    hi_io_set_func(9, HI_IO_FUNC_GPIO_9_PWM0_OUT);
    IoTPwmInit(0); //==初始化pwm0
                   //是继电器控制开开关，给个高电位也可，这里使用PWM驱动，可调节亮度
}

static void LambCtr()
{
    // Bluetooth_read(get_data, data_len);
    // printf("Uart1 read data:%s", get_data);
    if (get_data[0] == '3' && get_data[1] == '0') // 30
    {
        printf("turn off lamb\n");
        IoTPwmStop(0); // turn off lamb
    }
    if (get_data[0] == '3' && get_data[1] == '1') // 31
    {
        printf("turn on lamb \n");
        IoTPwmStart(0, 90, 80000); // turn on lamb
    }
}

/***********************Water***********************/
static void WaterInit()
{
    IoTGpioInit(11);
    //设置GPIO_8方向为PWM输出
    hi_io_set_func(11, HI_IO_FUNC_GPIO_11_PWM2_OUT);
    IoTPwmInit(2); //==初始化pwm1
}
static void WaterCtr()
{
    // Bluetooth_read(get_data, data_len);
    // printf("Uart1 read data:%s", get_data);
    if (get_data[0] == '4' && get_data[1] == '1') // 40
    {
        if (flag_water == 0)
        {
            printf(" water\n");
            IoTPwmStart(2, 90, 80000); // turn on water
            osDelay(200);              //定时间的
            IoTPwmStop(2);             // turn off water
            get_data[0] == '0';
            flag_water = 1;
        }
    }
    else{ //触发了不浇水指令
        flag_water=0;
    }

}

static void BlueTask(void *arg)
{
    (void)arg;
    sleep(2);
    int ret = 1;
    ret = Bluetooth_Init();
    if (ret != 0)
    {
        printf("Uart1 init failed! \n");
        // return;
    }
    FanInit();
    DoorInit();
    LambInit();
    WaterInit();
    char send_data[] = "AB";
    // char uart_buf[8] = {0};
    printf("get_data=%s\n", send_data);

    while (1)
    {
        // usleep(50000); // 0.5s
        // Bluetooth_write(send_data, 2);
        Bluetooth_read(get_data, 2);
        printf("get_data = %s\n", get_data);
        FanCtr();
        // DoorCtr();
        LambCtr();
        WaterCtr();
        get_data[0] == '0';
    }
}

static void BlueDemo(void)
{
    osThreadAttr_t attr;

    attr.name = "BlueTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096;
    attr.priority = osPriorityNormal;

    if (osThreadNew(BlueTask, NULL, &attr) == NULL)
    {
        printf("[BlueDemo] Falied to create BluetoothTask!\n");
    }
}

APP_FEATURE_INIT(BlueDemo);
