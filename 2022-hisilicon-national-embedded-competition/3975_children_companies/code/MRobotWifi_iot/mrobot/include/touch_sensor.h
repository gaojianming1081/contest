#ifndef __TOUCH_SENSOR__
#define __TOUCH_SENSOR__

#define TOUCH_ADC9 9 // touch sensor

float Get_touch_val(void);
void TouchSensorInit(void);

#endif