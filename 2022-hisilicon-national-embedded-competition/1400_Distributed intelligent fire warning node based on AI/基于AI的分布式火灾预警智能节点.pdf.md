## 基于AI的分布式火灾预警智能节点

**第一部分**  **设计概述**

1.1  设计目的

现代城市发展与消防安全问题突出，近年来，重大火灾事故频频发生，以及传统火灾探测系统无法满足大空间消防安全需求的难题。给国家和人民群众的生命和财产安全造成了巨大的损失，也敲响了火灾防范预警重要性的警钟。本项目通过摄像头采集实时视频图像，通过Resnet18检测网检测视频图像中出现的烟雾或火焰等早期火灾征兆，然后设备指示灯，Oled屏幕，微信小程序等方式进行警告，从而完成对火灾的预警。

1.2  应用领域

厂房仓库、医院、学校、森林、公园

1.3  主要技术特点

物联网图像火灾报警实现信息化，拥有时刻警惕的双眼和不知疲倦的大脑，业主提供了消防的立体远程监控；实现了“眼睛和大脑”的完美统一，能够对火情做出准确的判断，火焰识别报警响应速度快，报警时间≤15秒，探测器前端处理，无需主机，直接输出报警信号，火灾视频信号，探测准确率高达到90%；接入云平台能够直接通知到微信小程序。

1.4  关键性能指标

火焰、烟雾识别率92%

1.5  主要创新点

（1）、物联网技术、计算机视觉和神经网络相结合

（2）、通过云平台（微信小程序）对数据进行实时上传

**第二部分**  **系统组成及功能说明**

2   

2.1  整体介绍

​                               

2.2  各模块介绍

 

Hi3516DV300

 

Hi3861

**第三部分**  **完成情况及性能参数**

完成数据集制作，模型训练、量化，板端部署，3861使用WIFI连接互联网，通过MQTT协议与腾讯云服务器进行通信，及腾讯云服务器与微信小程序互联。

 

模型训练结果：Avg IOU 90%

 

板端部署模式识别结果

 

OLED屏幕显示结果

 

微信小程序界面

当火焰以及烟雾被识别的时候，3516对3861进行通信，Led灯进行闪烁，然后屏幕显示火WANING:Fire字样并且小程序端显示已经起火

**第四部分**  **总结**

3   

4   

4.1  可扩展之处

对于火灾的识别精度需要提升，以及与微信小程序通信中，识别的应该主动提交，让小程序进行弹窗显示，以及对于火焰识别后最好发出报警声音这些都是可扩展功能，由于时间问题还未实现。后期考虑移植YOLO V5算法，以提高识别精度。

4.2  心得体会

通过这次比赛我们学会了很多东西，对神经网络有了一定的了解，以及对通信协议这一块也有了一定的了解，最主要的是加强了我们的钻研精神，让我们学会自己钻进去学习，以及培养解决问题的能力。

**第五部分**  **参考文献**

《2022年嵌入式大赛海思赛道开发指导》

**第六部分**  **附录**

 

\#include <stdlib.h>

\#include <string.h>

\#include <stdio.h>

\#include <errno.h>

\#include <sys/prctl.h>

\#include "sample_comm_nnie.h"

\#include "sample_media_ai.h"

\#include "ai_infer_process.h"

\#include "yolov2_hand_detect.h"

\#include "vgs_img.h"

\#include "ive_img.h"

\#include "misc_util.h"

\#include "hisignalling.h"

\#include "audio_aac_adp.h"

\#include "posix_help.h"

\#include "base_interface.h"

\#include "osd_img.h"

 

 

\#ifdef __cplusplus

\#if __cplusplus

extern "C" {

\#endif

\#endif /* End of #ifdef __cplusplus */

 

\#define HAND_FRM_WIDTH   640

\#define HAND_FRM_HEIGHT  384

\#define DETECT_OBJ_MAX   32

\#define RET_NUM_MAX    4

\#define DRAW_RETC_THICK  8  // Draw the width of the line

\#define WIDTH_LIMIT    32

\#define HEIGHT_LIMIT    32

\#define IMAGE_WIDTH    224 // The resolution of the model IMAGE sent to the classification is 224*224

\#define IMAGE_HEIGHT    224

\#define MODEL_FILE_GESTURE  "/userdata/models/hand_classify/hand_gesture.wk" // darknet framework wk model

\#define MODEL_FILE_HAND  "/userdata/models/hand_classify/hand_detect.wk" // darknet framework wk model

\#define PIRIOD_NUM_MAX   49 // Logs are printed when the number of targets is detected

\#define DETECT_OBJ_MAX   32 // detect max obj

 

\#define MULTIPLE_OF_EXPANSION 100  // Multiple of expansion

\#define UNKOWN_WASTE     20  // Unkown Waste

\#define BUFFER_SIZE      16  // buffer size

\#define MIN_OF_BOX      16  // min of box

\#define MAX_OF_BOX      240  // max of box

 

 

static int biggestBoxIndex;

static IVE_IMAGE_S img;

static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};

static RectBox boxs[DETECT_OBJ_MAX] = {0};

static RectBox objBoxs[DETECT_OBJ_MAX] = {0};

static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};

static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the classification network

static RecogNumInfo numInfo[RET_NUM_MAX] = {0};

static IVE_IMAGE_S imgIn;

static IVE_IMAGE_S imgDst;

static VIDEO_FRAME_INFO_S frmIn;

static VIDEO_FRAME_INFO_S frmDst;

int uartFd = 0;

static HI_BOOL g_bAudioProcessStopSignal = HI_FALSE;

static pthread_t g_audioProcessThread = 0;

static OsdSet* g_osdsTrash = NULL;

static HI_S32 g_osd0Trash = -1;

static SkPair g_stmChn = {

  .in = -1,

  .out = -1

};

static HI_BOOL Panduan = HI_FALSE;

static HI_VOID* GetAudioFileName(HI_VOID* arg)

{

  

  SAMPLE_PRT("----------------------panduan");  

  AudioTest(0, -1);

  return NULL;

}

 

HI_S32 Yolo2HandDetectResnetClassifyLoad(uintptr_t* model)

{

  HI_CHAR audioThreadName[BUFFER_SIZE] = {0};

  SAMPLE_SVP_NNIE_CFG_S *self = NULL;

  HI_S32 ret;

  HandDetectInit();

  ret = Yolo2Create(&self, MODEL_FILE_HAND);

  *model = ret < 0 ? 0 : (uintptr_t)self;

  SAMPLE_PRT("Yolo2FdLoad ret:%d\n", ret);

  /* uart open init */

  uartFd = UartOpenInit();

  if (uartFd < 0) {

​    printf("uart1 open failed\r\n");

  } else {

​    printf("uart1 open successed\r\n");

  }

  

  

  

  

  return ret;

}

 

HI_S32 Yolo2HandDetectResnetClassifyUnload(uintptr_t model)

{

  // CnnDestroy((SAMPLE_SVP_NNIE_CFG_S*)model);

  Yolo2Destory((SAMPLE_SVP_NNIE_CFG_S*)model);

  HandDetectExit(); // Uninitialize the hand detection model

  SAMPLE_PRT("Unload hand detect claasify model success\n");

  return 0;

}

 

/* Get the maximum hand */

static HI_S32 GetBiggestHandIndex(RectBox boxs[], int detectNum)

{

  HI_S32 handIndex = 0;

  HI_S32 biggestBoxIndex = handIndex;

  HI_S32 biggestBoxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;

  HI_S32 biggestBoxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;

  HI_S32 biggestBoxArea = biggestBoxWidth * biggestBoxHeight;

 

  for (handIndex = 1; handIndex < detectNum; handIndex++) {

​    HI_S32 boxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;

​    HI_S32 boxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;

​    HI_S32 boxArea = boxWidth * boxHeight;

​    if (biggestBoxArea < boxArea) {

​      biggestBoxArea = boxArea;

​      biggestBoxIndex = handIndex;

​    }

​    biggestBoxWidth = boxs[biggestBoxIndex].xmax - boxs[biggestBoxIndex].xmin + 1;

​    biggestBoxHeight = boxs[biggestBoxIndex].ymax - boxs[biggestBoxIndex].ymin + 1;

  }

 

  if ((biggestBoxWidth == 1) || (biggestBoxHeight == 1) || (detectNum == 0)) {

​    biggestBoxIndex = -1;

  }

 

  return biggestBoxIndex;

}

 

 

static HI_S32 HandDetect(uintptr_t model, IVE_IMAGE_S *srcYuv, DetectObjInfo boxs[])

{

  SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;

  int objNum;

  int ret = Yolo2CalImg(self, srcYuv, boxs, DETECT_OBJ_MAX, &objNum);

  if (ret < 0) {

​    SAMPLE_PRT("Hand detect Yolo2CalImg FAIL, for cal FAIL, ret:%d\n", ret);

​    return ret;

  }

 

  return objNum;

}

 

 

HI_S32 Yolo2HandDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)

{

  SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;

  HI_S32 resLen = 0;

  RecogNumInfo resBuf = {0};

  int objNum;

  int ret;

  int num = 0;

 

  ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);

  SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "hand detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

 

  objNum = HandDetect(model,&img, objs); // Send IMG to the detection net for reasoning

  for (int i = 0; i < objNum; i++) {

​    cnnBoxs[i] = objs[i].box;

​    RectBox *box = &objs[i].box;

​    RectBoxTran(box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,

​      dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);

​    SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin, box->xmax, box->ymax);

​    boxs[i] = *box;

  }

  biggestBoxIndex = GetBiggestHandIndex(boxs, objNum);

  SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum)；

​    objBoxs[0] = boxs[biggestBoxIndex];

​    MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_RED, DRAW_RETC_THICK); // Target hand objnum is equal to 1

​    for (int j = 0; (j < objNum) && (objNum > 1); j++) {

​      if (j != biggestBoxIndex) {

​        remainingBoxs[num++] = boxs[j];

​        // others hand objnum is equal to objnum -1

​        MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);

​       }

​    }

​    UartSendRead(uartFd, FistGesture); 

  }

  return ret;

}

 

\#ifdef __cplusplus

\#if __cplusplus

}

\#endif

\#endif /* End of #ifdef __cplusplus */