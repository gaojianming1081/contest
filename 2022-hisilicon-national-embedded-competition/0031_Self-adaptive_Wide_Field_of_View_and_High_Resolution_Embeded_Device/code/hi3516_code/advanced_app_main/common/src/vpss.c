#include "vpss.h"

#include "mpi_vpss.h"

HI_S32 VPSS_Init(VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret = 0;

    VPSS_GRP_ATTR_S stVpssGrpAttr =
        {
            .u32MaxW = 2592,
            .u32MaxH = 1536,
            .enPixelFormat = PIXEL_FORMAT_YVU_SEMIPLANAR_420,
            .enDynamicRange = DYNAMIC_RANGE_SDR8,
            .stFrameRate =
                {
                    .s32SrcFrameRate = -1,
                    .s32DstFrameRate = -1
                },
            .bNrEn = HI_TRUE,
            .stNrAttr =
                {
                    .enNrType = VPSS_NR_TYPE_VIDEO,
                    .enCompressMode = COMPRESS_MODE_FRAME,
                    .enNrMotionMode = NR_MOTION_MODE_NORMAL,
                },
        };

    VPSS_CHN_ATTR_S astVpssChnAttr =
        {
            .enChnMode = VPSS_CHN_MODE_USER,
            .u32Width = 1920,
            .u32Height = 1080,
            .enVideoFormat = VIDEO_FORMAT_LINEAR,
            .enPixelFormat = PIXEL_FORMAT_YVU_SEMIPLANAR_420,
            .enDynamicRange = DYNAMIC_RANGE_SDR8,
            .enCompressMode = COMPRESS_MODE_NONE,
            .stFrameRate =
                {
                    .s32SrcFrameRate = 30,
                    .s32DstFrameRate = 30,
                },
            .bMirror = HI_FALSE,
            .bFlip = HI_FALSE,
            .u32Depth = 2,
            .stAspectRatio =
                {
                    .enMode = ASPECT_RATIO_NONE,
                },
        };

    s32Ret = HI_MPI_VPSS_CreateGrp(VpssGrp, &stVpssGrpAttr);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VPSS_CreateGrp: %x\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VPSS_SetChnAttr(VpssGrp, VpssChn, &astVpssChnAttr);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VPSS_SetChnAttr: %x\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VPSS_EnableChn(VpssGrp, VpssChn);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VPSS_EnableChn: %x\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VPSS_StartGrp(VpssGrp);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VPSS_StartGrp: %x\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_VOID VPSS_DeInit(VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret = 0;

    s32Ret = HI_MPI_VPSS_DisableChn(VpssGrp, VpssChn);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VPSS_DisableChn: %x\n", s32Ret);
    }

    s32Ret = HI_MPI_VPSS_StopGrp(VpssGrp);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VPSS_StopGrp: %x\n", s32Ret);
    }

    s32Ret = HI_MPI_VPSS_DestroyGrp(VpssGrp);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VPSS_DestroyGrp: %x\n", s32Ret);
    }
}