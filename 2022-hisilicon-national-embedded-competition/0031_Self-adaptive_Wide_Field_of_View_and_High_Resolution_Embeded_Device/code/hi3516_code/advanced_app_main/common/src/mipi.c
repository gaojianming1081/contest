#include "mipi.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "hi_mipi_tx.h"

HI_S32 SNS_MIPI_Init(combo_dev_t MipiDev)
{
    HI_S32 s32Ret = 0;

    HI_S32 fd = open("/dev/hi_mipi", O_RDWR);
    if (fd < 0)
    {
        printf("[FAIL] open: \"/dev/hi_mipi\"\n");
        return HI_FAILURE;
    }

    lane_divide_mode_t enHsMode = LANE_DIVIDE_MODE_0;
    s32Ret = ioctl(fd, HI_MIPI_SET_HS_MODE, &enHsMode);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_SET_HS_MODE\n");
        close(fd);
        return HI_FAILURE;
    }

    combo_dev_t devno = MipiDev;

    s32Ret = ioctl(fd, HI_MIPI_ENABLE_MIPI_CLOCK, &devno);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_ENABLE_MIPI_CLOCK\n");
        close(fd);
        return HI_FAILURE;
    }

    s32Ret = ioctl(fd, HI_MIPI_RESET_MIPI, &devno);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_RESET_MIPI\n");
        close(fd);
        return HI_FAILURE;
    }

    for (sns_clk_source_t SnsClkDev = 0; SnsClkDev < SNS_MAX_CLK_SOURCE_NUM; SnsClkDev++)
    {
        s32Ret = ioctl(fd, HI_MIPI_ENABLE_SENSOR_CLOCK, &SnsClkDev);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[FAIL] ioctl: HI_MIPI_ENABLE_SENSOR_CLOCK\n");
            close(fd);
            return HI_FAILURE;
        }
    }

    for (sns_rst_source_t SnsRstDev = 0; SnsRstDev < SNS_MAX_RST_SOURCE_NUM; SnsRstDev++)
    {
        s32Ret = ioctl(fd, HI_MIPI_RESET_SENSOR, &SnsRstDev);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[FAIL] ioctl: HI_MIPI_RESET_SENSOR\n");
            close(fd);
            return HI_FAILURE;
        }
    }

    usleep(10000);

    combo_dev_attr_t stcomboDevAttr =
        {
            .devno = MipiDev,
            .input_mode = INPUT_MODE_MIPI,
            .data_rate = MIPI_DATA_RATE_X1,
            .img_rect =
                {
                    .x = 0,
                    .y = 204,
                    .width = 2592,
                    .height = 1536,
                },
            .mipi_attr =
                {
                    .input_data_type = DATA_TYPE_RAW_12BIT,
                    .wdr_mode = HI_MIPI_WDR_MODE_NONE,
                    .lane_id = {0, 1, 2, 3},
                },
        };

    s32Ret = ioctl(fd, HI_MIPI_SET_DEV_ATTR, &stcomboDevAttr);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_SET_DEV_ATTR\n");
        close(fd);
        return HI_FAILURE;
    }

    devno = MipiDev;
    s32Ret = ioctl(fd, HI_MIPI_UNRESET_MIPI, &devno);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_UNRESET_MIPI\n");
        close(fd);
        return HI_FAILURE;
    }

    for (sns_rst_source_t SnsRstDev = 0; SnsRstDev < SNS_MAX_RST_SOURCE_NUM; SnsRstDev++)
    {
        s32Ret = ioctl(fd, HI_MIPI_UNRESET_SENSOR, &SnsRstDev);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[FAIL] ioctl: HI_MIPI_UNRESET_SENSOR\n");
            close(fd);
            return HI_FAILURE;
        }
    }

    close(fd);
    return HI_SUCCESS;
}

HI_VOID SNS_MIPI_DeInit(combo_dev_t MipiDev)
{
    HI_S32 s32Ret;

    HI_S32 fd = open("/dev/hi_mipi", O_RDWR);
    if (fd < 0)
    {
        printf("[FAIL] open: \"/dev/hi_mipi\"\n");
        return;
    }

    for (sns_rst_source_t SnsRstDev = 0; SnsRstDev < SNS_MAX_RST_SOURCE_NUM; SnsRstDev++)
    {
        s32Ret = ioctl(fd, HI_MIPI_RESET_SENSOR, &SnsRstDev);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[FAIL] ioctl: HI_MIPI_RESET_SENSOR\n");
        }
    }

    for (sns_clk_source_t SnsClkDev = 0; SnsClkDev < SNS_MAX_CLK_SOURCE_NUM; SnsClkDev++)
    {
        s32Ret = ioctl(fd, HI_MIPI_DISABLE_SENSOR_CLOCK, &SnsClkDev);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[FAIL] ioctl: HI_MIPI_DISABLE_SENSOR_CLOCK\n");
        }
    }

    combo_dev_t devno = MipiDev;

    s32Ret = ioctl(fd, HI_MIPI_RESET_MIPI, &devno);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_RESET_MIPI\n");
    }

    s32Ret = ioctl(fd, HI_MIPI_DISABLE_MIPI_CLOCK, &devno);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_DISABLE_MIPI_CLOCK\n");
    }

    close(fd);
}

HI_S32 LCD_MIPI_TX_Screen_Init(HI_S32 MipiFd)
{
    HI_S32 s32Ret = 0;

    HI_U8 m_buf[50];
    cmd_info_t cmd_info;

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xFF;
    m_buf[1] = 0x77;
    m_buf[2] = 0x01;
    m_buf[3] = 0x00;
    m_buf[4] = 0x00;
    m_buf[5] = 0x13;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 6;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x08ef;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xFF;
    m_buf[1] = 0x77;
    m_buf[2] = 0x01;
    m_buf[3] = 0x00;
    m_buf[4] = 0x00;
    m_buf[5] = 0x10;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 6;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xC0;
    m_buf[1] = 0x63;
    m_buf[2] = 0x00;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 3;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xC1;
    m_buf[1] = 0x10;
    m_buf[2] = 0x02;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 3;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xC2;
    m_buf[1] = 0x01;
    m_buf[2] = 0x08;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 3;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x18CC;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xB0;
    m_buf[1] = 0x40;
    m_buf[2] = 0xC9;
    m_buf[3] = 0x8F;
    m_buf[4] = 0x0D;
    m_buf[5] = 0x11;
    m_buf[6] = 0x07;
    m_buf[7] = 0x02;
    m_buf[8] = 0x09;
    m_buf[9] = 0x09;
    m_buf[10] = 0x1F;
    m_buf[11] = 0x04;
    m_buf[12] = 0x50;
    m_buf[13] = 0x0F;
    m_buf[14] = 0xE4;
    m_buf[15] = 0x29;
    m_buf[16] = 0xDF;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 17;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xB1;
    m_buf[1] = 0x40;
    m_buf[2] = 0xCB;
    m_buf[3] = 0xD3;
    m_buf[4] = 0x11;
    m_buf[5] = 0x8F;
    m_buf[6] = 0x04;
    m_buf[7] = 0x00;
    m_buf[8] = 0x08;
    m_buf[9] = 0x07;
    m_buf[10] = 0x1C;
    m_buf[11] = 0x06;
    m_buf[12] = 0x53;
    m_buf[13] = 0x12;
    m_buf[14] = 0x63;
    m_buf[15] = 0xEB;
    m_buf[16] = 0xDF;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 17;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xFF;
    m_buf[1] = 0x77;
    m_buf[2] = 0x01;
    m_buf[3] = 0x00;
    m_buf[4] = 0x00;
    m_buf[5] = 0x11;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 6;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x65b0;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x34b1;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x87b2;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x80b3;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x49b5;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x85b7;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x20b8;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x10b9;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x78c1;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x78c2;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x88d0;
    cmd_info.data_type = 0x23;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);
    usleep(100000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE0;
    m_buf[1] = 0x00;
    m_buf[2] = 0x19;
    m_buf[3] = 0x02;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 4;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE1;
    m_buf[1] = 0x05;
    m_buf[2] = 0xA0;
    m_buf[3] = 0x07;
    m_buf[4] = 0xA0;
    m_buf[5] = 0x04;
    m_buf[6] = 0xA0;
    m_buf[7] = 0x06;
    m_buf[8] = 0xA0;
    m_buf[9] = 0x00;
    m_buf[10] = 0x44;
    m_buf[11] = 0x44;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 12;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE2;
    m_buf[1] = 0x00;
    m_buf[2] = 0x00;
    m_buf[3] = 0x00;
    m_buf[4] = 0x00;
    m_buf[5] = 0x00;
    m_buf[6] = 0x00;
    m_buf[7] = 0x00;
    m_buf[8] = 0x00;
    m_buf[9] = 0x00;
    m_buf[10] = 0x00;
    m_buf[11] = 0x00;
    m_buf[12] = 0x00;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 13;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE3;
    m_buf[1] = 0x00;
    m_buf[2] = 0x00;
    m_buf[3] = 0x33;
    m_buf[4] = 0x33;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 5;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE4;
    m_buf[1] = 0x44;
    m_buf[2] = 0x44;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 3;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE5;
    m_buf[1] = 0x0D;
    m_buf[2] = 0x31;
    m_buf[3] = 0xC8;
    m_buf[4] = 0xAF;
    m_buf[5] = 0x0F;
    m_buf[6] = 0x33;
    m_buf[7] = 0xC8;
    m_buf[8] = 0xAF;
    m_buf[9] = 0x09;
    m_buf[10] = 0x2D;
    m_buf[11] = 0xC8;
    m_buf[12] = 0xAF;
    m_buf[13] = 0x0B;
    m_buf[14] = 0x2F;
    m_buf[15] = 0xC8;
    m_buf[16] = 0xAF;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 17;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE6;
    m_buf[1] = 0x00;
    m_buf[2] = 0x00;
    m_buf[3] = 0x33;
    m_buf[4] = 0x33;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 5;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE7;
    m_buf[1] = 0x44;
    m_buf[2] = 0x44;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 3;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE8;
    m_buf[1] = 0x0C;
    m_buf[2] = 0x30;
    m_buf[3] = 0xC8;
    m_buf[4] = 0xAF;
    m_buf[5] = 0x0E;
    m_buf[6] = 0x32;
    m_buf[7] = 0xC8;
    m_buf[8] = 0xAF;
    m_buf[9] = 0x08;
    m_buf[10] = 0x2C;
    m_buf[11] = 0xC8;
    m_buf[12] = 0xAF;
    m_buf[13] = 0x0A;
    m_buf[14] = 0x2E;
    m_buf[15] = 0xC8;
    m_buf[16] = 0xAF;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 17;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xEB;
    m_buf[1] = 0x02;
    m_buf[2] = 0x00;
    m_buf[3] = 0xE4;
    m_buf[4] = 0xE4;
    m_buf[5] = 0x44;
    m_buf[6] = 0x00;
    m_buf[7] = 0x40;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 8;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xEC;
    m_buf[1] = 0x3C;
    m_buf[2] = 0x00;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 3;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xED;
    m_buf[1] = 0xAB;
    m_buf[2] = 0x89;
    m_buf[3] = 0x76;
    m_buf[4] = 0x54;
    m_buf[5] = 0x01;
    m_buf[6] = 0xFF;
    m_buf[7] = 0xFF;
    m_buf[8] = 0xFF;
    m_buf[9] = 0xFF;
    m_buf[10] = 0xFF;
    m_buf[11] = 0xFF;
    m_buf[12] = 0x10;
    m_buf[13] = 0x45;
    m_buf[14] = 0x67;
    m_buf[15] = 0x98;
    m_buf[16] = 0xBA;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 17;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xEF;
    m_buf[1] = 0x08;
    m_buf[2] = 0x08;
    m_buf[3] = 0x08;
    m_buf[4] = 0x45;
    m_buf[5] = 0x3F;
    m_buf[6] = 0x54;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 7;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xFF;
    m_buf[1] = 0x77;
    m_buf[2] = 0x01;
    m_buf[3] = 0x00;
    m_buf[4] = 0x00;
    m_buf[5] = 0x00;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 6;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xFF;
    m_buf[1] = 0x77;
    m_buf[2] = 0x01;
    m_buf[3] = 0x00;
    m_buf[4] = 0x00;
    m_buf[5] = 0x13;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 6;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE8;
    m_buf[1] = 0x00;
    m_buf[2] = 0x0E;
    m_buf[3] = 0x11;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 4;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);
    usleep(120000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE8;
    m_buf[1] = 0x00;
    m_buf[2] = 0x0C;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 3;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);
    usleep(10000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xE8;
    m_buf[1] = 0x00;
    m_buf[2] = 0x00;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 3;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);
    usleep(10000);

    memset_s(m_buf, sizeof(m_buf), 0, 50);
    m_buf[0] = 0xFF;
    m_buf[1] = 0x77;
    m_buf[2] = 0x01;
    m_buf[3] = 0x00;
    m_buf[4] = 0x00;
    m_buf[5] = 0x00;
    cmd_info.devno = 0;
    cmd_info.cmd_size = 6;
    cmd_info.data_type = 0x29;
    cmd_info.cmd = m_buf;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);
    usleep(10000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x11;
    cmd_info.data_type = 0x05;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);
    usleep(150000);

    cmd_info.devno = 0;
    cmd_info.cmd_size = 0x29;
    cmd_info.data_type = 0x05;
    cmd_info.cmd = NULL;
    s32Ret = ioctl(MipiFd, HI_MIPI_TX_SET_CMD, &cmd_info);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_CMD\n");
        close(MipiFd);
        return HI_FAILURE;
    }
    usleep(1000);
    usleep(50000);

    s32Ret = ioctl(MipiFd, HI_MIPI_TX_ENABLE);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_ENABLE\n");
        close(MipiFd);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 LCD_MIPI_Init()
{
    HI_S32 s32Ret = 0;

    system("insmod /ko/hi_mipi_tx.ko");

    HI_S32 fd = open("/dev/hi_mipi_tx", O_RDWR);
    if (fd < 0)
    {
        printf("[FAIL] open: \"/dev/hi_mipi_tx\"\n");
        return HI_FAILURE;
    }

    combo_dev_cfg_t stMipiTxConfig =
        {
            .devno = 0,
            .lane_id = {0, 1, -1, -1},
            .output_mode = OUTPUT_MODE_DSI_VIDEO,
            .video_mode = BURST_MODE,
            .output_format = OUT_FORMAT_RGB_24_BIT,
            .sync_info =
                {
                    .vid_pkt_size = 480,
                    .vid_hsa_pixels = 10,
                    .vid_hbp_pixels = 50,
                    .vid_hline_pixels = 590,
                    .vid_vsa_lines = 4,
                    .vid_vbp_lines = 20,
                    .vid_vfp_lines = 20,
                    .vid_active_lines = 800,
                    .edpi_cmd_size = 0},
            .phy_data_rate = 359,
            .pixel_clk = 29878,
        };

    s32Ret = ioctl(fd, HI_MIPI_TX_SET_DEV_CFG, &stMipiTxConfig);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_SET_DEV_CFG\n");
        close(fd);
        return HI_FAILURE;
    }

    usleep(10000);
    system("echo 5 > /sys/class/gpio/unexport");
    system("echo 5 > /sys/class/gpio/export");
    system("echo out > /sys/class/gpio/gpio5/direction");
    system("echo 1 > /sys/class/gpio/gpio5/value");
    usleep(200000);
    system("echo 0 > /sys/class/gpio/gpio5/value");
    usleep(200000);
    system("echo 1 > /sys/class/gpio/gpio5/value");
    usleep(20000);

    s32Ret = LCD_MIPI_TX_Screen_Init(fd);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] LCD_MIPI_TX_Screen_Init\n");
        close(fd);
        LCD_MIPI_DeInit();
        return HI_FAILURE;
    }

    close(fd);
    return HI_SUCCESS;
}

HI_VOID LCD_MIPI_DeInit()
{
    HI_S32 s32Ret = 0;

    HI_S32 fd = open("/dev/hi_mipi_tx", O_RDWR);
    if (fd < 0)
    {
        printf("[FAIL] open: \"/dev/hi_mipi_tx\"\n");
        return;
    }

    s32Ret = ioctl(fd, HI_MIPI_TX_DISABLE);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] ioctl: HI_MIPI_TX_DISABLE\n");
    }

    close(fd);

    usleep(10000);
    system("echo 5 > /sys/class/gpio/unexport");
    system("echo 5 > /sys/class/gpio/export");
    system("echo out > /sys/class/gpio/gpio5/direction");
    system("echo 0 > /sys/class/gpio/gpio5/value");
    usleep(10000);
    system("echo 5 > /sys/class/gpio/unexport");
}
