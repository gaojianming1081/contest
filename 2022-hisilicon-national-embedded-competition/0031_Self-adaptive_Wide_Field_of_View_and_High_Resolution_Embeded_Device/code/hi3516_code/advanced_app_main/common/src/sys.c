#include "sys.h"

#include <stdio.h>

#include "sdk.h"

#include "hi_defines.h"
#include "hi_buffer.h"
#include "mpi_sys.h"
#include "mpi_vb.h"

HI_S32 SYS_Init()
{
    HI_S32 s32Ret = 0;

    s32Ret = sdk_init();
    if (SDK_INIT_SUCC != s32Ret)
    {
        printf("[FAIL] HI_MPI_VB_SetConfig: %d\n", s32Ret);
        return HI_FAILURE;
    }

    VB_CONFIG_S stVbConfig = {0};
    stVbConfig.u32MaxPoolCnt = 3;
    stVbConfig.astCommPool[0].u64BlkSize = COMMON_GetPicBufferSize(2592, 1536, PIXEL_FORMAT_YVU_SEMIPLANAR_420, DATA_BITWIDTH_8, COMPRESS_MODE_NONE, DEFAULT_ALIGN);
    stVbConfig.astCommPool[0].u32BlkCnt = 10;
    stVbConfig.astCommPool[1].u64BlkSize = VI_GetRawBufferSize(2592, 1536, PIXEL_FORMAT_RGB_BAYER_16BPP, COMPRESS_MODE_NONE, DEFAULT_ALIGN);
    stVbConfig.astCommPool[1].u32BlkCnt = 10;
    stVbConfig.astCommPool[2].u64BlkSize = COMMON_GetPicBufferSize(1280, 720, PIXEL_FORMAT_YVU_SEMIPLANAR_420, DATA_BITWIDTH_8, COMPRESS_MODE_NONE, DEFAULT_ALIGN);
    stVbConfig.astCommPool[2].u32BlkCnt = 10;

    s32Ret = HI_MPI_VB_SetConfig(&stVbConfig);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VB_SetConfig: %x\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VB_Init();
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VB_Init: %x\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_SYS_Init();
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_SYS_Init: %x\n", s32Ret);
        return HI_FAILURE;
    }

    VI_VPSS_MODE_S stVIVPSSMode;
    s32Ret = HI_MPI_SYS_GetVIVPSSMode(&stVIVPSSMode);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_SYS_GetVIVPSSMode: %x\n", s32Ret);
        return HI_FAILURE;
    }

    for (int i = 0 ; i < VI_MAX_PIPE_NUM; i++)
    {
        stVIVPSSMode.aenMode[i] = VI_OFFLINE_VPSS_OFFLINE;
    }
    stVIVPSSMode.aenMode[0] = VI_ONLINE_VPSS_OFFLINE;
    
    s32Ret = HI_MPI_SYS_SetVIVPSSMode(&stVIVPSSMode);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_SYS_SetVIVPSSMode: %x\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_VOID SYS_DeInit()
{
    HI_S32 s32Ret = 0;

    s32Ret = HI_MPI_SYS_Exit();
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_SYS_Exit: %x\n", s32Ret);
    }

    s32Ret = HI_MPI_VB_Exit();
    if (HI_SUCCESS != s32Ret)
    {
        printf("[FAIL] HI_MPI_VB_Exit: %x\n", s32Ret);
    }

    sdk_exit();
}