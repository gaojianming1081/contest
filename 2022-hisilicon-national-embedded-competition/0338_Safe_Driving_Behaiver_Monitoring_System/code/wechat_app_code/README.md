# 微信小程序开发代码

```
├─components
│  └─calendar
├─ec-canvas
├─miniprogram_npm
│  └─@vant
│      └─weapp
│          ├─action-sheet
│          ├─area
│          ├─button
│          ├─calendar
│          │  └─components
│          │      ├─header
│          │      └─month
│          ├─card
│          ├─cell
│          ├─cell-group
│          ├─checkbox
│          ├─checkbox-group
│          ├─circle
│          ├─col
│          ├─collapse
│          ├─collapse-item
│          ├─common
│          │  └─style
│          │      └─mixins
│          ├─config-provider
│          ├─count-down
│          ├─datetime-picker
│          ├─definitions
│          ├─dialog
│          ├─divider
│          ├─dropdown-item
│          ├─dropdown-menu
│          ├─empty
│          ├─field
│          ├─goods-action
│          ├─goods-action-button
│          ├─goods-action-icon
│          ├─grid
│          ├─grid-item
│          ├─icon
│          ├─image
│          ├─index-anchor
│          ├─index-bar
│          ├─info
│          ├─loading
│          ├─mixins
│          ├─nav-bar
│          ├─notice-bar
│          ├─notify
│          ├─overlay
│          ├─panel
│          ├─picker
│          ├─picker-column
│          ├─popup
│          ├─progress
│          ├─radio
│          ├─radio-group
│          ├─rate
│          ├─row
│          ├─search
│          ├─share-sheet
│          ├─sidebar
│          ├─sidebar-item
│          ├─skeleton
│          ├─slider
│          ├─stepper
│          ├─steps
│          ├─sticky
│          ├─submit-bar
│          ├─swipe-cell
│          ├─switch
│          ├─tab
│          ├─tabbar
│          ├─tabbar-item
│          ├─tabs
│          ├─tag
│          ├─toast
│          ├─transition
│          ├─tree-select
│          ├─uploader
│          └─wxs
├─pages
│  ├─index
│  └─line
└─utils
```

![image-20220713180302310](https://picture.nongchatea.xyz/images/2022/07/13/image-20220713180302310.png)



![image-20220713180241687](https://picture.nongchatea.xyz/images/2022/07/13/image-20220713180241687.png)

![image-20220713180844120](https://picture.nongchatea.xyz/images/2022/07/13/image-20220713180844120.png)

![image-20220713180907727](https://picture.nongchatea.xyz/images/2022/07/13/image-20220713180907727.png)

![image-20220713180926730](https://picture.nongchatea.xyz/images/2022/07/13/image-20220713180926730.png)



