
#ifndef IOT_HMAC_H
#define IOT_HMAC_H
/**
 * This function used to generate the passwd for the mqtt to connect the HW IoT platform
 * @param content: This is the content for the hmac,
 * and usually it is the device passwd set or get from the Iot Platform
 * @param content_len: The length of the content
 * @param key: This is the key for the hmac, and usually it is the time used in the client_id:
 * the format is：yearmonthdatehour:like 1970010100
 * @param key_len: The length of the key
 * @param buf: used to storage the hmac code
 * @param buf_len:the buf length
 * @return:0 success while others failed
*/
int HmacGeneratePwd(const unsigned char *content, int contentLen, const unsigned char *key,
    int keyLen, unsigned char *buf);
#endif