
#include <stdint.h>
#include <cJSON.h>
#include <hi_mem.h>

static void *cJsonMalloc(size_t sz)
{
    return hi_malloc(0, sz);
}

static void cJsonFree(const char *p)
{
    hi_free(0, p);
}

void cJsonInit(void)
{
    cJSON_Hooks  hooks;
    hooks.malloc_fn = cJsonMalloc;
    hooks.free_fn = cJsonFree;
    cJSON_InitHooks(&hooks);

    return;
}