// ToolBarm_ToolBar.cpp : implementation file

#include "stdafx.h"
#include "ped_navi.h"
#include "ToolBarm_ToolBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CToolBarm_ToolBar

CToolBarm_ToolBar::CToolBarm_ToolBar()
{
}

CToolBarm_ToolBar::~CToolBarm_ToolBar()
{
}


BEGIN_MESSAGE_MAP(CToolBarm_ToolBar, CToolBarCtrl)
	//{{AFX_MSG_MAP(CToolBarm_ToolBar)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CToolBarm_ToolBar message handlers
