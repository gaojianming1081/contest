/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OLED_SSD1306_H
#define OLED_SSD1306_H

#include <stdint.h>

/**
 * @brief ssd1306 OLED Initialize.
 */
uint32_t OledInit(void);

/**
 * @brief Set cursor position
 * @param x the horizontal posistion of cursor
 * @param y the vertical position of cursor
 * @return Returns {@link WIFI_IOT_SUCCESS} if the operation is successful;
 * returns an error code defined in {@link wifiiot_errno.h} otherwise.
 */
void OledSetPosition(uint8_t x, uint8_t y);

/**
 * @brief clear screen
 * @param fillData 
 */
void OledFillScreen(uint8_t fillData);

enum Font {
    FONT6_X8 = 1,
    FONT8_X16
};
typedef enum Font Font;

/**
 * @brief 8*16 typeface
 * @param x: write positon start from x axis    0 <= x <= 7  
 * @param y: write positon start from y axis    0 <= y <= 127
 * @param ch: write data
 * @param font: selected font
 */
void OledShowChar(uint8_t x, uint8_t y, uint8_t ch, Font font);

/**
 * @brief 8*16 typeface
 * @param x: write positon start from x axis    0 <= x <= 7 
 * @param y: write positon start from y axis    0 <= y <= 127
 * @param str: write string
 * @param font: selected font   font=1 FONT6_X8  font=2 FONT8_X16 
 */
void OledShowString(uint8_t x, uint8_t y, const char* str, Font font);

/**
 * @brief 8*16 typeface
 * 
 * @param x: write positon start from x axis    0 <= x <= 7
 * @param y: write positon start from y axis    0 <= y <= 127
 * @param num: write num 
 * @param len: length of num 
 * @param font: selected font
 */
void OledShowNum(uint8_t x,uint8_t y,uint32_t num,uint8_t len,Font font);


/**
 * @brief 8*16 typeface
 * 
 * @param x: write positon start from x axis    0 <= x <= 7
 * @param y: write positon start from y axis    0 <= y <= 127
 * @param num: chinese number 
 */
void OledShowChinese(uint8_t x,uint8_t y,uint8_t no);
void OledDrawBMP(uint8_t x0,uint8_t y0,uint8_t x1,uint8_t y1,uint8_t BMP[]);
void OledShowDecimal(uint8_t x,uint8_t y,float num,uint8_t z_len,uint8_t f_len, Font font);
#endif // OLED_SSD1306_H
