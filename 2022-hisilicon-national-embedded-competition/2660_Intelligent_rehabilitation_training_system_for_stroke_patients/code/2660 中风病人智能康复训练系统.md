# **基于疼痛表情评估的中风病人反馈型智能康复训练系统**

何豪；徐泽爽；张琦

## **第一部分 设计概述**

### 1.1 设计目的

据调查发现，现在很多的康复中心对于中风病人的肢体康复运动仅仅依靠简单康复机械进行，而这类器械不能够实时地对病人疼痛进行反馈调整，此外每个病人的状态、恢复程度等具有个异性。针对这个情况，我们团队决定基于病人的疼痛表情对每个病人设置个性化的训练目标，将算法移植到海思的AI计算机视觉套件上，通过串口与康复机械进行联动，调整机械的运动情况，从而形成系统的反馈式智能闭环训练。

### 1.2 应用领域

**康复训练**：智能康复训练系统主要应用于康复机构、养老院等场所，针对肢体偏瘫的中风病人进行康复训练，为更多中风病人带来更轻松、有效、科学的康复训练服务，重获生命的意义和战胜病魔的信心。

**康复评估**：对训练时疼痛表情以及训练强度的评估可以了解到患者在心理、身体状态等多个方面的障碍程度，从而为康复目标和康复方案的制定与施行提供基础。

### 1.3 主要创新点

1.  在传统中风病人肢体康复基础上增加了人工智能手段，将病人疼痛程度识别与康复训练结合，更加人性化。
2.  形成了闭环反馈式的智能康复模式，训练、识别、训练动态调整、优化训练；也是一种训练中的评估和评估中的训练的结合。
3.  配合闭环训练要求，设计了智能康复的具体实施新模式，包括完整的训练流程、流程中的识别与进阶、重复训练的智能动态调整等。

### 1.4 关键性能指标

**检测网检测准确率**：目标检测中衡量识别精度的指标是mAP（mean average precision），人脸检测模型在测试集下的mAP为94.98%。

**板端检测与分类推理性能**：部署到板端后，采用先检测后分类的方式，人脸检测的准确率达到了97.3%以上，分类网分类准确率达到了98%以上。模型识别速率约在25fps左右。

**康复装置性能**：康复装置的拉伸长度可实现整个手臂的动态拉伸，拉伸长度可达60cm。升降桌的升降范围在50cm-120cm。

## **第二部分 系统硬件框架及软件设计**

### 2.1 整体介绍

智能康复训练系统的整体框架图1，主要包括硬件和软件两部分。硬件部分包含Taurus AI套件、Pegasus套件、康复装置，Taurus AI套件上的软件部分主要由Sensor采集模块、VI模块、VPSS模块、YOLO v2人脸检测模块、Resnet 18疼痛表情分类模块、音频播放模块、OSD叠加显示模块、串口通信模块构成，Pegasus套件上的软件部分主要由WIFI通信模块、系统控制模块、串口通信模块构成。另外，依托于物联网平台，将微信小程序接入Pegasus端系统控制模块。

![image-20220714132704044](C:\Users\MI\Desktop\第五届嵌入式大赛\报告\2660 中风病人智能康复训练系统.assets\image-20220714132704044.png)

图1 系统整体框架

### 2.2 硬件各模块介绍

Taurus开发套件可以通过搭积木的方式组成USB计算棒、AI Camera以及AI视频分析记录仪。该开发套件基于专用的Smart HD IP Camera芯片Hi3516DV300设计，该芯片集成了新一代 ISP、业界最新的 H.265视频压缩编码器、高性能NNIE引擎，算力可达到1.0TOPS。具有灵活的存储空间、丰富的DIY 扩展接口和丰富的通信接口。本系统在Taurus上采用Linux 操作系统，使AI 推理过程更加稳定和高效。

通过sensor摄像头获取外界视觉信息，从视频流中抓取单帧并转为YUV格式的图片，将图片送入NNIE进行推理，首先利用YOLO v2对人脸进行检测，将检测到的人脸图片经Crop和Resize之后，送入Resnet 18分类网进行疼痛表情的分类，最后将人脸画框以及分类结果输出到OSD显示屏上，并通过串口将推理结果发送给Pegasus开发板。

同时在Taurus 创建一个线程专门用来接收Pegasus 开发板发送的消息，判断消息内容之后进行语音播放提示。Taurus AI 套件如图2所示。

![06d498f4867d46afef43a9523172ba6](media/4412bdc0dafa5c64732fe1633c4bcf02.jpeg)

### 图2 Taurus套件

Pegasus WiFi-IoT套件集成2.4GHz WLAN SoC芯片，Hi3861V100是32bit高性能微处理器，支持IEEE 802.11b/g/n基带和RF（Radio Frequency）电路。支持 OpenHarmony 操作系统，并配套提供开放、易用 的开发和调试运行环境。

在本系统中，主要利用拓展版的GPIO9引脚控制继电器的通断从而控制康复装置的启动与暂停；利用UART1口实现与Taurus AI套件的串口通信，将AI推理后的结果进行分析；WIFI-IoT 模块主要是连接指定 WIFI 网络，腾讯云物联网的平台进行数据的发布和订阅。Pegasus开发板如图3所示。

![634f55a821e5deb635396f7d40f6b9a](media/27bc346521b1599e09e9b2e927e787b5.jpeg)

### 图3 Pegasus开发板

康复装置主要包括可移动式升降桌、可悬挂式滑轮以及卷扬装置。康复装置总体构建图如图4所示。

![](media/083fb55fabf10876367e7a3fd9ebe8ae.png)

图 4 康复装置总体连接图

1.  可移动式便携升降桌主要放置器件，同时考虑到用户身高的差异性，用户可方便的移动升降桌并自行调节升降桌高度。如图 5所示。

![4120425d48805c3011db93408aa82ee](media/8d596ce869378418af701bf97a4080fc.jpeg)

图 5 可移动式便携升降桌

1.  可悬挂式滑轮主要用于减小拉伸时的阻力，具备便携易安装的特点，且宽度可调节，用户可自行根据自身肩宽调节宽度。安装情况如图 6所示。

![e772175c82f9b779088f702c7309dab](media/386e55d173bd90a027f7893ece7681b2.jpeg)

图 6 可悬挂式滑轮

1.  卷扬装置主要用来收放拉绳，并且在一端可接不同的握力装置，方便用户抓握。装置内部主要有一个220V转36V的变压器，通过控制电路驱动交流电机从而带动绳子的拉伸。Pegsus通过GPIO口控制继电器来控制康复装置的通断。具体结构如图 7所示：

![](media/c983b39f47da3f774bf86c7453baa951.png)

图 7 卷扬装置外壳、卷扬装置内部构造、握手带

### 2.3 系统软件介绍

#### 2.3.1系统的整体程序流程

系统的控制模块如图 8所示，康复患者通过小程序进行登录，如果是新用户的话会对患者进行注册，并采用默认值进行训练，如果是老用户的话会载入上一次的训练数据，并基于上一次进行训练。同时在训练的过程中，Taurus AI 套件会对用户的表情进行实时的分析，将推理后的结果反馈给康复装置，对病人的训练强度进行一个动态的调整，继而形成一个闭环反馈式训练。在训练之后会将患者的训练数据上传云端存储并生成报告，实时跟踪病人的康复情况。

图 8 系统整体流程图

#### 2.3.2 Taurus AI套件程序流程

Taurus AI套件软件程序主要流程图 9如所示，在程序初始化之后，创建一个线程用于AI推理，在将sensor摄像头采集后的图片进行预处理之后，送到NNIE模块进行推理，首先是通过YOLO v2对人脸进行检测，如果检测到人脸并且置信度大于80%的话，对图像进行Crop和Resize以适配Resnet 18分类网，分类网会对检测后的图片进行分类，并将分类的结果与准确率通过串口发送给Pegasus，同时将画框与分类结果输出到OSD屏幕上面进行显示。另一个线程主要进行串口通信，用于监听Pegasus串口发送过来的消息，播放对应的音频。

图 9 Taurus AI套件软件程序流程图

#### 2.3.3 Pegasus端主要的程序流程

Pegasus端主要的程序流程图如图 10所示，在程序初始化之后开启一个线程专门与腾讯云物联网控制台进行数据交互，通过WIFI-IoT与公网建立连接之后，就可以监听腾讯云的回调消息，并时刻保持与腾讯云的通信。第二个线程用于串口通信，在初始化串口之后，Pegasus的UART 1口会一直监听Taurus发送的消息，当接收的消息符合规定的通信协议之后对数据进行处理。当收到启动消息is_start为1时，获取云端的数据并进行分析，同时启动线程3用于康复训练系统的控制，启动后会通过串口向Taurus发送消息并播放音频，第一个阶段是基础训练，第二个阶段是加强训练，通过串口发送的AI推理信息，对训练过程进行一个动态调整；第三个阶段是放松训练，在训练结束之后会将数据上传到云端，系统也会播放语音提示在小程序上面查看报告。

图 10 Pegasus端程序流程图

### 2.4 核心算法介绍

疼痛表情的分类采取先检测再分类的方式，首先通过YOLO v2进行人脸检测，将YOLO v2检测到的人脸送入Resnet 18进行疼痛表情的分类。

YOLO v2采用Darknet-19，其网络结构如下图 11所示，包括19个卷积层和5个max pooling层，主要采用3x3卷积和1x1卷积，这里1x1卷积可以压缩特征图通道数以降低模型计算量和参数，每个卷积层后使用BN层以加快模型收敛同时防止过拟合。最终采用Global Avg Pool做预测。

![YOLO v2网络详细结构分析与参数计算](media/e4cee7c4664bee8463431885b44a9dcf.png)

图 11 YOLOv2网络结构

Resnet 18网络是由一系列残差块组成的，残差块的结构如图 12。一个残差块可以表示为：

残差块分为直接映射和残差部分。是直接映射，反应在下图是左边的直线；是残差部分，即右侧曲线部分。

![](media/4ccd2f277b4b79e63d530723f2ba0c49.png)

图 12 Resnet残差块网络结构

Resnet 18的网络结构如图 13所示：

![](media/e89552bfe02b485c17648e9a587da24f.emf)

图 13 Resnet18 网络结构

## **第三部分 完成情况及实验性能**

### 3.1 模型的训练部署

模型的训练部署过程可分为数据集的采集获取、清洗，模型的训练、转换、部署几大步骤。我们采用了先检测再分类的方式，接下来分别介绍人脸检测、疼痛表情识别的训练部署过程。

#### 3.1.1人脸检测模型的训练部署

（1）数据集的获取

人脸检测数据集采用开源数据集WIDER FACE（http://shuoyang1213.me/WIDERFACE/index.html），此数据集包含32203人脸数据集及人脸坐标标签，数据集的部分图片如图 14，由于WIDER FACE数据集的标注格式与Darknet框架要求标注格式不同，团队利用Python脚本对开源数据集的标注格式进行了适配（附录代码1）。脚本运行完毕后，会生成如图 15所示的文件。

![](media/a43bff0a6e12741e43aa96c0ee2844c7.png)

图 14 WIDER FACE数据集图片

![](media/713e8084b6eda4bb44519b26e22e957e.png)

图 15 适配后的标注文件

（2）模型的训练

基于Darknet框架中的YOLO v2网络，参照海思提供的检测网训练文档进行了配置。训练过程如下图16所示。

![](media/0592e50eddd6dfcaed8c98ff1c811e7c.png)

图16 YOLO v2模型训练过程

（3）模型转换

借助darknet2caffe工具将pytorch模型转换为caffe模型，对prototxt文件进行了板端适配。再利用Ruyi Studio工具，将caffe model以及prototxt文件转换为wk模型。具体转换的配置如图17所示。

![](media/f387651fc6d9b9206f0e59ed6b0fdec2.png)

图17 YOLO v2网络模型转换配置

（4）模型的部署和测试

基于手势检测实验，将wk文件导入Taurus后进行部署，采用复杂场景下单人、多人人脸进行了模型验证，人脸检测正确率高到97.3%，在多人的情况下，会选择画框区域最大的作为输出结果，具体的检测效果图片如图 18所示。

![](media/056173dfd1993c3dd73b81dfca9b9b5b.png)

图 18 多人检测、单人检测、单人部分遮罩检测

#### 3.1.2疼痛表情分类模型的训练部署

（1）数据集的获取

团队利用PC端python脚本（附录代码2），到医院采集了一定量的数据集，在采集时，对图片明暗度进行了随机变化，以保证数据集的多场景性。受疫情影响，所采数据集的量还不足以支撑网络模型的训练，因此，在训练前，对数据集进行了增广，通过联系RAF-DB数据集作者授权获取了微表情分类的数据集并对数据集进行了人工清洗分类，最终得到6000张疼痛、不疼痛的数据集。通过人工数据清洗的部分数据集如图 19所示:

![](media/13cf4dcbbdcc6b92b711a58bb6b71d94.png)

图 19 人工清洗的normal 数据集

（2）模型的训练

利用海思提供的训练服务器，参照垃圾分类实验进行了训练配置，基于mmclassification框架中的Resnet 18进行分类模型的训练。具体训练情况如图 20。

![](media/c5d568175604e617445f179dbf85660a.png)

图 20 Resnet18 网络训练过程

（3）模型转换

利用pytorch2caffe工具将pytorch模型转换为caffe模型，并对prototxt文件进行了板端适配。再利用Ruyi Studio工具，将caffe model以及prototxt文件转换为wk模型。Ruyi工具的相关配置如图 21所示。

![](media/4de6a370c1b19adf144687f9c44be751.png)

图 21 Resnet18 网络模型转换配置

（4）模型的部署和测试

将转换后wk文件导入Taurus后进行部署，将人脸检测的结果送入到分类网络，疼痛表情分类识别率高到98%，具体的测试结果如图 22、图 23所示。

![](media/434b732dc2c54eb7c1cf5c4c2bc81c44.png)

图 22 队员1正常表情（左）、疼痛表情（右）![](media/da360440c53b3c556ed1d1261e451859.png)

图 23 队员2正常表情（左）、疼痛表情（右）

### 3.2 康复装置的搭建与测试

整体康复装置的搭建如图 24所示，通过控制GPIO9引脚的高低电平输出，从而控制康复装置的升降，经过测试，康复装置功能正常，可对患者手臂进行拉伸，具体实验情况如图 25所示。

![](media/b4896530183ebb37a241c5d46fd0654d.png)

图 24 系统整体侧视图、俯视图

**![015943ed625444fa36fef0a02ce11eb](media/654b324d40f21eb32ede7594d2030042.jpeg)**

图 25 康复装置拉伸效果

### 3.3 用户训练模式设计

通过大量的市场调研，我们结合中风病人的康复训练特性并参考了现有的康复方法，将训练模式分为了基础训练、加强训练、放松训练三个模式。基础训练主要激发康复患者的手部活力，比较轻松，基于增量训练的理念；在加强训练阶段强度稍大，并会根据病人的疼痛表情动态调整训练强度；为了使训练更加的人性化，在检测到病人的疼痛表情后，会播放激励语音鼓励患者，增加患者的信心；在放松训练阶段，主要考虑病人训练完毕后会有一定的疲劳感，对病人的手部进行轻度拉伸，巩固康复训练效果。具体的串口打印消息如图所示，实际的训练效果请参看视频。

![](media/c3dba610a990c64c61cdd77c3994e3ef.png)

图 26 腾讯云物联网平台功能测试结果

### 3.4 腾讯云物联网平台功能测试

当用户通过小程序选择是否为新用户后，进入到训练界面，点击开始训练，向云端发布“新老用户标志”以及“开始训练标志”，Pegasus端从云端订阅消息，对消息类型做出判断后，可以看到康复器械开始拉伸与下降，整个响应过程在1S之内。具体的功能测试结果如图所示，在腾讯云物联网控制台可以看到设备在线，通过串口助手可以看到腾讯云发布的相应“标志位”。训练结束后，点击查看报告按钮可以查看训练报告。

![](media/25a6cfec367f00ad38cd1bdf705f5d65.png)

图 27 腾讯云物联网平台功能测试结果

![](media/df74db6ca3e54f49ef4575bcc840f6aa.png)

图 28 小程序UI界面

### 3.5语音播报

语音播报主要是利用腾讯云的语音合成功能生成相应的语音，并转换为适配Taurus的音频格式，由于Taurus端音频播放接口中设置了3S的延时，限制了语音的播放时长，我们将播放的音频都控制在了3S左右，具体的测试结果请参看视频。

## **第四部分  总结**

### 4.1 可扩展之处

1.疼痛表情可以进行更多的分级量化。

2.康复训练装置的设计可以更加人性合理化，训练模式可以更加的多样化。同时可以增加一些别的康复训练，比如手指训练、脚部训练等。

3.小程序端可以听取中风患者的使用建议，优化操作和界面的开发。

### 4.2 心得体会

参加这次嵌入式比赛真的学到了很多实实在在的东西。

选题方面，有关中风病人反馈式训练产品在市场上是没有的，整个系统对于病人的康复、医疗产业的发展都是具有重要意义。小组开始与康复科医院进行合作，但是由于疫情原因，无法去医院实地采集康复病人的面部表情数据集，如果后面有时间，小组继续完善作品，优化模型的性能，将产品投入实际的使用当中。

虽然小组在六月初才拿到开发套件，开发的时间比较晚，同时小组最后只有两名同学参与整个项目的开发，但是在指导老师、海思各位老师的帮助以及鼓励下，不断的去学习，不断的去克服困难，最终将模型成功部署在板端，并取得了非常不错的效果，深刻的体会到Taurus AI 视觉处理套件在深度学习方面的强大。

总体来说，本次比赛不仅扩充了我们的知识储备，学习到了许多嵌入式开发 的知识，同时将所学知识付诸实践，得以应用；同时，还提高了我们诸如资料查 询、错误排查与处理、团队协作等软性实力，此次嵌入式大赛我们受益匪浅。

最后，感谢筹备此次大赛的工作人员，也祝愿此次大赛能圆满完成。

## **第五部分  参考文献**

[1] Pegasus 开发套件实验指导手册

[2] Taurus&Pagasus 基础开发套件调试知道手册

[3] Taurus 计算机视觉基础开发套件操作指导

[4] 基于 Darknet 框架的 Yolo2 检测网进行 AI 应用开发与部署的指导手册(以手部检测为例)

[5] 基于 Taurus 套件进行训练图片制作、模型训练、模型转换指导手册（Resnet18 网络进行垃圾分类为例）

[6] 基于 Taurus 套件进行训练图片制作、模型训练、模型转换指导手册（Resnet18 网络进行垃圾分类为例）

[7] [1512.03385v1] Deep Residual Learning for Image Recognition (arxiv.org)

[8] [1612.08242] YOLO9000: Better， Faster， Stronger (arxiv.org)

## **第六部分  附录**

### 代码1：WIDER FACE数据集适配Darknet

```
# -*- coding: utf-8 -*-

import os, re, json, traceback
from random import shuffle
import cv2
from collections import defaultdict

img_dir = "./WIDER_train"

img_count = 0
file_list = []
for root, dirs, files in os.walk(img_dir):
    for file in files:
        img_count += 1
        file_list.append(os.path.join(root, file))

print("Total image number: %d" % img_count)

# make directory
if not os.path.exists("./human_face_train_images"):
    os.system("mkdir ./human_face_train_images")
if not os.path.exists("./human_face_train_labels"):
    os.system("mkdir ./human_face_train_labels")
if not os.path.exists("./human_face_val_images"):
    os.system("mkdir ./human_face_val_images")
if not os.path.exists("./human_face_val_labels"):
    os.system("mkdir ./human_face_val_labels")

# shuffle the files
shuffle(file_list)

# get label data
with open("./wider_face_train_bbx_gt.txt", "r", encoding="utf-8") as h:
    content = [_.strip() for _ in h.readlines()]

# get labeled data into arrange form
line_index = []
for i, line in enumerate(content):
    if "." in line:
        line_index.append(i)

line_index.append(len(content) + 1)

segments = []
for j in range(len(line_index) - 1):
    segments.append(content[line_index[j]: line_index[j + 1]])

img_box_dict = defaultdict(list)
for segment in segments:
    for i in range(2, len(segment)):
        img_box_dict[segment[0].split('/')[-1]].append(segment[i].split()[:4])

# copy images to rights place and write correct labeled data into txt file
# train data
train_part = 0.8
for i in range(int(train_part * img_count)):
    print(i, file_list[i])
    file = file_list[i].split('/')[-1]
    os.system("cp %s ./human_face_train_images/%s" % (file_list[i], file))
    with open("./human_face_train.txt", "a", encoding="utf-8") as f:
        f.write("./human_face_train_images/%s" % file + "\n")

    img = cv2.imread(file_list[i], 0)
    height, width = img.shape
    with open("./human_face_train_labels/%s" % file.replace(".jpg", ".txt"), "w", encoding="utf-8") as f:
        for label in img_box_dict[file]:
            left, top, w, h = [int(_) for _ in label]
            # to avoid any of the coordinate becomes 0
            if left == 0:
                left = 0.1
            if top == 0:
                top = 0.1
            if w == 0:
                w = 0.1
            if h == 0:
                h = 0.1
            x_center = (left + w / 2) / width
            y_center = (top + h / 2) / height
            f.write("0 %s %s %s %s\n" % (x_center, y_center, w / width, h / height))

# val data
for i in range(int(train_part * img_count) + 1, img_count):
    print(i, file_list[i])
    file = file_list[i].split('/')[-1]
    os.system("cp %s ./human_face_val_images/%s" % (file_list[i], file))
    with open("./human_face_val.txt", "a", encoding="utf-8") as f:
        f.write("./human_face_val_images/%s" % file + "\n")

    img = cv2.imread(file_list[i], 0)
    height, width = img.shape
    with open("./human_face_val_labels/%s" % file.replace(".jpg", ".txt"), "w", encoding="utf-8") as f:
        for label in img_box_dict[file]:
            left, top, w, h = [int(_) for _ in label]
            # to avoid any of the coordinate becomes 0
            if left == 0:
                left = 0.1
            if top == 0:
                top = 0.1
            if w == 0:
                w = 0.1
            if h == 0:
                h = 0.1
            x_center = (left + w / 2) / width
            y_center = (top + h / 2) / height
            f.write("0 %s %s %s %s\n" % (x_center, y_center, w / width, h / height))
```

### 代码2：人脸疼痛表情采集

```
import cv2
import dlib
import os
import sys
import random

# 存储位置
output_dir = './data/face/3'
size = 224

if not os.path.exists(output_dir):
    os.makedirs(output_dir)


# 改变图片的亮度与对比度

def relight(img, light=1, bias=0):
    w = img.shape[1]
    h = img.shape[0]
    # image = []
    for i in range(0, w):
        for j in range(0, h):
            for c in range(3):
                tmp = int(img[j, i, c] * light + bias)
                if tmp > 255:
                    tmp = 255
                elif tmp < 0:
                    tmp = 0
                img[j, i, c] = tmp
    return img


# 使用dlib自带的frontal_face_detector作为我们的特征提取器
detector = dlib.get_frontal_face_detector()
# 打开摄像头 参数为输入流，可以为摄像头或视频文件
camera = cv2.VideoCapture(0)
# camera = cv2.VideoCapture('C:/Users/CUNGU/Videos/Captures/wang.mp4')

index = 1
while True:
    if (index <= 627):  # 存储20张人脸特征图像
        print('Being processed picture %s' % index)
        # 从摄像头读取照片
        success, img = camera.read()
        # 转为灰度图片
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # 使用detector进行人脸检测
        dets = detector(gray_img, 1)

        for i, d in enumerate(dets):
            x1 = d.top() if d.top() > 0 else 0
            y1 = d.bottom() if d.bottom() > 0 else 0
            x2 = d.left() if d.left() > 0 else 0
            y2 = d.right() if d.right() > 0 else 0

            face = img[x1:y1, x2:y2]
            # 调整图片的对比度与亮度， 对比度与亮度值都取随机数，这样能增加样本的多样性
            face = relight(face, random.uniform(0.5, 1.5), random.randint(-50, 50))

            face = cv2.resize(face, (size, size))

            cv2.imshow('image', face)

            cv2.imwrite(output_dir + '/' + str(index) + '.jpg', face)

            index += 1
        key = cv2.waitKey(30) & 0xff
        if key == 27:
            break
    else:
        print('Finished!')
        # 释放摄像头 release camera
        camera.release()
        # 删除建立的窗口 delete all the windows
        cv2.destroyAllWindows()
        break

```

