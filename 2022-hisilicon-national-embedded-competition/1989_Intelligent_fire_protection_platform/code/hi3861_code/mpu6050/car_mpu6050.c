

/**
 *
 *
 *
    MPU6050

    AD0 - 接地 = 设备地址 [ 0xD0 ]

    电源管理寄存器1 0x6B
    [7] = 1 复位 数据清零
    [6] = 1 休眠模式 = 0 正常模式
    [3] = 0 使能 温度传感器
    [210] 选择系统时钟
    000 内部8M RC晶振
    001 PLL，使用X轴陀螺作为参考
    010 PLL，使用Y轴陀螺作为参考
    011 PLL，使用Z轴陀螺作为参考
    100 PLL，使用外部32.768Khz作为参考
    101 PLL，使用外部19.2Mhz作为参考
    110 保留
    111 关闭时钟，保持时序产生电路复位状态

    陀螺仪配置寄存器 0x1B
    [43]
    00 250‘/s
    01 500’/s
    10 1000‘/s
    11 2000’/s

    2000‘ = 0001 1000 = 0x18

    加速度传感器配置寄存器 0x1C
    [43]
    00 2g
    01 4g
    10 8g
    11 16g

    2g = 0000 0000 = 0x00


    FIFO 使能寄存器 0x23
    00000000 禁用
    11111111 使能

    陀螺仪采样率分频寄存器 0x19

    设置MPU6050的陀螺仪采样频率

    采样频率 = 陀螺仪输出频率 / （1+SMPLRT—DIV） ???

    陀螺仪输出频率 1KHz/8KHz 低通滤波器设置

    输出频率 = 1K SMPLRT_DIV = 50

    1K /（1+50） = 19

    配置寄存器 0x1A

    低通滤波器 设置

    [210]

    DLPF_CFG[2:0]	加速度传感器Fs=1Khz		|	角速度传感器（陀螺仪）

				    带宽(Hz)	延迟（ms）	|   带宽(Hz)    延迟（ms）  Fs(Khz)

    000				260			0               256			0.98		8
    001				184			2.0 			188			1.9 		1
    010				94			3.0 			98			2.8 		1
    011				44			4.9 			42			4.8 		1
    100				21 			8.5 			20 			8.3 		1
    101				10 			13.8 			10 			13.4 		1
    110				5 			19.0 			5 			18.6 		1
    111				保留						保留                     8

    电源管理寄存器2 0x6C

    0x00

    加速度传感器 数据输出寄存器 0x3B ~ 0x40

    高字节在前

    0x3B 0x3C accel_xout
    0x3D 0x3E accel_yout
    0x3F 0x40 accel_zout

    温度传感器 数据输出 寄存器 0x41 ~ 0x42

    0x41 0x42 temp_out = regval

    Temperature = 36.53 + regval / 340

    陀螺仪数据输出寄存器 0x43 ~ 0x48

    0x43 0x44 gyro_xout
    0x45 0x46 gyro_yout
    0x47 0x48 gyro_zout

    short	2 字节	-32,768 到 32,767

    unsigned short	2 字节	0 到 65,535

    每个数据2个字节，值是16位有符号整数，正值正向，负值负向。

    加速度计算：

    若取值2g 重力加速度取值9.8 AX = 2 * 9.8 * ( dx / 32768 ) ; // 为什么取32768，因为-1到0也是一个跨度值

    角速度计算：

    正值顺时针方向，负值逆时针方向

    若取值2000度/秒 Gx = 2000 * ( dx / 32768 );

 *
 *
 *
 *
 **/


#include "car_mpu6050.h"


static hi_u8 mpu6050_data[14] = {0};   // 直采数据
static hi_u8 data_len = 14;            // 直采数组长度

static hi_s16 value[7] = {0};          // 字节数组转整形结果值



//  写入 1条 指令
static hi_void Mpu6050_I2c_Write(hi_u8 addr, hi_u8 data)
{
    hi_u8 data_send_buf[] = {addr, data};

    hi_u32 status = 0;  // 状态

    hi_i2c_data mpu6050_i2c_data = {0};

    mpu6050_i2c_data.send_buf = data_send_buf;
    mpu6050_i2c_data.send_len = 2;

    status = hi_i2c_write(HI_I2C_IDX_0, 0xD0, &mpu6050_i2c_data);   // 0xD0 MPU6050地址

    if(status != HI_ERR_SUCCESS)
    {
        printf("\n [Mpu6050_I2c_Write] Failed \n");
    }
}


//  读取 n条 数据
static hi_void Mpu6050_I2c_Read(hi_u8 addr, hi_u8* data_receive_buf, hi_u32 data_receive_len)
{
    hi_u8 data_send_buf[] = {addr};

    hi_u32 status = 0;  // 状态

    hi_i2c_data mpu6050_i2c_data = {0};

    mpu6050_i2c_data.send_buf = data_send_buf;
    mpu6050_i2c_data.send_len = 1;

    status = hi_i2c_write(HI_I2C_IDX_0, 0xD0, &mpu6050_i2c_data);   // 0xD0 MPU6050地址

    if(status != HI_ERR_SUCCESS)
    {
        printf("\n [Mpu6050_I2c_Read] Write Failed \n");
    }

    mpu6050_i2c_data.receive_buf = data_receive_buf;
    mpu6050_i2c_data.receive_len = data_receive_len;

    status = hi_i2c_read(HI_I2C_IDX_0, 0xD1, &mpu6050_i2c_data);

    if(status != HI_ERR_SUCCESS)
    {
        printf("\n [Mpu6050_I2c_Read] Read Failed \n");
    }
}


//  0x6B    电源管理寄存器1
//  0x6C    电源管理寄存器2

//  0x1B    陀螺仪配置寄存器
//  0x1C    加速度计配置寄存器
//  0x19    采样频率分频器
//  0x1A    配置寄存器

//  0x38    中断使能寄存器
//  0x6A    用户控制寄存器
//  0x23    FIFO使能寄存器
//  0x37    中断/旁路设置寄存器
//  0x75    器件ID寄存器

// MPU6050 初始设置  这个参数 最后根据实际情况 不断修正
hi_void Mpu6050_Init(hi_void)
{
    Mpu6050_I2c_Read(0x75, mpu6050_data, 1);    // 0x75 MPU6050的设备标识地址

    if(mpu6050_data[0] == 0x68) // 读取标识位ID==0x68表明设备在线正常
    {
        printf("111111111111111111111111111111");
        Mpu6050_I2c_Write(0x6b, 0x80);  // [7] = 1 复位 所有数据自动清零
        hi_udelay(100 * 1000);          // 等待100ms

        Mpu6050_I2c_Write(0x6b, 0x00);  // 唤醒 正常模式 温度计使能 内部8MHz晶振

        Mpu6050_I2c_Write(0x1b, 0x18);  // 角速度 不自检 测量范围 2000dps
        Mpu6050_I2c_Write(0x1c, 0x00);  // 加速度 不自检 测量范围 2g

        Mpu6050_I2c_Write(0x19, 0x07);  // 采样率 125Hz
        Mpu6050_I2c_Write(0x1a, 0x06);  // 低通滤波频率 5Hz

        Mpu6050_I2c_Write(0x38, 0x00);  // 关闭所有中断
        Mpu6050_I2c_Write(0x6a, 0x00);  // i2c 主模式
        Mpu6050_I2c_Write(0x23, 0x00);  // fifo 关闭

        Mpu6050_I2c_Write(0x37, 0x80);  // int引脚低电平有效
        Mpu6050_I2c_Write(0x6c, 0x00);  // 加速器、陀螺仪工作

    }

    //printf("\n car_mpu6050 [Mpu6050_Init] \n");
}

// // 3861接入端口设置 i2c设置
// hi_void Hi3861_Gpio_Init(hi_void)
// {
//     hi_u32 ret;

//     hi_io_set_func(HI_IO_NAME_GPIO_13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
//     hi_io_set_func(HI_IO_NAME_GPIO_14, HI_IO_FUNC_GPIO_14_I2C0_SCL);

//     ret = hi_i2c_init(HI_I2C_IDX_0, 400000);

//     if (ret != HI_ERR_SUCCESS)
//     {
//         printf(" [hi_i2c_init] Failed \n");
//     }
// }


// 返回 字节型数据

// 返回整数数据 hi_u8 mpu6050_data[14] = {0} hi_u8 data_len = 14;
hi_void Mpu6050_Measure_By(hi_u8 *datas, hi_u8 len)
{
    Mpu6050_I2c_Read(0x3b, datas, len);

   // printf("\n car_mpu6050 [Mpu6050_Measure_By] \n");
}


// 返回整数数据 hi_s16 datas[7] = {0};
hi_void Mpu6050_Measure_Sh(hi_s16 *datas)
{
    Mpu6050_Measure_By(mpu6050_data, data_len);

    datas[0] = (hi_s16)mpu6050_data[0] << 8 | mpu6050_data[1];      // accel x
    datas[1] = (hi_s16)mpu6050_data[2] << 8 | mpu6050_data[3];      // accel y
    datas[2] = (hi_s16)mpu6050_data[4] << 8 | mpu6050_data[5];      // accel z
    datas[3] = (hi_s16)mpu6050_data[6] << 8 | mpu6050_data[7];      // temperature
    datas[4] = (hi_s16)mpu6050_data[8] << 8 | mpu6050_data[9];      // gyro x
    datas[5] = (hi_s16)mpu6050_data[10] << 8 | mpu6050_data[11];    // gyro y
    datas[6] = (hi_s16)mpu6050_data[12] << 8 | mpu6050_data[13];    // gyro z

    //printf("\n car_mpu6050 [Mpu6050_Measure_Sh] \n");
}

// 返回浮点结果 hi_double *datas[7] = {0.0};
hi_void Mpu6050_Measure_Do(hi_double *datas)
{
    Mpu6050_Measure_Sh(value);

    IMU_Update(value[4],value[5],value[6],value[0],value[1],value[2]);

    datas[0] = (hi_double)value[0]/32768*2*9.8; // 单位 米
    datas[1] = (hi_double)value[1]/32768*2*9.8;
    datas[2] = (hi_double)value[2]/32768*2*9.8;

    datas[3] = (hi_double)value[3]/340+36.53; //温度

    datas[4] = (hi_double)value[4]/32768*2000;
    datas[5] = (hi_double)value[5]/32768*2000;
    datas[6] = (hi_double)value[6]/32768*2000;

    //printf("\n car_mpu6050 [Mpu6050_Measure_Do] \n");
}
