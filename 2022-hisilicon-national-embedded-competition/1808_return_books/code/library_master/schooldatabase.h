#ifndef SCHOOLDATABASE_H
#define SCHOOLDATABASE_H

#include <QObject>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlQueryModel>
#include "student.h"
class SchoolDataBase
{
public:
    SchoolDataBase();
    QSqlDatabase db;
    QSqlQuery query;
    QSqlQueryModel *model;

    void addLine(Student* sm);//增加一行数据
    void deleteLine(int id);//删除一行数据
    void changeLine(Student* sm);//改变一行数据
    Student* getLine(int id);//获取一行数据
};

#endif // SCHOOLDATABASE_H
